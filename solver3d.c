
#include "solver3d.h"

#include "AnalyticalSolution.h"
#include "CartMesh.h"
#include "FiniteDifferenceMethod2ndOrder.h"
#include "MatrixConvDif.h"
#include "Reconstruction.h"
#include "Stencil.h"
#include "WeightedLeastSquares.h"

#define QUIET
// #define DEBUG
// #define PRINT_VECS


static char help[] = "\
A very-high order Weighted Least-Squares solver for 3D convection-diffusion equation using PETSc.\n\
Input parameters include:\n\
    ___________________________________GEOMETRY AND PROPERTIES___________________________________\n\
    -cell_side      <INT>                          :  number of cells per edge in grid           (default: 10)\n\
    -gamma_diff     <NUM>                          :  diffusion coefficient                      (default: -1)\n\
    -u_conv         <NUM>,<NUM>,<NUM>              :  x-, y- convection velocity                 (default: 1,1,1)\n\
    -bndcond        <STR>[,<STR>[,<STR>[,<STR>]]]  :  boundary condition                         (default: dirichlet)\n\
    -solution       <STR>                          :  exact solution type                        (default: sin)\n\n\
    ________________________________________SOLVER METHODS_______________________________________\n\
    -method         <STR>                          :  solver method and order                    (default: WLS_4)\n\
    -stencil        <STR>                          :  stencil type                               (default: face)\n\
    -weight         <INT>                          :  weight factor \'k\'                          (default: 6)\n\
    -ksptype        <STR>                          :  Krylov subspace method                     (default: bcgs)\n\
    -pctype         <STR>                          :  preconditioner                             (default: gamg(2))\n\
    -reltol         <NUM>                          :  relative tolerance for convergence         (default: 1.0E-15)\n\
    -abstol         <NUM>                          :  absolute tolerance for convergence         (default: 1.0E-20)\n\
    -divtol         <NUM>                          :  tolerance for divergence                   (default: 1.0E+03)\n\
    -maxits         <INT>                          :  maximum iterations                         (default: 1.0E+04)\n\
    -custom_KSP     <T/F>                          :  custom PETSc setup, requires extra inputs  (default: false)\n\n\
    ____________________________________________OUTPUT____________________________________________\n\
    -print_matinfo  <T/F>                          :  print MatInfo of matrix A                  (default: false)\n\
    -print_vars     <T/F>                          :  print vars to individual ouput files       (default: false)\n\
    -output         <STR>                          :  output filename                            (default: solver3d.out)\n\n";



int main(int argc, char *argv[])
{

    SolverSettings*     solver;
    MeshGrid*           mesh;
    Solution*           sol;
    Error*              error;
    FILE*               fout;
    

    /* Initialize Solver and Read Input Args */
    PetscInitialize( &argc, &argv, (char *)0, help );
    SolverInit( &solver, &mesh, &sol, &error, &fout );
    
    #ifndef QUIET
    if (solver->cell_side/solver->nprocs < 1+solver->method ) PetscPrintf( PETSC_COMM_WORLD, "\n\tWARNING: will probably crash due to new nonzero malloc :(\n\t\t Try increasing \'cell_side\' or decreasing \'nprocs\'\n");
    #endif // !QUIET
        

    
    /* Generate Grid */
    CartMesh( solver, mesh );


    /* Calculate Analytical Solution */
    AnalyticalSolution( solver, mesh, sol );
    {
        /* Print processor allocation */
        PetscInt m, n, p;
        DMDAGetInfo( sol->dm, NULL, NULL, NULL, NULL, &m, &n, &p, NULL, NULL, NULL, NULL, NULL, NULL );
        PetscPrintf( PETSC_COMM_WORLD, "\nnum procs x = %3ld\nnum procs y = %3ld\nnum procs z = %3ld\n", m, n, p );
        PetscFPrintf( PETSC_COMM_WORLD, fout, "\nnum procs x = %3ld\nnum procs y = %3ld\nnum procs z = %3ld\n", m, n, p );
    }


    /*--- Calculate Numerical Solution ---*/
    if (solver->method == FDM_2)
        FiniteDifferenceMethod2ndOrder( solver, mesh, sol );
    else
        WeightedLeastSquares( solver, mesh, sol );

    if (solver->print_matinfo)
        PetscFPrintf( PETSC_COMM_WORLD, fout, 
                        "\nnz_allocated = %e\nnz_used      = %e\nnz_unneded   = %e\nmemory       = %e\n",
                        solver->matinfo->nz_allocated, solver->matinfo->nz_used, 
                        solver->matinfo->nz_unneeded,  solver->matinfo->memory );

    switch (solver->reason)     // Print ConvergedReason and TotalIterations 
    {
    case KSP_DIVERGED_PC_FAILED:
        PetscFPrintf( PETSC_COMM_WORLD, fout, "\nDIVERGED_PC_FAILED at iteration %ld\n", solver->totits );
        break;
    case KSP_DIVERGED_INDEFINITE_MAT:
        PetscFPrintf( PETSC_COMM_WORLD, fout, "\nDIVERGED_INDEFINITE_MAT at iteration %ld\n", solver->totits );
        break;
    case KSP_DIVERGED_NANORINF:
        PetscFPrintf( PETSC_COMM_WORLD, fout, "\nDIVERGED_NANORINF at iteration %ld\n", solver->totits );
        break;
    case KSP_DIVERGED_INDEFINITE_PC:
        PetscFPrintf( PETSC_COMM_WORLD, fout, "\nDIVERGED_INDEFINITE_PC at iteration %ld\n", solver->totits );
        break;
    case KSP_DIVERGED_NONSYMMETRIC:
        PetscFPrintf( PETSC_COMM_WORLD, fout, "\nDIVERGED_NONSYMMETRIC at iteration %ld\n", solver->totits );
        break;
    case KSP_DIVERGED_BREAKDOWN_BICG:
        PetscFPrintf( PETSC_COMM_WORLD, fout, "\nDIVERGED_BREAKDOWN_BICG at iteration %ld\n", solver->totits );
        break;
    case KSP_DIVERGED_BREAKDOWN:
        PetscFPrintf( PETSC_COMM_WORLD, fout, "\nDIVERGED_BREAKDOWN at iteration %ld\n", solver->totits );
        break;
    case KSP_DIVERGED_DTOL:
        PetscFPrintf( PETSC_COMM_WORLD, fout, "\nDIVERGED_DTOL at iteration %ld\n", solver->totits );
        break;
    case KSP_DIVERGED_ITS:
        PetscFPrintf( PETSC_COMM_WORLD, fout, "\nDIVERGED_ITS at iteration %ld\n", solver->totits );
        break;
    case KSP_DIVERGED_NULL:
        PetscFPrintf( PETSC_COMM_WORLD, fout, "\nDIVERGED_NULL at iteration %ld\n", solver->totits );
        break;
    case KSP_CONVERGED_ITERATING:
        PetscFPrintf( PETSC_COMM_WORLD, fout, "\nCONVERGED_ITERATING at iteration %ld\n", solver->totits );
        break;
    case KSP_CONVERGED_RTOL_NORMAL:
        PetscFPrintf( PETSC_COMM_WORLD, fout, "\nCONVERGED_RTOL_NORMAL at iteration %ld\n", solver->totits );
        break;
    case KSP_CONVERGED_RTOL:
        PetscFPrintf( PETSC_COMM_WORLD, fout, "\nCONVERGED_RTOL at iteration %ld\n", solver->totits );
        break;
    case KSP_CONVERGED_ATOL:
        PetscFPrintf( PETSC_COMM_WORLD, fout, "\nCONVERGED_ATOL at iteration %ld\n", solver->totits );
        break;
    case KSP_CONVERGED_ITS:
        PetscFPrintf( PETSC_COMM_WORLD, fout, "\nCONVERGED_ITS at iteration %ld\n", solver->totits );
        break;
    case KSP_CONVERGED_CG_NEG_CURVE:
        PetscFPrintf( PETSC_COMM_WORLD, fout, "\nCONVERGED_CG_NEG_CURVE at iteration %ld\n", solver->totits );
        break;
    case KSP_CONVERGED_CG_CONSTRAINED:
        PetscFPrintf( PETSC_COMM_WORLD, fout, "\nCONVERGED_CG_CONSTRAINED at iteration %ld\n", solver->totits );
        break;
    case KSP_CONVERGED_STEP_LENGTH:
        PetscFPrintf( PETSC_COMM_WORLD, fout, "\nCONVERGED_STEP_LENGTH at iteration %ld\n", solver->totits );
        break;
    case KSP_CONVERGED_HAPPY_BREAKDOWN:
        PetscFPrintf( PETSC_COMM_WORLD, fout, "\nCONVERGED_HAPPY_BREAKDOWN at iteration %ld\n", solver->totits );
        break;
    case KSP_CONVERGED_ATOL_NORMAL:
        PetscFPrintf( PETSC_COMM_WORLD, fout, "\nCONVERGED_ATOL_NORMAL at iteration %ld\n", solver->totits );
        break;
    }
    

    /* --- Calculate Error Values --- */
    /* ----------------------------------------------------------------- */
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\nCALCULATING ERROR VALUES\n\tstarted ...\n");
    #endif // !QUIET

    // create error vectors
    Vec abs_err_vec, rel_err_vec;
    VecDuplicate( sol->num->phi, &abs_err_vec);
    VecDuplicate( sol->num->phi, &rel_err_vec);
    VecWAXPY(abs_err_vec, -1, sol->anal->phi, sol->num->phi);
    VecPointwiseDivide(rel_err_vec, abs_err_vec, sol->anal->phi);

    // calculate norm1, norm2, norminf (max) phi errors
    VecNormBegin( abs_err_vec, NORM_1, &(error->abs->norm1) );
    VecNormBegin( rel_err_vec, NORM_1, &(error->rel->norm1) );
    VecNormBegin( abs_err_vec, NORM_2, &(error->abs->norm2) );
    VecNormBegin( rel_err_vec, NORM_2, &(error->rel->norm2) );
    VecNormBegin( abs_err_vec, NORM_INFINITY, &(error->abs->norminf) );
    VecNormBegin( rel_err_vec, NORM_INFINITY, &(error->rel->norminf) );
    VecNormEnd( abs_err_vec, NORM_1, &(error->abs->norm1) );
    VecNormEnd( rel_err_vec, NORM_1, &(error->rel->norm1) );
    VecNormEnd( abs_err_vec, NORM_2, &(error->abs->norm2) );
    VecNormEnd( rel_err_vec, NORM_2, &(error->rel->norm2) );
    VecNormEnd( abs_err_vec, NORM_INFINITY, &(error->abs->norminf) );
    VecNormEnd( rel_err_vec, NORM_INFINITY, &(error->rel->norminf) );
    error->abs->norm1 = error->abs->norm1 / mesh->cell_num;
    error->rel->norm1 = error->rel->norm1 / mesh->cell_num;
    error->abs->norm2 = error->abs->norm2 / mesh->cell_num;
    error->rel->norm2 = error->rel->norm2 / mesh->cell_num;

    // find cells with max abs/rel errors
    VecMax( abs_err_vec, &(error->abs->cellmax), &(error->abs->valmax) );
    VecMax( rel_err_vec, &(error->rel->cellmax), &(error->rel->valmax) );
    
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\tabs max: %E at cell %ld\n", (double) error->abs->valmax, error->abs->cellmax );
    PetscPrintf( PETSC_COMM_WORLD, "\trel max: %E at cell %ld\n", (double) error->rel->valmax, error->rel->cellmax );
    #endif // !QUIET

    // clean up
    VecDestroy(&abs_err_vec);
    VecDestroy(&rel_err_vec);


    /* ----------------------------------------------------------------- */

    /*--- Export Results ---*/

    PetscPrintf( PETSC_COMM_WORLD, "\n--------------- RESULTS ---------------\n");
    PetscPrintf( PETSC_COMM_WORLD, "                 N: %ld\n", mesh->cell_num);
    PetscPrintf( PETSC_COMM_WORLD, "       norm1 (abs): %E\n", (double) error->abs->norm1);
    PetscPrintf( PETSC_COMM_WORLD, "       norm2 (abs): %E\n", (double) error->abs->norm2);
    PetscPrintf( PETSC_COMM_WORLD, "     norminf (abs): %E\n", (double) error->abs->norminf);
    PetscPrintf( PETSC_COMM_WORLD, "       norm1 (rel): %E\n", (double) error->rel->norm1);
    PetscPrintf( PETSC_COMM_WORLD, "       norm2 (rel): %E\n", (double) error->rel->norm2);
    PetscPrintf( PETSC_COMM_WORLD, "     norminf (rel): %E\n", (double) error->rel->norminf);
    

    PetscFPrintf( PETSC_COMM_WORLD, fout, "\n--------------- RESULTS ---------------\n");
    PetscFPrintf( PETSC_COMM_WORLD, fout, "                 N: %ld\n", mesh->cell_num);
    PetscFPrintf( PETSC_COMM_WORLD, fout, "       norm1 (abs): %E\n", (double) error->abs->norm1);
    PetscFPrintf( PETSC_COMM_WORLD, fout, "       norm2 (abs): %E\n", (double) error->abs->norm2);
    PetscFPrintf( PETSC_COMM_WORLD, fout, "     norminf (abs): %E\n", (double) error->abs->norminf);
    PetscFPrintf( PETSC_COMM_WORLD, fout, "       norm1 (rel): %E\n", (double) error->rel->norm1);
    PetscFPrintf( PETSC_COMM_WORLD, fout, "       norm2 (rel): %E\n", (double) error->rel->norm2);
    PetscFPrintf( PETSC_COMM_WORLD, fout, "     norminf (rel): %E\n", (double) error->rel->norminf);


    // ----------------------------------------------------

    /*--- End Program ---*/
    SolverEnd( solver, mesh, sol, error, fout );

    return 0;
}


static PetscErrorCode SolverInit( SolverSettings** solver, MeshGrid** mesh,
                                    Solution** sol, Error** error, FILE** fout )
{
    /* --------------------------------------------
        Reads input options from argument list. 
        Possible options are listed in help[].
    -------------------------------------------- */

    /* Aux variables */
    PetscErrorCode  ierr;
    PetscBool       set;
    PetscChar       buf[100];
    PetscChar       *cptr = NULL;
    PetscInt        len;
    PetscReal       aux_real, _u_conv[5];

    PetscOptionsGetBool(NULL, NULL, "-help", &set, &set);
    if (set) exit(0);
    PetscOptionsGetBool(NULL, NULL, "--help", &set, &set);
    if (set) { PetscPrintf(PETSC_COMM_WORLD, "\n%s\n", help); exit(0); }


    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\nINITIALIZING SOLVER...\n");
    #endif // !QUIET

    /* print local time */
    #ifndef QUIET
    time_t timer = time(NULL);
    struct tm* tm_info = localtime(&timer);
    strftime( buf, 50, "%H:%M", tm_info );
    PetscPrintf( PETSC_COMM_WORLD, "\tstart time: \t%s\n", buf);
    #endif // !QUIET


    /* Allocate memory space for struct variables */
    /* --- */
    *solver             = malloc(sizeof(SolverSettings));
    (*solver)->u_conv   = malloc(sizeof(struct Velocity));
    (*solver)->bndcond  = malloc(sizeof(struct Boundary));
    (*solver)->ops      = malloc(sizeof(SolverOps));

    *mesh               = malloc(sizeof(MeshGrid));

    *sol                = malloc(sizeof(Solution));
    (*sol)->anal        = malloc(sizeof(struct SolutionAux));
    (*sol)->num         = malloc(sizeof(struct SolutionAux));

    *error              = malloc(sizeof(Error));
    (*error)->abs       = malloc(sizeof(struct ErrorAux));
    (*error)->rel       = malloc(sizeof(struct ErrorAux));
    /* --- */

    (*solver)->ksptype  = KSPBCGS;
    (*solver)->pctype   = PCGAMG;
    (*solver)->gamg_threshold = .02;
    (*solver)->divtol   = 1.e+03;
    (*solver)->maxits   = 1e4;
    #ifdef PETSC_USE_REAL___FLOAT128
    (*solver)->reltol   = 1.e-35;
    (*solver)->abstol   = 1.e-40;
    #else
    (*solver)->reltol   = 1.e-15;
    (*solver)->abstol   = 1.e-20;
    #endif // PETSC_USE_REAL___FLOAT128


    /* Get MPI and OpenMP info */
    MPI_Comm_rank( PETSC_COMM_WORLD, &((*solver)->rank) );
    MPI_Comm_size( PETSC_COMM_WORLD, &((*solver)->nprocs) );
    (*solver)->nthreads = omp_get_max_threads();

    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\tnprocs: \t%ld\n\tnthreads: \t%ld\n",
            (*solver)->nprocs, (*solver)->nthreads );
    #endif // !QUIET


    /* Read Input Args */

    // #region      OUTPUT
    ierr = PetscOptionsGetString(NULL, NULL, "-output", buf, sizeof(buf), &set);
    CHKERRQ(ierr);
    if (!set)
        strcpy(buf, "solver3d.out");
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\toutput: \t%s\n", buf );
    #endif // !QUIET
    ierr = PetscFOpen( PETSC_COMM_WORLD, buf, "w", fout);
    CHKERRQ(ierr);
    #ifndef QUIET
    ierr = PetscFPrintf( PETSC_COMM_WORLD, *fout, "nprocs: \t%ld\nnthreads: \t%ld\n",
            (*solver)->nprocs, (*solver)->nthreads);
    PetscFPrintf( PETSC_COMM_WORLD, *fout, "output: \t%s\n", buf );
    #endif // !QUIET
    // #endregion

    // #region      PRECISION*
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\tprecision:\t%ld\n", 8*sizeof(PetscReal) );
    #endif // !QUIET
    PetscFPrintf( PETSC_COMM_WORLD, *fout, "precision:\t%ld\n", 8*sizeof(PetscReal) );
    // #endregion

    // #region      PRINT VARS
    ierr = PetscOptionsGetBool(NULL, NULL, "-print_vars", &((*solver)->print_vars), &set);
    CHKERRQ(ierr);
    if (!set)
        (*solver)->print_vars = PETSC_FALSE;
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\tprint_vars:\t%u\n", (*solver)->print_vars);
    #endif // !QUIET
    PetscFPrintf( PETSC_COMM_WORLD, *fout, "print_vars:\t%u\n", (*solver)->print_vars);
    // #endregion

    // #region      EXPORT MAT
    ierr = PetscOptionsGetString(NULL, NULL, "-export_linsys", (*solver)->linsys_fname, sizeof((*solver)->linsys_fname), &set);
    // ierr = PetscOptionsGetBool(NULL, NULL, "-export_linsys", &((*solver)->export_linsys), &set);
    CHKERRQ(ierr);
    if (set)
        (*solver)->export_linsys = PETSC_TRUE;
    else
        (*solver)->export_linsys = PETSC_FALSE;
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\texport_linsys:\t%u\n", (*solver)->export_linsys);
    #endif // !QUIET
    PetscFPrintf( PETSC_COMM_WORLD, *fout, "export_linsys:\t%u\n", (*solver)->export_linsys);
    // #endregion

    // #region      IMPORT MAT
    ierr = PetscOptionsGetString(NULL, NULL, "-import_linsys", (*solver)->linsys_fname, sizeof((*solver)->linsys_fname), &set);
    // ierr = PetscOptionsGetBool(NULL, NULL, "-import_linsys", &((*solver)->import_linsys), &set);
    CHKERRQ(ierr);
    if (set)
        (*solver)->import_linsys = PETSC_TRUE;
    else
        (*solver)->import_linsys = PETSC_FALSE;
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\timport_linsys:\t%u\n", (*solver)->import_linsys);
    #endif // !QUIET
    PetscFPrintf( PETSC_COMM_WORLD, *fout, "import_linsys:\t%u\n", (*solver)->import_linsys);
    // #endregion

    if ((*solver)->export_linsys && (*solver)->import_linsys)
    {
        PetscPrintf( PETSC_COMM_WORLD, "Option Error: requested to both \'export\' AND \'import\' matrix\n");
        exit(1);
    }
    else if ((*solver)->export_linsys || (*solver)->import_linsys)
    {
        #ifndef QUIET
        PetscPrintf( PETSC_COMM_WORLD, "\tlinsys_fname:\t%s\n", (*solver)->linsys_fname);
        #endif // !QUIET
        PetscFPrintf( PETSC_COMM_WORLD, *fout, "linsys_fname:\t%s\n", (*solver)->linsys_fname);
    }
    
    // #region      PRINT MAT INFO
    ierr = PetscOptionsGetBool(NULL, NULL, "-print_matinfo", &((*solver)->print_matinfo), &set);
    CHKERRQ(ierr);
    if (!set)
    {
        ierr = PetscOptionsGetBool(NULL, NULL, "-matinfo", &((*solver)->print_matinfo), &set);
        CHKERRQ(ierr);
        if (!set)
            (*solver)->print_matinfo = PETSC_FALSE;
    }
    if ((*solver)->print_matinfo)
        PetscMalloc1( 1, &(*solver)->matinfo );
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\tprint_matinfo:\t%u\n", (*solver)->print_matinfo);
    #endif // !QUIET
    PetscFPrintf( PETSC_COMM_WORLD, *fout, "print_matinfo:\t%u\n", (*solver)->print_matinfo);
    // #endregion

    // #region      ALLOC OPT
    ierr = PetscOptionsGetBool(NULL, NULL, "-alloc_opt", &((*solver)->alloc_opt), &set);
    CHKERRQ(ierr);
    if (!set)
        (*solver)->alloc_opt = PETSC_FALSE;
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\talloc_opt:\t%u\n", (*solver)->print_vars);
    #endif // !QUIET
    PetscFPrintf( PETSC_COMM_WORLD, *fout, "alloc_opt:\t%u\n", (*solver)->print_vars);
    // #endregion

    // #region      CELL SIDE
    ierr = PetscOptionsGetInt(NULL, NULL, "-cell_side", &((*solver)->cell_side), &set);
    CHKERRQ(ierr);
    if (!set)
        (*solver)->cell_side = 10;
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\tcell_side:\t%ld\n", (*solver)->cell_side);
    #endif // !QUIET
    PetscFPrintf( PETSC_COMM_WORLD, *fout, "cell_side:\t%ld\n", (*solver)->cell_side);
    // #endregion

    // #region      DIFFUSION COEFF
    ierr = PetscOptionsGetReal(NULL, NULL, "-gamma_diff", &((*solver)->gamma_diff), &set);
    CHKERRQ(ierr);
    if (!set)
        #ifdef  PETSC_USE_REAL___FLOAT128
        (*solver)->gamma_diff = -1.0Q;
        #else
        (*solver)->gamma_diff = -1.0;
        #endif
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\tgamma_diff:\t%f\n", (double) (*solver)->gamma_diff);
    #endif // !QUIET
    PetscFPrintf( PETSC_COMM_WORLD, *fout, "gamma_diff:\t%f\n", (double) (*solver)->gamma_diff);
    // #endregion

    // #region      CONVECTION VELOCITY
    len = 5;
    ierr = PetscOptionsGetRealArray(NULL, NULL, "-u_conv", _u_conv, &len, &set);
    CHKERRQ(ierr);
    if (!set)
    {
        #ifdef  PETSC_USE_REAL___FLOAT128
        (*solver)->u_conv->x = 1.0Q;
        (*solver)->u_conv->y = 1.0Q;
        (*solver)->u_conv->z = 1.0Q;
        #else
        (*solver)->u_conv->x = 1.0;
        (*solver)->u_conv->y = 1.0;
        (*solver)->u_conv->z = 1.0;
        #endif
    }
    else
    {
        if (len < 3)
        {
            PetscPrintf( PETSC_COMM_WORLD, "Input Error: \'u_conv\' has too few dimensions, please provide three.\n");
            exit(1);
        }
        (*solver)->u_conv->x = _u_conv[0];
        (*solver)->u_conv->y = _u_conv[1];
        (*solver)->u_conv->z = _u_conv[2];
        if (len > 3)
            PetscPrintf( PETSC_COMM_WORLD, "Input Warning: \'u_conv\' has too many dimensions, using only first three.\n");
    }
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\tu_conv:  \t%f, %f, %f\n", (double) (*solver)->u_conv->x, (double) (*solver)->u_conv->y, (double) (*solver)->u_conv->z);
    #endif // !QUIET
    PetscFPrintf( PETSC_COMM_WORLD, *fout, "u_conv:  \t%f, %f, %f\n", (double) (*solver)->u_conv->x, (double) (*solver)->u_conv->y, (double) (*solver)->u_conv->z);
    // #endregion

    // #region      SOLUTION
    ierr = PetscOptionsGetString(NULL, NULL, "-solution", buf, sizeof(buf), &set);
    CHKERRQ(ierr);
    if (!set || !strcmp(buf, "sin") || !strcmp(buf, "s"))
    {
        (*solver)->solution = SIN;
        #ifndef QUIET
        PetscPrintf( PETSC_COMM_WORLD, "\tsolution:\tsine\n");
        #endif // !QUIET
        PetscFPrintf( PETSC_COMM_WORLD, *fout, "solution:\tsine\n");
    }
    else
    {
        PetscPrintf( PETSC_COMM_WORLD, "Input Error: invalid \'solution\'\n\tAccepted values: \'sin\'/\'s\'\n");
        exit(1);
    }
    // #endregion

    // #region      METHOD
    ierr = PetscOptionsGetString(NULL, NULL, "-method", buf, sizeof(buf), &set);
    CHKERRQ(ierr);
    if (!set || !strcmp(buf, "WLS_4") || !strcmp(buf, "4"))
    {
        (*solver)->method = WLS_4;
        #ifndef QUIET
        PetscPrintf( PETSC_COMM_WORLD, "\tmethod:\t\tWLS_4\n");
        #endif // !QUIET
        PetscFPrintf( PETSC_COMM_WORLD, *fout, "method:\t\tWLS_4\n");
    }
    else if (!strcmp(buf, "FDM_2") || !strcmp(buf, "F"))
    {
        (*solver)->method = FDM_2;
        #ifndef QUIET
        PetscPrintf( PETSC_COMM_WORLD, "\tmethod:\t\tFDM_2\n");
        #endif // !QUIET
        PetscFPrintf( PETSC_COMM_WORLD, *fout, "method:\t\tFDM_2\n");
    }
    else if (!strcmp(buf, "WLS_2") || !strcmp(buf, "2"))
    {
        (*solver)->method = WLS_2;
        #ifndef QUIET
        PetscPrintf( PETSC_COMM_WORLD, "\tmethod:\t\tWLS_2\n");
        #endif // !QUIET
        PetscFPrintf( PETSC_COMM_WORLD, *fout, "method:\t\tWLS_2\n");
    }
    // else if (!strcmp(buf, "WLS_4") || !strcmp(buf, "4"))
    // {
    //     (*solver)->method = WLS_4;
    //     PetscPrintf( PETSC_COMM_WORLD, "\tmethod:\t\tWLS_4\n");
    //     PetscFPrintf( PETSC_COMM_WORLD, *fout, "method:\t\tWLS_4\n");
    // }
    else if (!strcmp(buf, "WLS_6") || !strcmp(buf, "6"))
    {
        (*solver)->method = WLS_6;
        #ifndef QUIET
        PetscPrintf( PETSC_COMM_WORLD, "\tmethod:\t\tWLS_6\n");
        #endif // !QUIET
        PetscFPrintf( PETSC_COMM_WORLD, *fout, "method:\t\tWLS_6\n");
    }
    else if (!strcmp(buf, "WLS_8") || !strcmp(buf, "8"))
    {
        (*solver)->method = WLS_8;
        #ifndef QUIET
        PetscPrintf( PETSC_COMM_WORLD, "\tmethod:\t\tWLS_8\n");
        #endif // !QUIET
        PetscFPrintf( PETSC_COMM_WORLD, *fout, "method:\t\tWLS_8\n");
    }
    else
    {
        PetscPrintf( PETSC_COMM_WORLD, "Input Error: invalid \'method\'\n\tAccepted values: \'FDM_2\'/ \'F\',\'WLS_2\'/\'2\',\'WLS_4\'/\'4\',\'WLS_6\'/\'6\',\'WLS_8\'/\'8\',\n");
        exit(1);
    }
    // #endregion

    // #region      BOUNDARY CONDITION
    set = PETSC_FALSE;
    ierr = PetscOptionsGetString(NULL, NULL, "-bndcond", buf, sizeof(buf), &set);
    CHKERRQ(ierr);
    cptr = strchr(buf, ',');
    if (!set || !strcmp(buf, "dirichlet") || !strcmp(buf, "d"))
    {
        (*solver)->bndcond->w = DIRICHLET;
        (*solver)->bndcond->e = DIRICHLET;
        (*solver)->bndcond->s = DIRICHLET;
        (*solver)->bndcond->n = DIRICHLET;
        (*solver)->bndcond->b = DIRICHLET;
        (*solver)->bndcond->t = DIRICHLET;
        #ifndef QUIET
        PetscPrintf( PETSC_COMM_WORLD, "\tbndcond:\tdirichlet\n");
        #endif // !QUIET
        PetscFPrintf( PETSC_COMM_WORLD, *fout, "bndcond:\tdirichlet\n");
    }
    else if (!strcmp(buf, "neumann") || !strcmp(buf, "n"))
    {
        (*solver)->bndcond->w = NEUMANN;
        (*solver)->bndcond->e = NEUMANN;
        (*solver)->bndcond->s = NEUMANN;
        (*solver)->bndcond->n = NEUMANN;
        (*solver)->bndcond->b = NEUMANN;
        (*solver)->bndcond->t = NEUMANN;
        #ifndef QUIET
        PetscPrintf( PETSC_COMM_WORLD, "\tbndcond:\tneumann\n");
        #endif // !QUIET
        PetscFPrintf( PETSC_COMM_WORLD, *fout, "bndcond:\tneumann\n");
    }
    else if (!strcmp(buf, "robin") || !strcmp(buf, "r"))
    {
        (*solver)->bndcond->w = ROBIN;
        (*solver)->bndcond->e = ROBIN;
        (*solver)->bndcond->s = ROBIN;
        (*solver)->bndcond->n = ROBIN;
        (*solver)->bndcond->b = ROBIN;
        (*solver)->bndcond->t = ROBIN;
        #ifndef QUIET
        PetscPrintf( PETSC_COMM_WORLD, "\tbndcond:\trobin\n");
        #endif // !QUIET
        PetscFPrintf( PETSC_COMM_WORLD, *fout, "bndcond:\trobin\n");
    }
    else
    {
        PetscChar       *tmpbuf = buf,
                        cardinal[6] = {'W', 'E', 'S', 'N', 'B', 'T'};
        BNDCOND_TYPE    tmpbndcond[6];
        for (PetscInt i = 0; i < 6; i++)
        {
            if (tmpbuf[0] == 'd')
                tmpbndcond[i] = DIRICHLET;
            else if (tmpbuf[0] == 'n')
                tmpbndcond[i] = NEUMANN;
            else if (tmpbuf[0] == 'r')
                tmpbndcond[i] = ROBIN;
            else
            {
                PetscPrintf( PETSC_COMM_WORLD, "Input Error: invalid \'bndcond\'\n\tAccepted values: \'dirichlet\'/ \'d\',\'neumann\'/\'n\',\'robin\'/\'r\',\n");
                exit(1);
            }
            if (i == 0)
            {
                #ifndef QUIET
                PetscPrintf( PETSC_COMM_WORLD, "\tbndcond:\t%c [E]", tmpbuf[0]);
                #endif // !QUIET
                PetscFPrintf( PETSC_COMM_WORLD, *fout, "bndcond:\t%c [E]", tmpbuf[0]);
            }
            else
            {
                #ifndef QUIET
                PetscPrintf( PETSC_COMM_WORLD, ", %c [%c]", tmpbuf[0], cardinal[i]);
                #endif // !QUIET
                PetscFPrintf( PETSC_COMM_WORLD, *fout, ", %c [%c]", tmpbuf[0], cardinal[i]);
            }
            if (cptr == NULL)
            {
                for (PetscInt j = i+1; j < 6; j++, i++)
                {
                    tmpbndcond[j] = DIRICHLET;
                    #ifndef QUIET
                    PetscPrintf( PETSC_COMM_WORLD, ", d [%c]", cardinal[j]);
                    #endif // !QUIET
                    PetscFPrintf( PETSC_COMM_WORLD, *fout, ", d [%c]", cardinal[j]);
                }
                break;
            }
            tmpbuf = cptr + 1;
            cptr = strchr(tmpbuf, ',');
        }
        #ifndef QUIET
        PetscPrintf( PETSC_COMM_WORLD, "\n"); 
        #endif // !QUIET
        PetscFPrintf( PETSC_COMM_WORLD, *fout, "\n"); 
        (*solver)->bndcond->w = tmpbndcond[0];
        (*solver)->bndcond->e = tmpbndcond[1];
        (*solver)->bndcond->s = tmpbndcond[2];
        (*solver)->bndcond->n = tmpbndcond[3];
        (*solver)->bndcond->b = tmpbndcond[4];
        (*solver)->bndcond->t = tmpbndcond[5];
    }

    if ( (*solver)->bndcond->e == ROBIN || (*solver)->bndcond->w == ROBIN || (*solver)->bndcond->s == ROBIN || (*solver)->bndcond->n == ROBIN  )
        if ( abs((*solver)->u_conv->x) < (*solver)->reltol || abs((*solver)->u_conv->y) < (*solver)->reltol  )
        {
            #ifndef QUIET
            PetscPrintf( PETSC_COMM_WORLD, "Input Warning: near-zero velocity component with \'robin\' may cause division-by-zero.\n");
            #endif // !QUIET
            PetscFPrintf( PETSC_COMM_WORLD, *fout, "Input Warning: near-zero velocity component with \'robin\' may cause division-by-zero.\n");
        }
    
    // #endregion

    // #region      WEIGHT
    ierr = PetscOptionsGetInt(NULL, NULL, "-weight", &((*solver)->weight), &set);
    CHKERRQ(ierr);
    if (!set)
        (*solver)->weight = 6;
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\tweight:  \t%ld\n", (*solver)->weight);
    #endif // !QUIET
    PetscFPrintf( PETSC_COMM_WORLD, *fout, "weight:  \t%ld\n", (*solver)->weight);
    // #endregion

    // #region      TOLS & MAXITS
    ierr = PetscOptionsGetReal(NULL, NULL, "-reltol", &aux_real, &set);
    CHKERRQ(ierr);
    if (set)
    {
        if (aux_real < 0)
        {
            PetscPrintf( PETSC_COMM_WORLD, "\nInput Error: reltol < 0\n\n");
            exit(1);
        }
        (*solver)->reltol = aux_real;
    }
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\treltol: \t%E\n", (double) (*solver)->reltol);
    #endif // !QUIET
    PetscFPrintf( PETSC_COMM_WORLD, *fout, "reltol: \t%E\n", (double) (*solver)->reltol);

    ierr = PetscOptionsGetReal(NULL, NULL, "-abstol", &aux_real, &set);
    CHKERRQ(ierr);
    if (set)
    {
        if (aux_real < 0)
        {
            PetscPrintf( PETSC_COMM_WORLD, "\nInput Error: abstol < 0\n\n");
            exit(1);
        }
        (*solver)->abstol = aux_real;
    }
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\tabstol: \t%E\n", (double) (*solver)->abstol);
    #endif // !QUIET
    PetscFPrintf( PETSC_COMM_WORLD, *fout, "abstol: \t%E\n", (double) (*solver)->abstol);

    ierr = PetscOptionsGetReal(NULL, NULL, "-divtol", &aux_real, &set);
    CHKERRQ(ierr);
    if (set)
    {
        if (aux_real < 0)
        {
            PetscPrintf( PETSC_COMM_WORLD, "\nInput Error: divtol < 0\n\n");
            exit(1);
        }
        (*solver)->divtol = aux_real;
    }
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\tdivtol: \t%E\n", (double) (*solver)->divtol);
    #endif // !QUIET
    PetscFPrintf( PETSC_COMM_WORLD, *fout, "divtol: \t%E\n", (double) (*solver)->divtol);

    ierr = PetscOptionsGetReal(NULL, NULL, "-maxits", &aux_real, &set);
    CHKERRQ(ierr);
    if (set)
    {
        if (aux_real < 1)
        {
            PetscPrintf( PETSC_COMM_WORLD, "\nInput Error: maxits < 1\n\n");
            exit(1);
        }
        (*solver)->maxits = aux_real;
    }
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\tmaxits: \t%E\n", (double) (*solver)->maxits);
    #endif // !QUIET
    PetscFPrintf( PETSC_COMM_WORLD, *fout, "maxits: \t%E\n", (double) (*solver)->maxits);
    // #endregion

    // #region      KSP & PC TYPES
    ierr = PetscOptionsGetString(NULL, NULL, "-ksptype", buf, sizeof(buf), &set);
    if (!strcmp(buf, "CG") || !strcmp(buf, "cg"))
        (*solver)->ksptype = KSPCG;
    else if (!strcmp(buf, "PIPECG") || !strcmp(buf, "pipecg"))
        (*solver)->ksptype = KSPPIPECG;
    else if (!strcmp(buf, "FCG") || !strcmp(buf, "fcg"))
        (*solver)->ksptype = KSPFCG;
    else if (!strcmp(buf, "PIPEFCG") || !strcmp(buf, "pipefcg"))
        (*solver)->ksptype = KSPPIPEFCG;
    else if (!strcmp(buf, "BiCG") || !strcmp(buf, "bicg"))
        (*solver)->ksptype = KSPBICG;
    else if (!strcmp(buf, "BCGS") || !strcmp(buf, "bcgs"))
        (*solver)->ksptype = KSPBCGS;
    else if (!strcmp(buf, "iBCGS") || !strcmp(buf, "ibcgs"))
        (*solver)->ksptype = KSPIBCGS;
    else if (!strcmp(buf, "GMRES") || !strcmp(buf, "gmres"))
        (*solver)->ksptype = KSPGMRES;
    else if (!strcmp(buf, "FGMRES") || !strcmp(buf, "fgmres"))
        (*solver)->ksptype = KSPFGMRES;
    else if (!strcmp(buf, "PIPEFGMRES") || !strcmp(buf, "pipefgmres"))
        (*solver)->ksptype = KSPPIPEFGMRES;
    else if (!strcmp(buf, "CR") || !strcmp(buf, "cr"))
        (*solver)->ksptype = KSPCR;
    else if (!strcmp(buf, "PIPECR") || !strcmp(buf, "pipecr"))
        (*solver)->ksptype = KSPPIPECR;
    else if (!strcmp(buf, "GCR") || !strcmp(buf, "gcr"))
        (*solver)->ksptype = KSPGCR;
    else if (!strcmp(buf, "PIPEGCR") || !strcmp(buf, "pipegcr"))
        (*solver)->ksptype = KSPPIPEGCR;
    else if (!strcmp(buf, "TFQMR") || !strcmp(buf, "tfqmr"))
        (*solver)->ksptype = KSPTFQMR;
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\tksptype:\t%s\n", (*solver)->ksptype );
    #endif // !QUIET
    PetscFPrintf( PETSC_COMM_WORLD, *fout, "ksptype:\t%s\n", (*solver)->ksptype );

    ierr = PetscOptionsGetString(NULL, NULL, "-pctype", buf, sizeof(buf), &set);
    if (!strcmp(buf, "NONE") || !strcmp(buf, "none"))
        (*solver)->pctype = PCNONE;
    else if (!strcmp(buf, "JACOBI") || !strcmp(buf, "jacobi"))
        (*solver)->pctype = PCJACOBI;
    else if (!strcmp(buf, "BJACOBI") || !strcmp(buf, "bjacobi"))
        (*solver)->pctype = PCBJACOBI;
    else if (!strcmp(buf, "ILU") || !strcmp(buf, "ilu"))
        (*solver)->pctype = PCILU;
    else if (!strcmp(buf, "ICC") || !strcmp(buf, "icc"))
        (*solver)->pctype = PCICC;
    else if (!strncmp(buf, "ASM", 3) || !strncmp(buf, "asm", 3))    // assuming format "ASM(n)" where n=overlap
    {
        (*solver)->pctype = PCASM;
        if (strlen(buf) > 4)
            (*solver)->asm_overlap = buf[4] - '0';
        else
            (*solver)->asm_overlap = 0;
    }
    else if (!strncmp(buf, "GAMG", 4) || !strncmp(buf, "gamg", 4))  // assuming format "GAMG(n)" where n*.01=threshold
    {
        (*solver)->pctype = PCGAMG;
        if (strlen(buf) > 5)
            (*solver)->gamg_threshold = (buf[5] - '0') * .01;
        else
            (*solver)->gamg_threshold = 0;
    }
    
    if (!strcmp((*solver)->pctype, PCASM))
    {
        #ifndef QUIET
        PetscPrintf( PETSC_COMM_WORLD, "\tpctype:\t\t%s, %ld\n", (*solver)->pctype, (*solver)->asm_overlap );
        #endif // !QUIET
        PetscFPrintf( PETSC_COMM_WORLD, *fout, "pctype:\t\t%s, %ld\n", (*solver)->pctype, (*solver)->asm_overlap );
    }
    else if (!strcmp((*solver)->pctype, PCGAMG))
    {
        #ifndef QUIET
        PetscPrintf( PETSC_COMM_WORLD, "\tpctype:\t\t%s, %.2f\n", (*solver)->pctype, (*solver)->gamg_threshold );
        #endif // !QUIET
        PetscFPrintf( PETSC_COMM_WORLD, *fout, "pctype:\t\t%s, %.2f\n", (*solver)->pctype, (*solver)->gamg_threshold );
    }
    else
    {
        #ifndef QUIET
        PetscPrintf( PETSC_COMM_WORLD, "\tpctype:\t\t%s\n", (*solver)->pctype );
        #endif // !QUIET
        PetscFPrintf( PETSC_COMM_WORLD, *fout, "pctype:\t\t%s\n", (*solver)->pctype );
    }
    // #endregion

    // #region      CUSTOM KSP
    ierr = PetscOptionsGetBool(NULL, NULL, "-custom_KSP", &((*solver)->custom_KSP), &set);
    CHKERRQ(ierr);
    if (!set)
        (*solver)->custom_KSP = PETSC_FALSE;
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\tcustom_KSP:\t%u\n", (*solver)->custom_KSP);
    #endif // !QUIET
    PetscFPrintf( PETSC_COMM_WORLD, *fout, "custom_KSP:\t%u\n", (*solver)->custom_KSP);
    // #endregion

    SetOps_Reconstruction( (*solver)->ops, (*solver)->method );
    
    return ierr;
}

static PetscErrorCode SolverEnd( SolverSettings* solver, MeshGrid* mesh, 
                                    Solution* sol, Error* error, FILE* fout )
{

    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\nENDING...\n\n");
    #endif // !QUIET
    PetscFClose(PETSC_COMM_WORLD, fout);

    free(solver->u_conv);
    free(solver->bndcond);
    free(solver->ops);
    free(solver);

    free(mesh);

    VecDestroy( &(sol->anal->phi) );
    VecDestroy( &(sol->anal->source) );
    VecDestroy( &(sol->num->phi) );
    VecDestroy( &(sol->num->source) );
    free(sol->anal->phi_faces);
    free(sol->anal->flux);
    // if (sol->num->phi_faces != NULL)
    //     free(sol->num->phi_faces);
    // if (sol->num->flux != NULL)
    //     free(sol->num->flux);
    DMDestroy( &(sol->dm) );
    free(sol->anal);
    free(sol->num);
    free(sol);

    free(error->abs);
    free(error->rel);
    free(error);
    

    return PetscFinalize();
}
