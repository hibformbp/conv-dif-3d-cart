#ifndef RECONSTRUCTION_H
#define RECONSTRUCTION_H


#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <petscksp.h>

#ifdef PETSC_USE_REAL___FLOAT128
    #include <quadmath.h>
    #define SQRT(x) sqrtq((x))
#else
    #define SQRT(x) sqrt((x))
#endif

#include "StructTypes.h"

#define DIM_2_WLS  	  4
#define DIM_4_WLS  	 20
#define DIM_6_WLS 	 56
#define DIM_8_WLS 	120


void SetOps_Reconstruction( SolverOps* ops, METHOD_TYPE type );

/* ------------- Weighted Least-Squares -------------- */

PetscErrorCode Reconstruction2_WLS( SolverSettings* solver, MeshGrid* mesh, Stencil* stencil, 
                                    PetscInt* f, PetscReal* poly_cells, PetscReal* poly_faces );

PetscErrorCode Reconstruction4_WLS( SolverSettings* solver, MeshGrid* mesh, Stencil* stencil, 
                                    PetscInt* f, PetscReal* poly_cells, PetscReal* poly_faces );

PetscErrorCode Reconstruction6_WLS( SolverSettings* solver, MeshGrid* mesh, Stencil* stencil, 
                                    PetscInt* f, PetscReal* poly_cells, PetscReal* poly_faces );

PetscErrorCode Reconstruction8_WLS( SolverSettings* solver, MeshGrid* mesh, Stencil* stencil, 
                                    PetscInt* f, PetscReal* poly_cells, PetscReal* poly_faces );


#endif // !RECONSTRUCTION_H