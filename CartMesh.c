#include "CartMesh.h"

#define QUIET
// #define DEBUG

void CartMesh( SolverSettings* solver, MeshGrid* mesh )
{

    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\nGENERATING GRID\n\tstarted ...\n");
    #endif // !QUIET


    /* CALCULATE GENERAL MESH VARIABLES */
    mesh->cell_side         = (solver->cell_side);
    mesh->vert_side         = (solver->cell_side) + 1;

    mesh->cs2               = (mesh->cell_side) * (mesh->cell_side);
    mesh->vs2               = (mesh->vert_side) * (mesh->vert_side);
    mesh->csXvs             = (mesh->cell_side) * (mesh->vert_side);
    mesh->fn_3              = (mesh->cell_side) * (mesh->cell_side) * (mesh->vert_side);

    mesh->cell_num          = (mesh->cell_side) * (mesh->cell_side) * (mesh->cell_side);
    mesh->vert_num          = (mesh->vert_side) * (mesh->vert_side) * (mesh->vert_side);
    mesh->face_num          = 3 * (mesh->fn_3);

    mesh->Lref              = 1. / (mesh->cell_side);
    mesh->_2_Lref2          = 4  * mesh->cs2;                // (2/Lref)^2
    mesh->_2_Lref3          = 8  * mesh->cell_num;           // (2/Lref)^3
    mesh->area              = (mesh->Lref) * (mesh->Lref);
    mesh->vol               = (mesh->Lref) * (mesh->Lref) * (mesh->Lref);



    return;
}



void setMeshDMSize( MeshGrid* mesh, DM dm )
{
    DMDAGetCorners( dm, &(mesh->ci_start), &(mesh->cj_start), &(mesh->ck_start),
                    &(mesh->cm), &(mesh->cn), &(mesh->cp) );
    
    mesh->ci_end = mesh->ci_start + mesh->cm;
    mesh->cj_end = mesh->cj_start + mesh->cn;
    mesh->ck_end = mesh->ck_start + mesh->cp;

    mesh->cell_num_local = mesh->cm * mesh->cn * mesh->cp;

    return;
}



void getCellIJK( MeshGrid* mesh, PetscInt* c)
{
    /* c[4] = {i, j, k, c} */

    c[0] = (c[3] / mesh->cs2);
    c[1] = (c[3] / mesh->cell_side) % mesh->cell_side;
    c[2] =  c[3] % mesh->cell_side;

    return;
}



void getFaceIJK( MeshGrid* mesh, PetscInt* f )
{
    /* f[4] = {i, j, k, f} */
    
    /* x-normal faces */
    if (f[3] < mesh->fn_3)
    {
        f[0] =  f[3] / mesh->cs2;
        f[1] = (f[3] / mesh->cell_side) % mesh->cell_side;
        f[2] =  f[3] % mesh->cell_side;
    }

    /* y-normal faces */
    else if (f[3] < 2*mesh->fn_3)
    {
        PetscInt aux_f = f[3] - mesh->fn_3;
        f[0] =  aux_f / mesh->csXvs;
        f[1] = (aux_f / mesh->cell_side) % mesh->vert_side;
        f[2] =  aux_f % mesh->cell_side;
    }

    /* z-normal faces */
    else
    {
        PetscInt aux_f = f[3] - 2*mesh->fn_3;
        f[0] =  aux_f / mesh->csXvs;
        f[1] = (aux_f / mesh->vert_side) % mesh->cell_side;
        f[2] =  aux_f % mesh->vert_side;
    }

    return;
}



void getCellCoords( MeshGrid* mesh, PetscInt* c, PetscReal* xyz )
{
    /* c[4] = {i, j, k, c} */

    xyz[0] = mesh->Lref * ( .5  +  c[0] );
    xyz[1] = mesh->Lref * ( .5  +  c[1] );
    xyz[2] = mesh->Lref * ( .5  +  c[2] );

    return;
}



void getFaceCoords( MeshGrid* mesh, PetscInt* f, PetscReal* xyz )
{
    /* f[4] = {i, j, k, f} */
    
    /* x-normal faces */
    if (f[3] < mesh->fn_3)
    {
        xyz[0] = mesh->Lref * f[0];
        xyz[1] = mesh->Lref * ( .5  +  f[1] );
        xyz[2] = mesh->Lref * ( .5  +  f[2] );
    }

    /* y-normal faces */
    else if (f[3] < 2*mesh->fn_3)
    {
        xyz[0] = mesh->Lref * ( .5  +  f[0] );
        xyz[1] = mesh->Lref * f[1];
        xyz[2] = mesh->Lref * ( .5  +  f[2] );
    }

    /* z-normal faces */
    else
    {
        xyz[0] = mesh->Lref * ( .5  +  f[0] );
        xyz[1] = mesh->Lref * ( .5  +  f[1] );
        xyz[2] = mesh->Lref * f[2];
    }

    return;
}



/*  updates an array with the vertices of face 'f', 
    where the ordering follows right-hand rule for face-normal axis
*/
void getFaceVerts( MeshGrid* mesh, PetscInt* f, PetscInt* face_verts )
{
    /* notes: dx=vs^2, dy=vs, dz=1 */
    /* f[4] = {i, j, k, f} */


    /* x-normal faces */
    if (f[3] < mesh->fn_3)
    {
        face_verts[0] =  f[0]*mesh->vs2   +    f[1]   *mesh->vert_side   +   f[2];          //  ( i,    j,    k )
        face_verts[1] =  f[0]*mesh->vs2   +    f[1]   *mesh->vert_side   +   f[2]+1;        //  ( i,    j, dz+k )
        face_verts[2] =  f[0]*mesh->vs2   +   (f[1]+1)*mesh->vert_side   +   f[2]+1;        //  ( i, dy+j, dz+k )
        face_verts[3] =  f[0]*mesh->vs2   +   (f[1]+1)*mesh->vert_side   +   f[2];          //  ( i, dy+j,    k )
    }


    /* y-normal faces */
    else if (f[3] < 2*mesh->fn_3)
    {
        face_verts[0] =   f[0]   *mesh->vs2   +   f[1]*mesh->vert_side   +   f[2];          //  (    i, j,    k )
        face_verts[1] =  (f[0]+1)*mesh->vs2   +   f[1]*mesh->vert_side   +   f[2];          //  ( dx+i, j,    k )
        face_verts[2] =  (f[0]+1)*mesh->vs2   +   f[1]*mesh->vert_side   +   f[2]+1;        //  ( dx+i, j, dz+k )
        face_verts[3] =   f[0]   *mesh->vs2   +   f[1]*mesh->vert_side   +   f[2]+1;        //  (    i, j, dz+k )
    }
    

    /* z-normal faces */
    else
    {
        face_verts[0] =   f[0]   *mesh->vs2   +    f[1]   *mesh->vert_side   +   f[2];      //  (    i,    j, k )
        face_verts[1] =   f[0]   *mesh->vs2   +   (f[1]+1)*mesh->vert_side   +   f[2];      //  (    i, dy+j, k )
        face_verts[2] =  (f[0]+1)*mesh->vs2   +   (f[1]+1)*mesh->vert_side   +   f[2];      //  ( dx+i, dy+j, k )
        face_verts[3] =  (f[0]+1)*mesh->vs2   +    f[1]   *mesh->vert_side   +   f[2];      //  ( dx+i,    j, k )
    }


    return;
}



/*  updates an array with the neighboring cells of face 'f',
    where a value of -1 means that that cell does not exist
*/
void getFaceCells( MeshGrid* mesh, PetscInt* f, PetscInt* face_cells )
{
    /* f[4] = {i, j, k, f} */

    /* x-normal */
    if (f[3] < mesh->fn_3)
    {
        /* west */
        if (f[3] < mesh->cs2)
        {
            face_cells[0] = -1;
            face_cells[1] = f[3];
        }
        /* inner */
        else if (f[3] < mesh->cell_num)
        {
            face_cells[0] = f[3] - mesh->cs2;
            face_cells[1] = f[3];
        }
        /* east */
        else
        {
            face_cells[0] = f[3] - mesh->cs2;
            face_cells[1] = -1;
        }
    }


    /* y-normal */
    else if (f[3] < 2*mesh->fn_3)
    {
        /* south */
        if (f[1] == 0)
        {
            face_cells[0] = -1;
            face_cells[1] = f[0]*mesh->cs2  +  f[2];
        }
        /* inner */
        else if (f[1] < mesh->cell_side)
        {
            face_cells[0] =  f[0]*mesh->cs2  +  (f[1]-1)*mesh->cell_side  +  f[2];
            face_cells[1] =  f[0]*mesh->cs2  +   f[1]   *mesh->cell_side  +  f[2];
        }
        /* north */
        else
        {
            face_cells[0] =  f[0]*mesh->cs2  +  (f[1]-1)*mesh->cell_side  +  f[2];
            face_cells[1] = -1;
        }
    }

    
    /* z-normal */
    else
    {
        /* bottom */
        if (f[2] == 0)
        {
            face_cells[0] = -1;
            face_cells[1] = f[0]*mesh->cs2  +  f[1]*mesh->cell_side;
        }
        /* inner */
        else if (f[2] < mesh->cell_side)
        {
            face_cells[0] =  f[0]*mesh->cs2  +  f[1]*mesh->cell_side  +  f[2]-1;
            face_cells[1] =  f[0]*mesh->cs2  +  f[1]*mesh->cell_side  +  f[2];
        }
        /* top */
        else
        {
            face_cells[0] =  f[0]*mesh->cs2  +  f[1]*mesh->cell_side  +  f[2]-1;
            face_cells[1] = -1;
        }
    }


    return;
}



/*  updates an array the neighboring cells of vert 'v', in the following order:
    vert_cells = {c_WSB, c_WST, c_WNB, c_WNT, c_ESB, c_EST, c_ENB, c_ENT},
    where a value of -1 means that that cell does not exist
*/
void getVertCells( MeshGrid* mesh, PetscInt v, PetscInt* vert_cells )
{

    /* AUX VARIABLES */
    PetscInt    i, j, k;            // vertex indexing
    PetscInt    e, w, n, s, t, b;   // cell count contribution

    i =  v / mesh->vs2;
    j = (v / mesh->vert_side) % mesh->vert_side;
    k =  v % mesh->vert_side;

    e = i * mesh->cs2;
    n = j * mesh->cell_side;
    t = k;

    w = e - mesh->cs2;
    s = n - mesh->cell_side;
    b = t - 1;    


    /* CALCULATE COMMON-CASE VALUES (i.e. for a inner vertex) */
    vert_cells[0] =  w  +  s  +  b;
    vert_cells[1] =  w  +  s  +  t;
    vert_cells[2] =  w  +  n  +  b;
    vert_cells[3] =  w  +  n  +  t;
    vert_cells[4] =  e  +  s  +  b;
    vert_cells[5] =  e  +  s  +  t;
    vert_cells[6] =  e  +  n  +  b;
    vert_cells[7] =  e  +  n  +  t;


    /* CORRECT BOUNDARY-CASE VALUES */
    /* west */
    if (i == 0)
    {
        vert_cells[0] = -1;
        vert_cells[1] = -1;
        vert_cells[2] = -1;
        vert_cells[3] = -1;
    }
    /* east */
    else if (i == mesh->cell_side)
    {
        vert_cells[4] = -1;
        vert_cells[5] = -1;
        vert_cells[6] = -1;
        vert_cells[7] = -1;
    }

    /* south */
    if (j == 0)
    {
        vert_cells[0] = -1;
        vert_cells[1] = -1;
        vert_cells[4] = -1;
        vert_cells[5] = -1;
    }
    /* north */
    else if (j == mesh->cell_side)
    {
        vert_cells[2] = -1;
        vert_cells[3] = -1;
        vert_cells[6] = -1;
        vert_cells[7] = -1;
    }

    /* bottom */
    if (k == 0)
    {
        vert_cells[0] = -1;
        vert_cells[2] = -1;
        vert_cells[4] = -1;
        vert_cells[6] = -1;
    }
    /* top */
    else if (k == mesh->cell_side)
    {
        vert_cells[1] = -1;
        vert_cells[3] = -1;
        vert_cells[5] = -1;
        vert_cells[7] = -1;
    }



    return;
}



/*  updates an array the faces of cell 'c', in the following order:
    cell_faces = {f_W, f_E, f_S, f_N, f_B, f_T}
*/
void getCellFaces( MeshGrid* mesh, PetscInt* c, PetscInt* cell_faces )
{
    
    /* c[4] = {i, j, k, c} */


    /* x-normal faces */
    cell_faces[0] = c[3];
    cell_faces[1] = c[3] + mesh->cs2;

    /* y-normal faces */
    cell_faces[2] =  mesh->fn_3  +  c[0]*mesh->csXvs  +  c[1]*mesh->cell_side  +  c[2];
    cell_faces[3] =  mesh->fn_3  +  c[0]*mesh->csXvs  +  c[1]*mesh->cell_side  +  c[2] + mesh->cell_side;

    /* z-normal faces */
    cell_faces[4] =  2*mesh->fn_3  +  c[0]*mesh->csXvs  +  c[1]*mesh->vert_side  +  c[2];
    cell_faces[5] =  2*mesh->fn_3  +  c[0]*mesh->csXvs  +  c[1]*mesh->vert_side  +  c[2] + 1;


    return;
}



/*  checks if face 'f' is a boundary face
    returns:
        * PETSC_TRUE, if face 'f' is a boundary face
        * PETSC_FALSE, otherwise
*/
PetscBool isBndFace( MeshGrid* mesh, PetscInt* f )
{
    /* f[4] = {i, j, k, f} */

    /* x-normal */
    if (f[3] < mesh->fn_3)
    {
        if ( f[0] == 0  ||  f[0] == mesh->cell_side )
            return PETSC_TRUE;
        else
            return PETSC_FALSE;
    }


    /* y-normal */
    else if (f[3] < 2*mesh->fn_3)
    {
        if ( f[1] == 0  ||  f[1] == mesh->cell_side )
            return PETSC_TRUE;
        else
            return PETSC_FALSE;
    }


    /* z-normal */
    else
    {
        if ( f[2] == 0  ||  f[2] == mesh->cell_side )
            return PETSC_TRUE;
        else
            return PETSC_FALSE;
    }

}



/*  checks if cell 'c' has a boundary face
    returns:
        *  0 (000), if inner cell
        *  1 (001), if x-boundary only
        *  2 (010), if y-boundary only
        *  3 (011), if y- and x-boundary
        *  4 (100), if z-boundary only
        *  5 (101), if z- and x-boundary
        *  6 (110), if z- and y-boundary
        *  7 (111), if corner cell
    use method:
        * is inner cell:  ans == 0, or !isBndCell()
        * is x-boundary:  ans%2 == 1
        * is y-boundary:  ans%4 > 1
        * is z-boundary:  ans > 3
    */
PetscInt isBndCell( MeshGrid* mesh, PetscInt* c )
{

    /* c[4] = {i, j, k, c} */
    PetscInt    ans = 0;


    /* x-boundary */
    if ( c[0] == 0  ||  c[0] == mesh->cell_side-1 )
        ans += 1;


    /* y-boundary */
    if ( c[1] == 0  ||  c[1] == mesh->cell_side-1 )
        ans += 2;

    
    /* z-boundary */
    if ( c[2] == 0  ||  c[2] == mesh->cell_side-1 )
        ans += 4;


    return ans;
}



/*  determines how many stencils must be actively calculated,
    based on the process corner indices and boundary width 'bw'
    i.e. total = all boundary stencils + 1 inner stencil            */
PetscInt totalUniqueStencil( MeshGrid* mesh, PetscInt bw )
{


    /* aux variables */
    PetscInt    total = 0;
    PetscBool   west, east, 
                north, south, 
                bottom, top;


    /* determine which boundaries the process sub-division has */
    west   = ( mesh->ci_start == 0  ?  PETSC_TRUE  :  PETSC_FALSE );
    south  = ( mesh->cj_start == 0  ?  PETSC_TRUE  :  PETSC_FALSE );
    bottom = ( mesh->ck_start == 0  ?  PETSC_TRUE  :  PETSC_FALSE );
    east  = ( mesh->ci_end == mesh->cell_side  ?  PETSC_TRUE  :  PETSC_FALSE );
    north = ( mesh->cj_end == mesh->cell_side  ?  PETSC_TRUE  :  PETSC_FALSE );
    top   = ( mesh->ck_end == mesh->cell_side  ?  PETSC_TRUE  :  PETSC_FALSE );


    /* ADD CONTRIBUTIONS */
#ifdef DEBUG
PetscInt aux=total;
PetscPrintf(PETSC_COMM_WORLD," total=");
#endif // DEBUG

    /* x-bnds */
    if (west)
        total += bw * mesh->cp * mesh->cn;
#ifdef DEBUG
aux=total-aux;
PetscPrintf(PETSC_COMM_WORLD,"%d+",aux);
aux=total;
#endif // DEBUG
    if (east)
        total += bw * mesh->cp * mesh->cn;
#ifdef DEBUG
aux=total-aux;
PetscPrintf(PETSC_COMM_WORLD,"%d+",aux);
aux=total;
#endif // DEBUG

    /* y-bnds */
    if (south)
    {
        if (west && east)
            total +=  bw  *  mesh->cp  *  (mesh->cm - 2*bw);
        else if (west || east)
            total +=  bw  *  mesh->cp  *  (mesh->cm - bw);
        else
            total +=  bw  *  mesh->cp  *  mesh->cm;
    }
#ifdef DEBUG
aux=total-aux;
PetscPrintf(PETSC_COMM_WORLD,"%d+",aux);
aux=total;
#endif // DEBUG
    if (north)
    {
        if (west && east)
            total +=  bw  *  mesh->cp  *  (mesh->cm - 2*bw);
        else if (west || east)
            total +=  bw  *  mesh->cp  *  (mesh->cm - bw);
        else
            total +=  bw  *  mesh->cp  *  mesh->cm;
    }
#ifdef DEBUG
aux=total-aux;
PetscPrintf(PETSC_COMM_WORLD,"%d+",aux);
aux=total;
#endif // DEBUG

    /* z-bnds */
    if (bottom)
    {
        if (south && north)
        {
            if (west && east)
                total +=  bw  *  (mesh->cn - 2*bw)  *  (mesh->cm - 2*bw);
            else if (west || east)
                total +=  bw  *  (mesh->cn - 2*bw)  *  (mesh->cm - bw);
            else
                total +=  bw  *  (mesh->cn - 2*bw)  *  mesh->cm;
        }
        else if (south || north)
        {
            if (west && east)
                total +=  bw  *  (mesh->cn - bw)  *  (mesh->cm - 2*bw);
            else if (west || east)
                total +=  bw  *  (mesh->cn - bw)  *  (mesh->cm - bw);
            else
                total +=  bw  *  (mesh->cn - bw)  *  mesh->cm;
        }
        else
        {
            if (west && east)
                total +=  bw  *  mesh->cn  *  (mesh->cm - 2*bw);
            else if (west || east)
                total +=  bw  *  mesh->cn  *  (mesh->cm - bw);
            else
                total +=  bw  *  mesh->cn  *  mesh->cm;
        }
    }
#ifdef DEBUG
aux=total-aux;
PetscPrintf(PETSC_COMM_WORLD,"%d+",aux);
aux=total;
#endif // DEBUG
    if (top)
    {
        if (south && north)
        {
            if (west && east)
                total +=  bw  *  (mesh->cn - 2*bw)  *  (mesh->cm - 2*bw);
            else if (west || east)
                total +=  bw  *  (mesh->cn - 2*bw)  *  (mesh->cm - bw);
            else
                total +=  bw  *  (mesh->cn - 2*bw)  *  mesh->cm;
        }
        else if (south || north)
        {
            if (west && east)
                total +=  bw  *  (mesh->cn - bw)  *  (mesh->cm - 2*bw);
            else if (west || east)
                total +=  bw  *  (mesh->cn - bw)  *  (mesh->cm - bw);
            else
                total +=  bw  *  (mesh->cn - bw)  *  mesh->cm;
        }
        else
        {
            if (west && east)
                total +=  bw  *  mesh->cn  *  (mesh->cm - 2*bw);
            else if (west || east)
                total +=  bw  *  mesh->cn  *  (mesh->cm - bw);
            else
                total +=  bw  *  mesh->cn  *  mesh->cm;
        }
    }
#ifdef DEBUG
aux=total-aux;
PetscPrintf(PETSC_COMM_WORLD,"%d=%d",aux,total);
#endif // DEBUG

    return total;
}



/*  map face from global numbering system to boundary-face numbering system */
PetscInt faceGlobal2Bnd( MeshGrid* mesh, PetscInt f )
{
    
    /* west */
    if (f < mesh->cs2)
        return (f);
    /* east */
    else if (f < mesh->fn_3)
        return (f + 2*mesh->cs2 - mesh->fn_3);

    /* south, north */
    else if (f < 2*mesh->fn_3)
    {
        PetscInt aux_f = f - mesh->fn_3;
        /* south */
        if (aux_f / mesh->csXvs == 0)
            return ( 2*mesh->cs2 + f%mesh->csXvs 
                    + 2*((aux_f/mesh->cell_side)%mesh->vert_side)*mesh->cell_side );
        /* north */
        else
            return ( 2*mesh->cs2 + f%mesh->csXvs 
                    + 2*((aux_f/mesh->cell_side)%mesh->vert_side)*mesh->cell_side 
                    - (mesh->csXvs-2*mesh->cell_side) );
    }

    /* bottom, top */
    else
    {
        PetscInt aux_f = f - 2*mesh->fn_3;
        /* bottom */
        if (aux_f % mesh->vert_side == 0)
            return (4*mesh->cs2 + 2*((aux_f-2*mesh->fn_3)/mesh->vert_side));
        /* top */
        else
            return (4*mesh->cs2 + 2*((aux_f-2*mesh->fn_3)/mesh->vert_side));
    }

}

