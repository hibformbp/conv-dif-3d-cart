#ifndef WEIGHTEDLEASTSQUARES_H
#define WEIGHTEDLEASTSQUARES_H


#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <petscksp.h>
// #if defined(PETSC_HAVE_HDF5)
// #include <petscviewerhdf5.h>
// #endif

#ifdef PETSC_USE_REAL___FLOAT128
#include <quadmath.h>
#endif

#include "StructTypes.h"


PetscErrorCode WeightedLeastSquares( SolverSettings* solver, MeshGrid* mesh, Solution* sol );


#endif // !WEIGHTEDLEASTSQUARES_H