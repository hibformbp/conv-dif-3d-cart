#!/bin/bash

# if no arguments are provided
if [[ $# -lt 2 ]]; then
    echo "Insufficient arguments"
    echo "Usage: ./run-batch-SIDE-small.sh <NPROCS> <METHOD> [-stencil STENCIL] [-u_conv U_CONV] [-pctype PCTYPE]"
    exit
fi

# if asked for help
declare -A helpstr=( [--help]=1 [-help]=1 [-h]=1 )
if [[ ${helpstr[$1]} ]]; then
    echo "Usage: ./run-batch-SIDE-small.sh <NPROCS> <METHOD> [-stencil STENCIL] [-u_conv U_CONV] [-pctype PCTYPE]"
    exit
fi

# default args
NPROCS=$1; shift
METHOD=$1; shift
STENCIL="face"
U_CONV="1,1,1"
PCTYPE="gamg(2)"

# get user options
while [[ $# -gt 0 ]]; do 
    case $1 in
        -stencil | stencil )
            shift; STENCIL=$1
            ;;
        -u_conv | u_conv )
            shift; U_CONV=$1
            ;;
        -pctype | pctype )
            shift; PCTYPE=$1
            ;;
        * )
            echo "Unknown option '${1}'"
            echo "Usage: ./run-batch-SIDE-small.sh <NPROCS> <METHOD> [STENCIL] [U_CONV] [PCTYPE]"
            exit
            ;;
    esac
    shift
done

echo "Checking for SIDE directory..."
if [[ -e SIDE ]]; then
    echo "found..."
else
    echo "not found, creating..."
    mkdir SIDE
fi

echo "-------------------------- Running SIDE-type test batch --------------------------"
echo "------------------------------------ SIDE=10 -------------------------------------"
mpirun -n $NPROCS ./solver3d -output SIDE/"$METHOD"-010-n"$NPROCS".out -cell_side 10 \
        -method $METHOD -stencil $STENCIL -u_conv $U_CONV -pctype $PCTYPE

echo "------------------------------------ SIDE=20 -------------------------------------"
mpirun -n $NPROCS ./solver3d -output SIDE/"$METHOD"-020-n"$NPROCS".out -cell_side 20 \
        -method $METHOD -stencil $STENCIL -u_conv $U_CONV -pctype $PCTYPE

echo "------------------------------------ SIDE=40 -------------------------------------"
mpirun -n $NPROCS ./solver3d -output SIDE/"$METHOD"-040-n"$NPROCS".out -cell_side 40 \
        -method $METHOD -stencil $STENCIL -u_conv $U_CONV -pctype $PCTYPE

echo "------------------------------------ SIDE=80 -------------------------------------"
mpirun -n $NPROCS ./solver3d -output SIDE/"$METHOD"-080-n"$NPROCS".out -cell_side 80 \
        -method $METHOD -stencil $STENCIL -u_conv $U_CONV -pctype $PCTYPE

echo "--------------------------- Done! See results in SIDE/ ---------------------------"