#ifndef STENCIL_H
#define STENCIL_H


#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <petscsystypes.h>

#ifdef PETSC_USE_REAL___FLOAT128
#include <quadmath.h>
#endif

#include "StructTypes.h"

#define N_CELLS_PER_VERT     8
#define N_VERTS_PER_FACE     4
#define N_FACES_PER_CELL     6


void StencilSetTypeSizes( SolverSettings* solver, StencilSize* size );

PetscInt StencilFaceNBs( SolverSettings* solver, MeshGrid* mesh, Stencil* stencil, PetscInt* f );


#endif // !STENCIL_H