#include "CartMesh.h"
#include "Poly.h"
#include "Reconstruction.h"


void SetOps_Reconstruction( SolverOps* ops, METHOD_TYPE type )
{

    if (type == WLS_2)
        ops->reconstruction = Reconstruction2_WLS;

    else if (type == WLS_4)
        ops->reconstruction = Reconstruction4_WLS;

    else if (type == WLS_6)
        ops->reconstruction = Reconstruction6_WLS;

    else if (type == WLS_8)
        ops->reconstruction = Reconstruction8_WLS;
    
    return;
}

/* ------------- Weighted Least-Squares -------------- */

PetscErrorCode Reconstruction2_WLS( SolverSettings* solver, MeshGrid* mesh, Stencil* stencil, 
                                    PetscInt* f, PetscReal* poly_cells, PetscReal* poly_faces )
{

    PetscInt        i, j, k, s[4], idx[DIM_2_WLS];
    PetscReal       w, _w, aux_conv, Pe, dn, dta, dtb,   // d_normal, d_tangent
                    s_xyz[3], f_xyz[3],
                    aux[DIM_2_WLS], aux_w[DIM_2_WLS];
    BNDCOND_TYPE    bndcond_type;
    Mat             P, D, D1, DD1t, Imat;

    getFaceCoords( mesh, f, f_xyz);

    if (f[3] < mesh->fn_3)
        aux_conv = solver->u_conv->x;
    else if (f[3] < 2*mesh->fn_3)
        aux_conv = solver->u_conv->y;
    else
        aux_conv = solver->u_conv->z;

    /* NOTE: 'Pe' isn't really Peclet, but actually L/Pe */
    Pe = solver->gamma_diff / aux_conv;

    for (i = 0; i < DIM_2_WLS; i++)
        idx[i] = i;
    
    /* Create auxiliary matrices 'D', 'D1', 'Imat' */
    MatCreateSeqDense( PETSC_COMM_SELF, DIM_2_WLS, stencil->size->total, NULL, &D );
    MatCreateSeqDense( PETSC_COMM_SELF, DIM_2_WLS, stencil->size->total, NULL, &D1);
    MatCreateSeqDense( PETSC_COMM_SELF, DIM_2_WLS, DIM_2_WLS, NULL, &Imat );
    MatSetUp( D );
    MatSetUp( D1);
    MatSetUp( Imat );
    

    /* Populate dense identity matrix 'Imat' */
    MatZeroEntries( Imat );
    MatShift( Imat, 1.0 );
    MatAssemblyBegin( Imat, MAT_FINAL_ASSEMBLY );
    

    /* Build Matrices from Stencil Cells */
    for (i = 0; i < stencil->size->cells; i++)
    {
        /* Define 's' as current stencil cell */
        s[3] = stencil->cells[i];
        getCellIJK( mesh, s );
        getCellCoords( mesh, s, s_xyz );

        /* Calculate auxiliary values */
        if (f[3] < mesh->fn_3)
        {
            dn  = s_xyz[0] - f_xyz[0];
            dta = s_xyz[1] - f_xyz[1];
            dtb = s_xyz[2] - f_xyz[2];
        }
        else if (f[3] < 2*mesh->fn_3)
        {
            dtb = s_xyz[0] - f_xyz[0];
            dn  = s_xyz[1] - f_xyz[1];
            dta = s_xyz[2] - f_xyz[2];
        }
        else
        {
            dta = s_xyz[0] - f_xyz[0];
            dtb = s_xyz[1] - f_xyz[1];
            dn  = s_xyz[2] - f_xyz[2];
        }

        /* Calculate weight function */
        _w = 1. / (dn*dn + dta*dta + dtb*dtb);
        if (solver->weight % 2 == 0)
            w = 1.;
        else
            w = SQRT(_w);
        for (k = 2 + (solver->weight%2); k <= solver->weight; k+=2)
            w *= _w;

        /* Calculate row 'i' of Matrix 'D' */
        aux[0] = 1.;
        aux[1] = dn;
        aux[2] = dta;
        aux[3] = dtb;

        /* Calculate row 'i' of Matrix 'D1' */
        aux_w[0] = w;
        aux_w[1] = w*dn;
        aux_w[2] = w*dta;
        aux_w[3] = w*dtb;

        /* Insert values into matrices */
        MatSetValues(  D, DIM_2_WLS, idx, 1, &i,   aux, INSERT_VALUES);
        MatSetValues( D1, DIM_2_WLS, idx, 1, &i, aux_w, INSERT_VALUES);
    }
    
    
    /* Build Matrices from Stencil Faces */
    for (j = 0; j < stencil->size->faces; i++, j++)
    {
        /* Define 's' as current stencil face */
        s[3] = stencil->faces[j];
        getFaceIJK( mesh, s );
        getFaceCoords( mesh, s, s_xyz );

        /* Determine parent boundary of 's' */
        if (s[3] < mesh->cs2)
            bndcond_type = solver->bndcond->w;
        else if (s[3] < mesh->fn_3)
            bndcond_type = solver->bndcond->e;
        else if (s[3] < 2*mesh->fn_3)
        {
            if (s[1] == 0)
                bndcond_type = solver->bndcond->s;
            else
                bndcond_type = solver->bndcond->n;
        }
        else if (s[2] == 0)
            bndcond_type = solver->bndcond->b;
        else
            bndcond_type = solver->bndcond->t;

        /* Calculate auxiliary values */
        if (f[3] < mesh->fn_3)
        {
            dn  = s_xyz[0] - f_xyz[0];
            dta = s_xyz[1] - f_xyz[1];
            dtb = s_xyz[2] - f_xyz[2];
        }
        else if (f[3] < 2*mesh->fn_3)
        {
            dtb = s_xyz[0] - f_xyz[0];
            dn  = s_xyz[1] - f_xyz[1];
            dta = s_xyz[2] - f_xyz[2];
        }
        else
        {
            dta = s_xyz[0] - f_xyz[0];
            dtb = s_xyz[1] - f_xyz[1];
            dn  = s_xyz[2] - f_xyz[2];
        }

        /* Calculate weight function */
        if (s[3] != f[3])
            _w = 1. / (dn*dn + dta*dta + dtb*dtb);
        else
            _w = mesh->_2_Lref2;
        
        if (solver->weight % 2 == 0)
            w = 1.;
        else
            w = SQRT(_w);
        for (k = 2 + (solver->weight%2); k <= solver->weight; k+=2)
            w *= _w;

        /* Calculate row 'i' of Matrix 'D' and of Matrix 'D1' */
        if (bndcond_type == DIRICHLET)
        {
            aux[0] = 1.;
            aux[1] = dn;
            aux[2] = dta;
            aux[3] = dtb;

            aux_w[0] = w;
            aux_w[1] = w*dn;
            aux_w[2] = w*dta;
            aux_w[3] = w*dtb;
        }
        else if (bndcond_type == NEUMANN)
        {
            aux[0] = 0;
            aux[1] = mesh->area;
            aux[2] = 0;
            aux[3] = 0;

            aux_w[0] = 0;
            aux_w[1] = w * mesh->area;
            aux_w[2] = 0;
            aux_w[3] = 0;
        }
        else    // bncondtype == ROBIN
        {
            aux[0] = mesh->area;
            aux[1] = mesh->area * ( dn + Pe );
            aux[2] = mesh->area * ( dta );
            aux[3] = mesh->area * ( dtb );

            aux_w[0] = w * mesh->area;
            aux_w[1] = w * mesh->area * ( dn + Pe );
            aux_w[2] = w * mesh->area * ( dta );
            aux_w[3] = w * mesh->area * ( dtb );
        }

        /* Insert values into matrices */
        MatSetValues(  D, DIM_2_WLS, idx, 1, &i,   aux, INSERT_VALUES);
        MatSetValues( D1, DIM_2_WLS, idx, 1, &i, aux_w, INSERT_VALUES);
    }


    /* Assemble Matrices */
    MatAssemblyBegin( D, MAT_FINAL_ASSEMBLY );
    MatAssemblyBegin( D1, MAT_FINAL_ASSEMBLY );
    MatAssemblyEnd( Imat, MAT_FINAL_ASSEMBLY );
    MatAssemblyEnd( D, MAT_FINAL_ASSEMBLY );
    MatAssemblyEnd( D1, MAT_FINAL_ASSEMBLY );


    /* Calculate Polynomial Coeffs 'P' */
    MatMatTransposeMult( D, D1, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &DD1t );
    MatCholeskyFactor( DD1t, NULL, NULL );
    MatMatSolve( DD1t, Imat, Imat );
    MatMatMult( Imat, D1, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &P );


    /* Reconstruct Polynomial */
    Poly2_WLS( stencil, mesh, solver->gamma_diff, aux_conv, poly_cells, poly_faces, P );


    /* Clean up */
    MatDestroy( &Imat );
    MatDestroy( &DD1t);
    MatDestroy( &D1);
    MatDestroy( &D );
    MatDestroy( &P );

    return 0;
}

PetscErrorCode Reconstruction4_WLS( SolverSettings* solver, MeshGrid* mesh, Stencil* stencil, 
                                    PetscInt* f, PetscReal* poly_cells, PetscReal* poly_faces )
{

    PetscInt        i, j, k, s[4], idx[DIM_4_WLS];
    PetscReal       w, _w, aux_conv, Pe,
                    dn[4], dta[4], dtb[4],
                    s_xyz[3], f_xyz[3], 
                    aux[DIM_4_WLS], aux_w[DIM_4_WLS];
    BNDCOND_TYPE    bndcond_type;
    Mat             P, D, D1, DD1t, Imat;

    getFaceCoords( mesh, f, f_xyz );

    dn[0]  = 1;     // dn[i]  = d_normal^i
    dta[0] = 1;     // dta[i] = d_tangent_a^i
    dtb[0] = 1;     // dtb[i] = d_tangent_b^i

    if (f[3] < mesh->fn_3)
        aux_conv = solver->u_conv->x;
    else if (f[3] < 2*mesh->fn_3)
        aux_conv = solver->u_conv->y;
    else
        aux_conv = solver->u_conv->z;

    /* NOTE: 'Pe' isn't really Peclet, but actually L/Pe */
    Pe = solver->gamma_diff / aux_conv;

    for (i = 0; i < DIM_4_WLS; i++)
        idx[i] = i;
    
    
    /* Create auxiliary matrices 'D', 'D1', 'Imat' */
    MatCreateSeqDense( PETSC_COMM_SELF, DIM_4_WLS, stencil->size->total, NULL, &D );
    MatCreateSeqDense( PETSC_COMM_SELF, DIM_4_WLS, stencil->size->total, NULL, &D1);
    MatCreateSeqDense( PETSC_COMM_SELF, DIM_4_WLS, DIM_4_WLS, NULL, &Imat );
    MatSetUp( D );
    MatSetUp( D1);
    MatSetUp( Imat );

    /* Populate dense identity matrix 'Imat' */
    MatZeroEntries( Imat );
    MatShift( Imat, 1.0 );
    MatAssemblyBegin( Imat, MAT_FINAL_ASSEMBLY );

    /* Build Matrices from Stencil Cells */
    for (i = 0; i < stencil->size->cells; i++)
    {
        /* Define 's' as current stencil cell */
        s[3] = stencil->cells[i];
        getCellIJK( mesh, s );
        getCellCoords( mesh, s, s_xyz );

        /* Calculate auxiliary values */
        if (f[3] < mesh->fn_3)
        {
            dn[1]  = s_xyz[0] - f_xyz[0];
            dta[1] = s_xyz[1] - f_xyz[1];
            dtb[1] = s_xyz[2] - f_xyz[2];
        }
        else if (f[3] < 2*mesh->fn_3)
        {
            dtb[1] = s_xyz[0] - f_xyz[0];
            dn[1]  = s_xyz[1] - f_xyz[1];
            dta[1] = s_xyz[2] - f_xyz[2];
        }
        else
        {
            dta[1] = s_xyz[0] - f_xyz[0];
            dtb[1] = s_xyz[1] - f_xyz[1];
            dn[1]  = s_xyz[2] - f_xyz[2];
        }
        /* --- */
        dn[2] = dn[1]*dn[1];        // dn^2
        dn[3] = dn[1]*dn[2];        // dn^3
        /* --- */
        dta[2] = dta[1]*dta[1];     // dta^2
        dta[3] = dta[1]*dta[2];     // dta^3
        /* --- */
        dtb[2] = dtb[1]*dtb[1];     // dtb^2
        dtb[3] = dtb[1]*dtb[2];     // dtb^3

        /* Calculate weight function */
        _w = 1. / (dn[2] + dta[2] + dtb[2]);
        
        if (solver->weight % 2 == 0)
            w = 1.;
        else
        #ifdef PETSC_USE_REAL___FLOAT128
            w = sqrtq(_w);
        #else
            w = sqrt(_w);
        #endif
        for (k = 2 + (solver->weight%2); k <= solver->weight; k+=2)
            w *= _w;

        /* Calculate row 'i' of Matrix 'D' */
        /* --- 0 order --- */
        aux[ 0] = 1.;
        /* --- 1 order --- */
        aux[ 1] = dn[1];
        aux[ 2] =       dta[1];
        aux[ 3] =              dtb[1];
        /* --- 2 order --- */
        aux[ 4] = dn[2];
        aux[ 5] = dn[1]*dta[1];
        aux[ 6] = dn[1]*       dtb[1];
        aux[ 7] =       dta[2];
        aux[ 8] =       dta[1]*dtb[1];
        aux[ 9] =              dtb[2];
        /* --- 3 order --- */
        aux[10] = dn[3];
        aux[11] = dn[2]*dta[1];
        aux[12] = dn[2]*       dtb[1];
        aux[13] = dn[1]*dta[2];
        aux[14] = dn[1]*dta[1]*dtb[1];
        aux[15] = dn[1]*       dtb[2];
        aux[16] =       dta[3];
        aux[17] =       dta[2]*dtb[1];
        aux[18] =       dta[1]*dtb[2];
        aux[19] =              dtb[3];

        /* Calculate row 'i' of Matrix 'D1' */
        aux_w[ 0] = w;
        aux_w[ 1] = w*aux[ 1];
        aux_w[ 2] = w*aux[ 2];
        aux_w[ 3] = w*aux[ 3];
        aux_w[ 4] = w*aux[ 4];
        aux_w[ 5] = w*aux[ 5];
        aux_w[ 6] = w*aux[ 6];
        aux_w[ 7] = w*aux[ 7];
        aux_w[ 8] = w*aux[ 8];
        aux_w[ 9] = w*aux[ 9];
        aux_w[10] = w*aux[10];
        aux_w[11] = w*aux[11];
        aux_w[12] = w*aux[12];
        aux_w[13] = w*aux[13];
        aux_w[14] = w*aux[14];
        aux_w[15] = w*aux[15];
        aux_w[16] = w*aux[16];
        aux_w[17] = w*aux[17];
        aux_w[18] = w*aux[18];
        aux_w[19] = w*aux[19];

        /* Insert values into matrices */
        MatSetValues(  D, DIM_4_WLS, idx, 1, &i,   aux, INSERT_VALUES);
        MatSetValues( D1, DIM_4_WLS, idx, 1, &i, aux_w, INSERT_VALUES);
    }
    
    /* Build Matrices from Stencil Faces */
    for (j = 0; j < stencil->size->faces; i++, j++)
    {
        /* Define 's' as current stencil face */
        s[3] = stencil->faces[j];
        getFaceIJK( mesh, s );
        getFaceCoords( mesh, s, s_xyz );

        /* Determine parent boundary of 's' */
        if (s[3] < mesh->cell_side)
            bndcond_type = solver->bndcond->e;      // east
        else if (s[3] < mesh->fn_3)
            bndcond_type = solver->bndcond->w;      // west
        else if (s[3] < 2*mesh->fn_3)
        {
            if ((s[3]-mesh->fn_3)%mesh->csXvs < mesh->cell_side)
                bndcond_type = solver->bndcond->s;  // south
            else
                bndcond_type = solver->bndcond->n;  // north;
        }
        else if ((s[3]-2*mesh->fn_3)%mesh->vert_side == 0)
            bndcond_type = solver->bndcond->b;      // bottom
        else
            bndcond_type = solver->bndcond->t;      // top

        /* Calculate auxiliary values */
        if (f[3] < mesh->fn_3)
        {
            dn[1]  = s_xyz[0] - f_xyz[0];
            dta[1] = s_xyz[1] - f_xyz[1];
            dtb[1] = s_xyz[2] - f_xyz[2];
        }
        else if (f[3] < 2*mesh->fn_3)
        {
            dtb[1] = s_xyz[0] - f_xyz[0];
            dn[1]  = s_xyz[1] - f_xyz[1];
            dta[1] = s_xyz[2] - f_xyz[2];
        }
        else
        {
            dta[1] = s_xyz[0] - f_xyz[0];
            dtb[1] = s_xyz[1] - f_xyz[1];
            dn[1]  = s_xyz[2] - f_xyz[2];
        }
        /* --- */
        dn[2] = dn[1]*dn[1];        // dn^2
        dn[3] = dn[1]*dn[2];        // dn^3
        /* --- */
        dta[2] = dta[1]*dta[1];     // dta^2
        dta[3] = dta[1]*dta[2];     // dta^3
        /* --- */
        dtb[2] = dtb[1]*dtb[1];     // dtb^2
        dtb[3] = dtb[1]*dtb[2];     // dtb^3

        /* Calculate weight function */
        if (s[3] != f[3])
            _w = 1. / (dn[2] + dta[2] + dtb[2]);
        else
            _w = mesh->_2_Lref2;
        if (solver->weight % 2 == 0)
            w = 1.;
        else
            w = SQRT(_w);
        for (k = 2 + (solver->weight%2); k <= solver->weight; k+=2)
            w *= _w;

        /* Calculate row 'i' of Matrix 'D' and of Matrix 'D1' */
        if (bndcond_type == DIRICHLET)
        {
            /* --- 0 order --- */
            aux[ 0] = 1.;
            /* --- 1 order --- */
            aux[ 1] = dn[1];
            aux[ 2] =       dta[1];
            aux[ 3] =              dtb[1];
            /* --- 2 order --- */
            aux[ 4] = dn[2];
            aux[ 5] = dn[1]*dta[1];
            aux[ 6] = dn[1]*       dtb[1];
            aux[ 7] =       dta[2];
            aux[ 8] =       dta[1]*dtb[1];
            aux[ 9] =              dtb[2];
            /* --- 3 order --- */
            aux[10] = dn[3];
            aux[11] = dn[2]*dta[1];
            aux[12] = dn[2]*       dtb[1];
            aux[13] = dn[1]*dta[2];
            aux[14] = dn[1]*dta[1]*dtb[1];
            aux[15] = dn[1]*       dtb[2];
            aux[16] =       dta[3];
            aux[17] =       dta[2]*dtb[1];
            aux[18] =       dta[1]*dtb[2];
            aux[19] =              dtb[3];

            aux_w[ 0] = w;
            aux_w[ 1] = w*aux[ 1];
            aux_w[ 2] = w*aux[ 2];
            aux_w[ 3] = w*aux[ 3];
            aux_w[ 4] = w*aux[ 4];
            aux_w[ 5] = w*aux[ 5];
            aux_w[ 6] = w*aux[ 6];
            aux_w[ 7] = w*aux[ 7];
            aux_w[ 8] = w*aux[ 8];
            aux_w[ 9] = w*aux[ 9];
            aux_w[10] = w*aux[10];
            aux_w[11] = w*aux[11];
            aux_w[12] = w*aux[12];
            aux_w[13] = w*aux[13];
            aux_w[14] = w*aux[14];
            aux_w[15] = w*aux[15];
            aux_w[16] = w*aux[16];
            aux_w[17] = w*aux[17];
            aux_w[18] = w*aux[18];
            aux_w[19] = w*aux[19];
        }
        else if (bndcond_type == NEUMANN)
        {
            /* --- 0 order --- */
            aux[ 0] = 0;
            /* --- 1 order --- */
            aux[ 1] = mesh->area;
            aux[ 2] = 0;
            aux[ 3] = 0;
            /* --- 2 order --- */
            aux[ 4] = mesh->area * 2*dn[1];
            aux[ 5] = mesh->area *         dta[1];
            aux[ 6] = mesh->area *                dtb[1];
            aux[ 7] = 0;
            aux[ 8] = 0;
            aux[ 9] = 0;
            /* --- 3 order --- */
            aux[10] = mesh->area * 3*dn[2];
            aux[11] = mesh->area * 2*dn[1]*dta[1];
            aux[12] = mesh->area * 2*dn[1]*       dtb[1];
            aux[13] = mesh->area *         dta[2];
            aux[14] = mesh->area *         dta[1]*dtb[1];
            aux[15] = mesh->area *                dtb[2];
            aux[16] = 0;
            aux[17] = 0;
            aux[18] = 0;
            aux[19] = 0;

            aux_w[ 0] = 0;
            aux_w[ 1] = w*aux[ 1];
            aux_w[ 2] = 0;
            aux_w[ 3] = 0;
            aux_w[ 4] = w*aux[ 4];
            aux_w[ 5] = w*aux[ 5];
            aux_w[ 6] = w*aux[ 6];
            aux_w[ 7] = 0;
            aux_w[ 8] = 0;
            aux_w[ 9] = 0;
            aux_w[10] = w*aux[10];
            aux_w[11] = w*aux[11];
            aux_w[12] = w*aux[12];
            aux_w[13] = w*aux[13];
            aux_w[14] = w*aux[14];
            aux_w[15] = w*aux[15];
            aux_w[16] = 0;
            aux_w[17] = 0;
            aux_w[18] = 0;
            aux_w[19] = 0;
        }
        else    // bndcond_type == ROBIN
        {
            /* --- 0 order --- */ 
            aux[ 0] = mesh->area *  ( 1                                                 );
            /* --- 1 order --- */ 
            aux[ 1] = mesh->area *  ( dn[1]                 +   Pe                      );
            aux[ 2] = mesh->area *  (       dta[1]                                      );
            aux[ 3] = mesh->area *  (              dtb[1]                               ); 
            /* --- 2 order --- */ 
            aux[ 4] = mesh->area *  ( dn[2]                 +  Pe*2*dn[1]               );
            aux[ 5] = mesh->area *  ( dn[1]*dta[1]          +  Pe*        dta[1]        );
            aux[ 6] = mesh->area *  ( dn[1]*       dtb[1]   +  Pe*               dtb[1] );
            aux[ 7] = mesh->area *  (       dta[2]                                      );
            aux[ 8] = mesh->area *  (       dta[1]*dtb[1]                               );
            aux[ 9] = mesh->area *  (              dtb[2]                               );
            /* --- 3 order --- */ 
            aux[10] = mesh->area *  ( dn[3]                 +  Pe*3*dn[2]               );
            aux[11] = mesh->area *  ( dn[2]*dta[1]          +  Pe*2*dn[1]*dta[1]        );
            aux[12] = mesh->area *  ( dn[2]*       dtb[1]   +  Pe*2*dn[1]*       dtb[1] );
            aux[13] = mesh->area *  ( dn[1]*dta[2]          +  Pe*        dta[2]        );
            aux[14] = mesh->area *  ( dn[1]*dta[1]*dtb[1]   +  Pe*        dta[1]*dtb[1] );
            aux[15] = mesh->area *  ( dn[1]*       dtb[2]   +  Pe*               dtb[2] );
            aux[16] = mesh->area *  (       dta[3]                                      );
            aux[17] = mesh->area *  (       dta[2]*dtb[1]                               );
            aux[18] = mesh->area *  (       dta[1]*dtb[2]                               );
            aux[19] = mesh->area *  (              dtb[3]                               );

            aux_w[ 0] = w;
            aux_w[ 1] = w*aux[ 1];
            aux_w[ 2] = w*aux[ 2];
            aux_w[ 3] = w*aux[ 3];
            aux_w[ 4] = w*aux[ 4];
            aux_w[ 5] = w*aux[ 5];
            aux_w[ 6] = w*aux[ 6];
            aux_w[ 7] = w*aux[ 7];
            aux_w[ 8] = w*aux[ 8];
            aux_w[ 9] = w*aux[ 9];
            aux_w[10] = w*aux[10];
            aux_w[11] = w*aux[11];
            aux_w[12] = w*aux[12];
            aux_w[13] = w*aux[13];
            aux_w[14] = w*aux[14];
            aux_w[15] = w*aux[15];
            aux_w[16] = w*aux[16];
            aux_w[17] = w*aux[17];
            aux_w[18] = w*aux[18];
            aux_w[19] = w*aux[19];
        }

        /* Insert values into matrices */
        MatSetValues(  D, DIM_4_WLS, idx, 1, &i,   aux, INSERT_VALUES);
        MatSetValues( D1, DIM_4_WLS, idx, 1, &i, aux_w, INSERT_VALUES);
    }


    /* Assemble Matrices */
    MatAssemblyBegin( D, MAT_FINAL_ASSEMBLY );
    MatAssemblyBegin( D1, MAT_FINAL_ASSEMBLY );
    MatAssemblyEnd( Imat, MAT_FINAL_ASSEMBLY );
    MatAssemblyEnd( D, MAT_FINAL_ASSEMBLY );
    MatAssemblyEnd( D1, MAT_FINAL_ASSEMBLY );


    /* Calculate Polynomial Coeffs 'P' */
    MatMatTransposeMult( D, D1, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &DD1t );
    // MatChop( DD1t, solver->reltol );
    MatCholeskyFactor( DD1t, NULL, NULL );
    MatMatSolve( DD1t, Imat, Imat );
    MatMatMult( Imat, D1, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &P );


    /* Reconstruct Polynomial */
    Poly4_WLS( stencil, mesh, solver->gamma_diff, aux_conv, poly_cells, poly_faces, P );


    /* Clean up */
    MatDestroy( &Imat );
    MatDestroy( &DD1t);
    MatDestroy( &D1);
    MatDestroy( &D );
    MatDestroy( &P );

    return 0;
}

PetscErrorCode Reconstruction6_WLS( SolverSettings* solver, MeshGrid* mesh, Stencil* stencil, 
                                    PetscInt* f, PetscReal* poly_cells, PetscReal* poly_faces )
{

    PetscInt        i, j, k, s[4], idx[DIM_6_WLS];
    PetscReal       aux_conv, Pe, w, _w,
                    dn[6], dta[6], dtb[6],
                    s_xyz[3], f_xyz[3],
                    aux[DIM_6_WLS], aux_w[DIM_6_WLS];
    BNDCOND_TYPE    bndcond_type;
    Mat             P, D, D1, DD1t, Imat;

    getFaceCoords( mesh, f, f_xyz );

    dn[0]  = 1;     // dn[i]  = d_normal^i
    dta[0] = 1;     // dta[i] = d_tangent_a^i
    dtb[0] = 1;     // dtb[i] = d_tangent_b^i

    if (f[3] < mesh->fn_3)
        aux_conv = solver->u_conv->x;
    else if (f[3] < 2*mesh->fn_3)
        aux_conv = solver->u_conv->y;
    else
        aux_conv = solver->u_conv->z;

    /* NOTE: 'Pe' isn't really Peclet, but actually L/Pe */
    Pe = solver->gamma_diff / aux_conv;

    for (i = 0; i < DIM_6_WLS; i++)
        idx[i] = i;
    
    /* Create auxiliary matrices 'D', 'D1', 'Imat' */
    MatCreateSeqDense( PETSC_COMM_SELF, DIM_6_WLS, stencil->size->total, NULL, &D );
    MatCreateSeqDense( PETSC_COMM_SELF, DIM_6_WLS, stencil->size->total, NULL, &D1 );
    MatCreateSeqDense( PETSC_COMM_SELF, DIM_6_WLS, DIM_6_WLS, NULL, &Imat );
    MatSetUp( D );
    MatSetUp( D1 );
    MatSetUp( Imat );
    

    /* Populate identity matrix 'Imat' */
    MatZeroEntries( Imat );
    MatShift( Imat, 1.0 );
    MatAssemblyBegin( Imat, MAT_FINAL_ASSEMBLY );
    

    /* Build Matrices from Stencil Cells */
    for (i = 0; i < stencil->size->cells; i++)
    {
        /* Define 's' as current stencil cell */
        s[3] = stencil->cells[i];
        getCellIJK( mesh, s );
        getCellCoords( mesh, s, s_xyz );

        /* Calculate auxiliary values */
        if (f[3] < mesh->fn_3)
        {
            dn[1]  = s_xyz[0] - f_xyz[0];
            dta[1] = s_xyz[1] - f_xyz[1];
            dtb[1] = s_xyz[2] - f_xyz[2];
        }
        else if (f[3] < 2*mesh->fn_3)
        {
            dtb[1] = s_xyz[0] - f_xyz[0];
            dn[1]  = s_xyz[1] - f_xyz[1];
            dta[1] = s_xyz[2] - f_xyz[2];
        }
        else
        {
            dta[1] = s_xyz[0] - f_xyz[0];
            dtb[1] = s_xyz[1] - f_xyz[1];
            dn[1]  = s_xyz[2] - f_xyz[2];
        }
        /* --- */
        dn[2] = dn[1]*dn[1];        // dn^2
        dn[3] = dn[1]*dn[2];        // dn^3
        dn[4] = dn[2]*dn[2];        // dn^4
        dn[5] = dn[2]*dn[3];        // dn^5
        /* --- */
        dta[2] = dta[1]*dta[1];     // dta^2
        dta[3] = dta[1]*dta[2];     // dta^3
        dta[4] = dta[2]*dta[2];     // dta^4
        dta[5] = dta[2]*dta[3];     // dta^5
        /* --- */
        dtb[2] = dtb[1]*dtb[1];     // dtb^2
        dtb[3] = dtb[1]*dtb[2];     // dtb^3
        dtb[4] = dtb[2]*dtb[2];     // dtb^4
        dtb[5] = dtb[2]*dtb[3];     // dtb^5

        /* Calculate weight function */
        _w = 1. / (dn[2] + dta[2] + dtb[2]);
        if (solver->weight % 2 == 0)
            w = 1.;
        else
        #ifdef PETSC_USE_REAL___FLOAT128
            w = sqrtq(_w);
        #else
            w = sqrt(_w);
        #endif
        for (k = 2 + (solver->weight%2); k <= solver->weight; k+=2)
            w *= _w;

        /* Calculate row 'i' of Matrix 'D' */
        /* --- 0 order --- */
        aux[ 0] = 1.;
        /* --- 1 order --- */
        aux[ 1] = dn[1];
        aux[ 2] =       dta[1];
        aux[ 3] =              dtb[1];
        /* --- 2 order --- */
        aux[ 4] = dn[2];
        aux[ 5] = dn[1]*dta[1];
        aux[ 6] = dn[1]*       dtb[1];
        aux[ 7] =       dta[2];
        aux[ 8] =       dta[1]*dtb[1];
        aux[ 9] =              dtb[2];
        /* --- 3 order --- */
        aux[10] = dn[3];
        aux[11] = dn[2]*dta[1];
        aux[12] = dn[2]*       dtb[1];
        aux[13] = dn[1]*dta[2];
        aux[14] = dn[1]*dta[1]*dtb[1];
        aux[15] = dn[1]*       dtb[2];
        aux[16] =       dta[3];
        aux[17] =       dta[2]*dtb[1];
        aux[18] =       dta[1]*dtb[2];
        aux[19] =              dtb[3];
        /* --- 4 order --- */
        aux[20] = dn[4];
        aux[21] = dn[3]*dta[1];
        aux[22] = dn[3]*       dtb[1];
        aux[23] = dn[2]*dta[2];
        aux[24] = dn[2]*dta[1]*dtb[1];
        aux[25] = dn[2]*       dtb[2];
        aux[26] = dn[1]*dta[3];
        aux[27] = dn[1]*dta[2]*dtb[1];
        aux[28] = dn[1]*dta[1]*dtb[2];
        aux[29] = dn[1]*       dtb[3];
        aux[30] =       dta[4];
        aux[31] =       dta[3]*dtb[1];
        aux[32] =       dta[2]*dtb[2];
        aux[33] =       dta[1]*dtb[3];
        aux[34] =              dtb[4];
        /* --- 5 order --- */
        aux[35] = dn[5];
        aux[36] = dn[4]*dta[1];
        aux[37] = dn[4]*       dtb[1];
        aux[38] = dn[3]*dta[2];
        aux[39] = dn[3]*dta[1]*dtb[1];
        aux[40] = dn[3]*       dtb[2];
        aux[41] = dn[2]*dta[3];
        aux[42] = dn[2]*dta[2]*dtb[1];
        aux[43] = dn[2]*dta[1]*dtb[2];
        aux[44] = dn[2]*       dtb[3];
        aux[45] = dn[1]*dta[4];
        aux[46] = dn[1]*dta[3]*dtb[1];
        aux[47] = dn[1]*dta[2]*dtb[2];
        aux[48] = dn[1]*dta[1]*dtb[3];
        aux[49] = dn[1]*       dtb[4];
        aux[50] =       dta[5];
        aux[51] =       dta[4]*dtb[1];
        aux[52] =       dta[3]*dtb[2];
        aux[53] =       dta[2]*dtb[3];
        aux[54] =       dta[1]*dtb[4];
        aux[55] =              dtb[5];

        /* Calculate row 'i' of Matrix 'D1' */
        aux_w[ 0] = w;
        aux_w[ 1] = w*aux[ 1];
        aux_w[ 2] = w*aux[ 2];
        aux_w[ 3] = w*aux[ 3];
        aux_w[ 4] = w*aux[ 4];
        aux_w[ 5] = w*aux[ 5];
        aux_w[ 6] = w*aux[ 6];
        aux_w[ 7] = w*aux[ 7];
        aux_w[ 8] = w*aux[ 8];
        aux_w[ 9] = w*aux[ 9];
        aux_w[10] = w*aux[10];
        aux_w[11] = w*aux[11];
        aux_w[12] = w*aux[12];
        aux_w[13] = w*aux[13];
        aux_w[14] = w*aux[14];
        aux_w[15] = w*aux[15];
        aux_w[16] = w*aux[16];
        aux_w[17] = w*aux[17];
        aux_w[18] = w*aux[18];
        aux_w[19] = w*aux[19];
        aux_w[20] = w*aux[20];
        aux_w[21] = w*aux[21];
        aux_w[22] = w*aux[22];
        aux_w[23] = w*aux[23];
        aux_w[24] = w*aux[24];
        aux_w[25] = w*aux[25];
        aux_w[26] = w*aux[26];
        aux_w[27] = w*aux[27];
        aux_w[28] = w*aux[28];
        aux_w[29] = w*aux[29];
        aux_w[30] = w*aux[30];
        aux_w[31] = w*aux[31];
        aux_w[32] = w*aux[32];
        aux_w[33] = w*aux[33];
        aux_w[34] = w*aux[34];
        aux_w[35] = w*aux[35];
        aux_w[36] = w*aux[36];
        aux_w[37] = w*aux[37];
        aux_w[38] = w*aux[38];
        aux_w[39] = w*aux[39];
        aux_w[40] = w*aux[40];
        aux_w[41] = w*aux[41];
        aux_w[42] = w*aux[42];
        aux_w[43] = w*aux[43];
        aux_w[44] = w*aux[44];
        aux_w[45] = w*aux[45];
        aux_w[46] = w*aux[46];
        aux_w[47] = w*aux[47];
        aux_w[48] = w*aux[48];
        aux_w[49] = w*aux[49];
        aux_w[50] = w*aux[50];
        aux_w[51] = w*aux[51];
        aux_w[52] = w*aux[52];
        aux_w[53] = w*aux[53];
        aux_w[54] = w*aux[54];
        aux_w[55] = w*aux[55];

        /* Insert values into matrices */
        MatSetValues(  D, DIM_6_WLS, idx, 1, &i,  aux,  INSERT_VALUES );
        MatSetValues( D1, DIM_6_WLS, idx, 1, &i, aux_w, INSERT_VALUES );
    }
    
    
    /* Build Matrices from Stencil Faces */
    for (j = 0; j < stencil->size->faces; i++, j++)
    {
        /* Define 's' as current stencil face */
        s[3] = stencil->faces[j];
        getFaceIJK( mesh, s );
        getFaceCoords( mesh, s, s_xyz );

        /* Determine parent boundary of 's' */
        if (s[3] < mesh->cell_side)
            bndcond_type = solver->bndcond->e;      // east
        else if (s[3] < mesh->fn_3)
            bndcond_type = solver->bndcond->w;      // west
        else if (s[3] < 2*mesh->fn_3)
        {
            if ((s[3]-mesh->fn_3)%mesh->csXvs < mesh->cell_side)
                bndcond_type = solver->bndcond->s;  // south
            else
                bndcond_type = solver->bndcond->n;  // north;
        }
        else if ((s[3]-2*mesh->fn_3)%mesh->vert_side == 0)
            bndcond_type = solver->bndcond->b;      // bottom
        else
            bndcond_type = solver->bndcond->t;      // top

        /* Calculate auxiliary values */
        if (f[3] < mesh->fn_3)
        {
            dn[1]  = s_xyz[0] - f_xyz[0];
            dta[1] = s_xyz[1] - f_xyz[1];
            dtb[1] = s_xyz[2] - f_xyz[2];
        }
        else if (f[3] < 2*mesh->fn_3)
        {
            dtb[1] = s_xyz[0] - f_xyz[0];
            dn[1]  = s_xyz[1] - f_xyz[1];
            dta[1] = s_xyz[2] - f_xyz[2];
        }
        else
        {
            dta[1] = s_xyz[0] - f_xyz[0];
            dtb[1] = s_xyz[1] - f_xyz[1];
            dn[1]  = s_xyz[2] - f_xyz[2];
        }
        /* --- */
        dn[2] = dn[1]*dn[1];        // dn^2
        dn[3] = dn[1]*dn[2];        // dn^3
        dn[4] = dn[2]*dn[2];        // dn^4
        dn[5] = dn[2]*dn[3];        // dn^5
        /* --- */
        dta[2] = dta[1]*dta[1];     // dta^2
        dta[3] = dta[1]*dta[2];     // dta^3
        dta[4] = dta[2]*dta[2];     // dta^4
        dta[5] = dta[2]*dta[3];     // dta^5
        /* --- */
        dtb[2] = dtb[1]*dtb[1];     // dtb^2
        dtb[3] = dtb[1]*dtb[2];     // dtb^3
        dtb[4] = dtb[2]*dtb[2];     // dtb^4
        dtb[5] = dtb[2]*dtb[3];     // dtb^5

        /* Calculate weight function */
        if (s[3] != f[3])
            _w = 1. / (dn[2] + dta[2] + dtb[2]);
        else
            _w = mesh->_2_Lref2;
        if (solver->weight % 2 == 0)
            w = 1.;
        else
            w = SQRT(_w);
        for (k = 2 + (solver->weight%2); k <= solver->weight; k+=2)
            w *= _w;

        /* Calculate row 'i' of Matrix 'D' and of Matrix 'D1' */
        if (bndcond_type == DIRICHLET)
        {
            /* --- 0 order --- */
            aux[ 0] = 1.;
            /* --- 1 order --- */
            aux[ 1] = dn[1];
            aux[ 2] =       dta[1];
            aux[ 3] =              dtb[1];
            /* --- 2 order --- */
            aux[ 4] = dn[2];
            aux[ 5] = dn[1]*dta[1];
            aux[ 6] = dn[1]*       dtb[1];
            aux[ 7] =       dta[2];
            aux[ 8] =       dta[1]*dtb[1];
            aux[ 9] =              dtb[2];
            /* --- 3 order --- */
            aux[10] = dn[3];
            aux[11] = dn[2]*dta[1];
            aux[12] = dn[2]*       dtb[1];
            aux[13] = dn[1]*dta[2];
            aux[14] = dn[1]*dta[1]*dtb[1];
            aux[15] = dn[1]*       dtb[2];
            aux[16] =       dta[3];
            aux[17] =       dta[2]*dtb[1];
            aux[18] =       dta[1]*dtb[2];
            aux[19] =              dtb[3];
            /* --- 4 order --- */
            aux[20] = dn[4];
            aux[21] = dn[3]*dta[1];
            aux[22] = dn[3]*       dtb[1];
            aux[23] = dn[2]*dta[2];
            aux[24] = dn[2]*dta[1]*dtb[1];
            aux[25] = dn[2]*       dtb[2];
            aux[26] = dn[1]*dta[3];
            aux[27] = dn[1]*dta[2]*dtb[1];
            aux[28] = dn[1]*dta[1]*dtb[2];
            aux[29] = dn[1]*       dtb[3];
            aux[30] =       dta[4];
            aux[31] =       dta[3]*dtb[1];
            aux[32] =       dta[2]*dtb[2];
            aux[33] =       dta[1]*dtb[3];
            aux[34] =              dtb[4];
            /* --- 5 order --- */
            aux[35] = dn[5];
            aux[36] = dn[4]*dta[1];
            aux[37] = dn[4]*       dtb[1];
            aux[38] = dn[3]*dta[2];
            aux[39] = dn[3]*dta[1]*dtb[1];
            aux[40] = dn[3]*       dtb[2];
            aux[41] = dn[2]*dta[3];
            aux[42] = dn[2]*dta[2]*dtb[1];
            aux[43] = dn[2]*dta[1]*dtb[2];
            aux[44] = dn[2]*       dtb[3];
            aux[45] = dn[1]*dta[4];
            aux[46] = dn[1]*dta[3]*dtb[1];
            aux[47] = dn[1]*dta[2]*dtb[2];
            aux[48] = dn[1]*dta[1]*dtb[3];
            aux[49] = dn[1]*       dtb[4];
            aux[50] =       dta[5];
            aux[51] =       dta[4]*dtb[1];
            aux[52] =       dta[3]*dtb[2];
            aux[53] =       dta[2]*dtb[3];
            aux[54] =       dta[1]*dtb[4];
            aux[55] =              dtb[5];

            aux_w[ 0] = w;
            aux_w[ 1] = w*aux[ 1];
            aux_w[ 2] = w*aux[ 2];
            aux_w[ 3] = w*aux[ 3];
            aux_w[ 4] = w*aux[ 4];
            aux_w[ 5] = w*aux[ 5];
            aux_w[ 6] = w*aux[ 6];
            aux_w[ 7] = w*aux[ 7];
            aux_w[ 8] = w*aux[ 8];
            aux_w[ 9] = w*aux[ 9];
            aux_w[10] = w*aux[10];
            aux_w[11] = w*aux[11];
            aux_w[12] = w*aux[12];
            aux_w[13] = w*aux[13];
            aux_w[14] = w*aux[14];
            aux_w[15] = w*aux[15];
            aux_w[16] = w*aux[16];
            aux_w[17] = w*aux[17];
            aux_w[18] = w*aux[18];
            aux_w[19] = w*aux[19];
            aux_w[20] = w*aux[20];
            aux_w[21] = w*aux[21];
            aux_w[22] = w*aux[22];
            aux_w[23] = w*aux[23];
            aux_w[24] = w*aux[24];
            aux_w[25] = w*aux[25];
            aux_w[26] = w*aux[26];
            aux_w[27] = w*aux[27];
            aux_w[28] = w*aux[28];
            aux_w[29] = w*aux[29];
            aux_w[30] = w*aux[30];
            aux_w[31] = w*aux[31];
            aux_w[32] = w*aux[32];
            aux_w[33] = w*aux[33];
            aux_w[34] = w*aux[34];
            aux_w[35] = w*aux[35];
            aux_w[36] = w*aux[36];
            aux_w[37] = w*aux[37];
            aux_w[38] = w*aux[38];
            aux_w[39] = w*aux[39];
            aux_w[40] = w*aux[40];
            aux_w[41] = w*aux[41];
            aux_w[42] = w*aux[42];
            aux_w[43] = w*aux[43];
            aux_w[44] = w*aux[44];
            aux_w[45] = w*aux[45];
            aux_w[46] = w*aux[46];
            aux_w[47] = w*aux[47];
            aux_w[48] = w*aux[48];
            aux_w[49] = w*aux[49];
            aux_w[50] = w*aux[50];
            aux_w[51] = w*aux[51];
            aux_w[52] = w*aux[52];
            aux_w[53] = w*aux[53];
            aux_w[54] = w*aux[54];
            aux_w[55] = w*aux[55];
        }
        else if (bndcond_type == NEUMANN)
        {
            /* --- 0 order --- */
            aux[ 0] = 0;
            /* --- 1 order --- */
            aux[ 1] = mesh->area;
            aux[ 2] = 0;
            aux[ 3] = 0;
            /* --- 2 order --- */
            aux[ 4] = mesh->area * 2*dn[1];
            aux[ 5] = mesh->area *         dta[1];
            aux[ 6] = mesh->area *                dtb[1];
            aux[ 7] = 0;
            aux[ 8] = 0;
            aux[ 9] = 0;
            /* --- 3 order --- */
            aux[10] = mesh->area * 3*dn[2];
            aux[11] = mesh->area * 2*dn[1]*dta[1];
            aux[12] = mesh->area * 2*dn[1]*       dtb[1];
            aux[13] = mesh->area *         dta[2];
            aux[14] = mesh->area *         dta[1]*dtb[1];
            aux[15] = mesh->area *                dtb[2];
            aux[16] = 0;
            aux[17] = 0;
            aux[18] = 0;
            aux[19] = 0;
            /* --- 4 order --- */
            aux[20] = mesh->area * 4*dn[3];
            aux[21] = mesh->area * 3*dn[2]*dta[1];
            aux[22] = mesh->area * 3*dn[2]*       dtb[1];
            aux[23] = mesh->area * 2*dn[1]*dta[2];
            aux[24] = mesh->area * 2*dn[1]*dta[1]*dtb[1];
            aux[25] = mesh->area * 2*dn[1]*       dtb[2];
            aux[26] = mesh->area *         dta[3];
            aux[27] = mesh->area *         dta[2]*dtb[1];
            aux[28] = mesh->area *         dta[1]*dtb[2];
            aux[29] = mesh->area *                dtb[3];
            aux[30] = 0;
            aux[31] = 0;
            aux[32] = 0;
            aux[33] = 0;
            aux[34] = 0;
            /* --- 5 order --- */
            aux[35] = mesh->area * 5*dn[4];
            aux[36] = mesh->area * 4*dn[3]*dta[1];
            aux[37] = mesh->area * 4*dn[3]*       dtb[1];
            aux[38] = mesh->area * 3*dn[2]*dta[2];
            aux[39] = mesh->area * 3*dn[2]*dta[1]*dtb[1];
            aux[40] = mesh->area * 3*dn[2]*       dtb[2];
            aux[41] = mesh->area * 2*dn[1]*dta[3];
            aux[42] = mesh->area * 2*dn[1]*dta[2]*dtb[1];
            aux[43] = mesh->area * 2*dn[1]*dta[1]*dtb[2];
            aux[44] = mesh->area * 2*dn[1]*       dtb[3];
            aux[45] = mesh->area *         dta[4];
            aux[46] = mesh->area *         dta[3]*dtb[1];
            aux[47] = mesh->area *         dta[2]*dtb[2];
            aux[48] = mesh->area *         dta[1]*dtb[3];
            aux[49] = mesh->area *                dtb[4];
            aux[50] = 0;
            aux[51] = 0;
            aux[52] = 0;
            aux[53] = 0;
            aux[54] = 0;
            aux[55] = 0;

            aux_w[ 0] = 0;
            aux_w[ 1] = w*aux[ 1];
            aux_w[ 2] = 0;
            aux_w[ 3] = 0;
            aux_w[ 4] = w*aux[ 4];
            aux_w[ 5] = w*aux[ 5];
            aux_w[ 6] = w*aux[ 6];
            aux_w[ 7] = 0;
            aux_w[ 8] = 0;
            aux_w[ 9] = 0;
            aux_w[10] = w*aux[10];
            aux_w[11] = w*aux[11];
            aux_w[12] = w*aux[12];
            aux_w[13] = w*aux[13];
            aux_w[14] = w*aux[14];
            aux_w[15] = w*aux[15];
            aux_w[16] = 0;
            aux_w[17] = 0;
            aux_w[18] = 0;
            aux_w[19] = 0;
            aux_w[20] = w*aux[20];
            aux_w[21] = w*aux[21];
            aux_w[22] = w*aux[22];
            aux_w[23] = w*aux[23];
            aux_w[24] = w*aux[24];
            aux_w[25] = w*aux[25];
            aux_w[26] = w*aux[26];
            aux_w[27] = w*aux[27];
            aux_w[28] = w*aux[28];
            aux_w[29] = w*aux[29];
            aux_w[30] = 0;
            aux_w[31] = 0;
            aux_w[32] = 0;
            aux_w[33] = 0;
            aux_w[34] = 0;
            aux_w[35] = w*aux[35];
            aux_w[36] = w*aux[36];
            aux_w[37] = w*aux[37];
            aux_w[38] = w*aux[38];
            aux_w[39] = w*aux[39];
            aux_w[40] = w*aux[40];
            aux_w[41] = w*aux[41];
            aux_w[42] = w*aux[42];
            aux_w[43] = w*aux[43];
            aux_w[44] = w*aux[44];
            aux_w[45] = w*aux[45];
            aux_w[46] = w*aux[46];
            aux_w[47] = w*aux[47];
            aux_w[48] = w*aux[48];
            aux_w[49] = w*aux[49];
            aux_w[50] = 0;
            aux_w[51] = 0;
            aux_w[52] = 0;
            aux_w[53] = 0;
            aux_w[54] = 0;
            aux_w[55] = 0;
        }
        else    // bndcond_type == ROBIN
        {
            /* TODO */
        }

        /* Insert values into matrices */
        MatSetValues(  D, DIM_6_WLS, idx, 1, &i,  aux,  INSERT_VALUES );
        MatSetValues( D1, DIM_6_WLS, idx, 1, &i, aux_w, INSERT_VALUES );
    }
    

    /* Assemble Matrices */
    MatAssemblyBegin(  D, MAT_FINAL_ASSEMBLY );
    MatAssemblyBegin( D1, MAT_FINAL_ASSEMBLY );
    MatAssemblyEnd( Imat, MAT_FINAL_ASSEMBLY );
    MatAssemblyEnd(    D, MAT_FINAL_ASSEMBLY );
    MatAssemblyEnd(   D1, MAT_FINAL_ASSEMBLY );


    /* Calculate Polynomial Coeffs 'P' */
    MatMatTransposeMult( D, D1, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &DD1t );
    // MatChop( DD1t, solver->reltol );
    MatCholeskyFactor( DD1t, NULL, NULL );
    MatMatSolve( DD1t, Imat, Imat );
    MatMatMult( Imat, D1, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &P );


    /* Reconstruct Polynomial */
    Poly6_WLS( stencil, mesh, solver->gamma_diff, aux_conv, poly_cells, poly_faces, P );
                        

    /* Clean up */
    MatDestroy( &Imat );
    MatDestroy( &DD1t );
    MatDestroy( &D1 );
    MatDestroy( &D );
    MatDestroy( &P );

    return 0;
}

PetscErrorCode Reconstruction8_WLS( SolverSettings* solver, MeshGrid* mesh, Stencil* stencil, 
                                    PetscInt* f, PetscReal* poly_cells, PetscReal* poly_faces )
{

    PetscInt        i, j, k, s[4], idx[DIM_8_WLS];
    PetscReal       aux_conv, Pe, w, _w,
                    dn[8], dta[8], dtb[8],
                    s_xyz[3], f_xyz[3],
                    aux[DIM_8_WLS], aux_w[DIM_8_WLS];
    BNDCOND_TYPE    bndcond_type;
    Mat             P, D, D1, DD1t, Imat;

    getFaceCoords( mesh, f, f_xyz );

    dn[0]  = 1;     // dn[i]  = d_normal^i
    dta[0] = 1;     // dta[i] = d_tangent_a^i
    dtb[0] = 1;     // dtb[i] = d_tangent_b^i

    if (f[3] < mesh->fn_3)
        aux_conv = solver->u_conv->x;
    else if (f[3] < 2*mesh->fn_3)
        aux_conv = solver->u_conv->y;
    else
        aux_conv = solver->u_conv->z;

    /* NOTE: 'Pe' isn't really Peclet, but actually L/Pe */
    Pe = solver->gamma_diff / aux_conv;

    for (i = 0; i < DIM_8_WLS; i++)
        idx[i] = i;
    
    
    /* Create auxiliary matrices 'D', 'D1', 'Imat' */
    MatCreateSeqDense( PETSC_COMM_SELF, DIM_8_WLS, stencil->size->total, NULL, &D );
    MatCreateSeqDense( PETSC_COMM_SELF, DIM_8_WLS, stencil->size->total, NULL, &D1);
    MatCreateSeqDense( PETSC_COMM_SELF, DIM_8_WLS, DIM_8_WLS, NULL, &Imat );
    MatSetUp( D );
    MatSetUp( D1);
    MatSetUp( Imat );

    /* Populate dense identity matrix 'Imat' */
    MatZeroEntries( Imat );
    MatShift( Imat, 1.0 );
    MatAssemblyBegin( Imat, MAT_FINAL_ASSEMBLY );

    /* Build Matrices from Stencil Cells */
    for (i = 0; i < stencil->size->cells; i++)
    {
        /* Define 's' as current stencil cell */
        s[3] = stencil->cells[i];
        getCellIJK( mesh, s );
        getCellCoords( mesh, s, s_xyz );

        /* Calculate auxiliary values */
        if (f[3] < mesh->fn_3)
        {
            dn[1]  = s_xyz[0] - f_xyz[0];
            dta[1] = s_xyz[1] - f_xyz[1];
            dtb[1] = s_xyz[2] - f_xyz[2];
        }
        else if (f[3] < 2*mesh->fn_3)
        {
            dtb[1] = s_xyz[0] - f_xyz[0];
            dn[1]  = s_xyz[1] - f_xyz[1];
            dta[1] = s_xyz[2] - f_xyz[2];
        }
        else
        {
            dta[1] = s_xyz[0] - f_xyz[0];
            dtb[1] = s_xyz[1] - f_xyz[1];
            dn[1]  = s_xyz[2] - f_xyz[2];
        }
        /* --- */
        dn[2] = dn[1]*dn[1];        // dn^2
        dn[3] = dn[1]*dn[2];        // dn^3
        dn[4] = dn[2]*dn[2];        // dn^4
        dn[5] = dn[2]*dn[3];        // dn^5
        dn[6] = dn[3]*dn[3];        // dn^6
        dn[7] = dn[3]*dn[4];        // dn^7
        /* --- */
        dta[2] = dta[1]*dta[1];     // dta^2
        dta[3] = dta[1]*dta[2];     // dta^3
        dta[4] = dta[2]*dta[2];     // dta^4
        dta[5] = dta[2]*dta[3];     // dta^5
        dta[6] = dta[3]*dta[3];     // dta^6
        dta[7] = dta[3]*dta[4];     // dta^7
        /* --- */
        dtb[2] = dtb[1]*dtb[1];     // dtb^2
        dtb[3] = dtb[1]*dtb[2];     // dtb^3
        dtb[4] = dtb[2]*dtb[2];     // dtb^4
        dtb[5] = dtb[2]*dtb[3];     // dtb^5
        dtb[6] = dtb[3]*dtb[3];     // dtb^6
        dtb[7] = dtb[3]*dtb[4];     // dtb^7

        /* Calculate weight function */
        _w = 1.0 / (dn[2] + dta[2] + dtb[2]);
        if (solver->weight % 2 == 0)
            w = 1.0;
        else
            w = SQRT(_w);
        for (k = 2 + (solver->weight%2); k <= solver->weight; k+=2)
            w *= _w;

        /* Calculate row 'i' of Matrix 'D' */
        /* --- 0 order --- */
        aux[  0] = 1.;
        /* --- 1 order --- */
        aux[  1] = dn[1];
        aux[  2] =       dta[1];
        aux[  3] =              dtb[1];
        /* --- 2 order --- */
        aux[  4] = dn[2];
        aux[  5] = dn[1]*dta[1];
        aux[  6] = dn[1]*       dtb[1];
        aux[  7] =       dta[2];
        aux[  8] =       dta[1]*dtb[1];
        aux[  9] =              dtb[2];
        /* --- 3 order --- */
        aux[ 10] = dn[3];
        aux[ 11] = dn[2]*dta[1];
        aux[ 12] = dn[2]*       dtb[1];
        aux[ 13] = dn[1]*dta[2];
        aux[ 14] = dn[1]*dta[1]*dtb[1];
        aux[ 15] = dn[1]*       dtb[2];
        aux[ 16] =       dta[3];
        aux[ 17] =       dta[2]*dtb[1];
        aux[ 18] =       dta[1]*dtb[2];
        aux[ 19] =              dtb[3];
        /* --- 4 order --- */
        aux[ 20] = dn[4];
        aux[ 21] = dn[3]*dta[1];
        aux[ 22] = dn[3]*       dtb[1];
        aux[ 23] = dn[2]*dta[2];
        aux[ 24] = dn[2]*dta[1]*dtb[1];
        aux[ 25] = dn[2]*       dtb[2];
        aux[ 26] = dn[1]*dta[3];
        aux[ 27] = dn[1]*dta[2]*dtb[1];
        aux[ 28] = dn[1]*dta[1]*dtb[2];
        aux[ 29] = dn[1]*       dtb[3];
        aux[ 30] =       dta[4];
        aux[ 31] =       dta[3]*dtb[1];
        aux[ 32] =       dta[2]*dtb[2];
        aux[ 33] =       dta[1]*dtb[3];
        aux[ 34] =              dtb[4];
        /* --- 5 order --- */
        aux[ 35] = dn[5];
        aux[ 36] = dn[4]*dta[1];
        aux[ 37] = dn[4]*       dtb[1];
        aux[ 38] = dn[3]*dta[2];
        aux[ 39] = dn[3]*dta[1]*dtb[1];
        aux[ 40] = dn[3]*       dtb[2];
        aux[ 41] = dn[2]*dta[3];
        aux[ 42] = dn[2]*dta[2]*dtb[1];
        aux[ 43] = dn[2]*dta[1]*dtb[2];
        aux[ 44] = dn[2]*       dtb[3];
        aux[ 45] = dn[1]*dta[4];
        aux[ 46] = dn[1]*dta[3]*dtb[1];
        aux[ 47] = dn[1]*dta[2]*dtb[2];
        aux[ 48] = dn[1]*dta[1]*dtb[3];
        aux[ 49] = dn[1]*       dtb[4];
        aux[ 50] =       dta[5];
        aux[ 51] =       dta[4]*dtb[1];
        aux[ 52] =       dta[3]*dtb[2];
        aux[ 53] =       dta[2]*dtb[3];
        aux[ 54] =       dta[1]*dtb[4];
        aux[ 55] =              dtb[5];
        /* --- 6 order --- */
        aux[ 56] = dn[6];
        aux[ 57] = dn[5]*dta[1];
        aux[ 58] = dn[5]*       dtb[1];
        aux[ 59] = dn[4]*dta[2];
        aux[ 60] = dn[4]*dta[1]*dtb[1];
        aux[ 61] = dn[4]*       dtb[2];
        aux[ 62] = dn[3]*dta[3];
        aux[ 63] = dn[3]*dta[2]*dtb[1];
        aux[ 64] = dn[3]*dta[1]*dtb[2];
        aux[ 65] = dn[3]*       dtb[3];
        aux[ 66] = dn[2]*dta[4];
        aux[ 67] = dn[2]*dta[3]*dtb[1];
        aux[ 68] = dn[2]*dta[2]*dtb[2];
        aux[ 69] = dn[2]*dta[1]*dtb[3];
        aux[ 70] = dn[2]*       dtb[4];
        aux[ 71] = dn[1]*dta[5];
        aux[ 72] = dn[1]*dta[4]*dtb[1];
        aux[ 73] = dn[1]*dta[3]*dtb[2];
        aux[ 74] = dn[1]*dta[2]*dtb[3];
        aux[ 75] = dn[1]*dta[1]*dtb[4];
        aux[ 76] = dn[1]*       dtb[5];
        aux[ 77] =       dta[6];
        aux[ 78] =       dta[5]*dtb[1];
        aux[ 79] =       dta[4]*dtb[2];
        aux[ 80] =       dta[3]*dtb[3];
        aux[ 81] =       dta[2]*dtb[4];
        aux[ 82] =       dta[1]*dtb[5];
        aux[ 83] =              dtb[6];
        /* --- 7 order --- */
        aux[ 84] = dn[7];
        aux[ 85] = dn[6]*dta[1];
        aux[ 86] = dn[6]*       dtb[1];
        aux[ 87] = dn[5]*dta[2];
        aux[ 88] = dn[5]*dta[1]*dtb[1];
        aux[ 89] = dn[5]*       dtb[2];
        aux[ 90] = dn[4]*dta[3];
        aux[ 91] = dn[4]*dta[2]*dtb[1];
        aux[ 92] = dn[4]*dta[1]*dtb[2];
        aux[ 93] = dn[4]*       dtb[3];
        aux[ 94] = dn[3]*dta[4];
        aux[ 95] = dn[3]*dta[3]*dtb[1];
        aux[ 96] = dn[3]*dta[2]*dtb[2];
        aux[ 97] = dn[3]*dta[1]*dtb[3];
        aux[ 98] = dn[3]*       dtb[4];
        aux[ 99] = dn[2]*dta[5];
        aux[100] = dn[2]*dta[4]*dtb[1];
        aux[101] = dn[2]*dta[3]*dtb[2];
        aux[102] = dn[2]*dta[2]*dtb[3];
        aux[103] = dn[2]*dta[1]*dtb[4];
        aux[104] = dn[2]*       dtb[5];
        aux[105] = dn[1]*dta[6];
        aux[106] = dn[1]*dta[5]*dtb[1];
        aux[107] = dn[1]*dta[4]*dtb[2];
        aux[108] = dn[1]*dta[3]*dtb[3];
        aux[109] = dn[1]*dta[2]*dtb[4];
        aux[110] = dn[1]*dta[1]*dtb[5];
        aux[111] = dn[1]*       dtb[6];
        aux[112] =       dta[7];
        aux[113] =       dta[6]*dtb[1];
        aux[114] =       dta[5]*dtb[2];
        aux[115] =       dta[4]*dtb[3];
        aux[116] =       dta[3]*dtb[4];
        aux[117] =       dta[2]*dtb[5];
        aux[118] =       dta[1]*dtb[6];
        aux[119] =              dtb[7];

        /* Calculate row 'i' of Matrix 'D1' */
        aux_w[  0] = w;
        aux_w[  1] = w*aux[  1];
        aux_w[  2] = w*aux[  2];
        aux_w[  3] = w*aux[  3];
        aux_w[  4] = w*aux[  4];
        aux_w[  5] = w*aux[  5];
        aux_w[  6] = w*aux[  6];
        aux_w[  7] = w*aux[  7];
        aux_w[  8] = w*aux[  8];
        aux_w[  9] = w*aux[  9];
        aux_w[ 10] = w*aux[ 10];
        aux_w[ 11] = w*aux[ 11];
        aux_w[ 12] = w*aux[ 12];
        aux_w[ 13] = w*aux[ 13];
        aux_w[ 14] = w*aux[ 14];
        aux_w[ 15] = w*aux[ 15];
        aux_w[ 16] = w*aux[ 16];
        aux_w[ 17] = w*aux[ 17];
        aux_w[ 18] = w*aux[ 18];
        aux_w[ 19] = w*aux[ 19];
        aux_w[ 20] = w*aux[ 20];
        aux_w[ 21] = w*aux[ 21];
        aux_w[ 22] = w*aux[ 22];
        aux_w[ 23] = w*aux[ 23];
        aux_w[ 24] = w*aux[ 24];
        aux_w[ 25] = w*aux[ 25];
        aux_w[ 26] = w*aux[ 26];
        aux_w[ 27] = w*aux[ 27];
        aux_w[ 28] = w*aux[ 28];
        aux_w[ 29] = w*aux[ 29];
        aux_w[ 30] = w*aux[ 30];
        aux_w[ 31] = w*aux[ 31];
        aux_w[ 32] = w*aux[ 32];
        aux_w[ 33] = w*aux[ 33];
        aux_w[ 34] = w*aux[ 34];
        aux_w[ 35] = w*aux[ 35];
        aux_w[ 36] = w*aux[ 36];
        aux_w[ 37] = w*aux[ 37];
        aux_w[ 38] = w*aux[ 38];
        aux_w[ 39] = w*aux[ 39];
        aux_w[ 40] = w*aux[ 40];
        aux_w[ 41] = w*aux[ 41];
        aux_w[ 42] = w*aux[ 42];
        aux_w[ 43] = w*aux[ 43];
        aux_w[ 44] = w*aux[ 44];
        aux_w[ 45] = w*aux[ 45];
        aux_w[ 46] = w*aux[ 46];
        aux_w[ 47] = w*aux[ 47];
        aux_w[ 48] = w*aux[ 48];
        aux_w[ 49] = w*aux[ 49];
        aux_w[ 50] = w*aux[ 50];
        aux_w[ 51] = w*aux[ 51];
        aux_w[ 52] = w*aux[ 52];
        aux_w[ 53] = w*aux[ 53];
        aux_w[ 54] = w*aux[ 54];
        aux_w[ 55] = w*aux[ 55];
        aux_w[ 55] = w*aux[ 55];
        aux_w[ 56] = w*aux[ 56];
        aux_w[ 57] = w*aux[ 57];
        aux_w[ 58] = w*aux[ 58];
        aux_w[ 59] = w*aux[ 59];
        aux_w[ 60] = w*aux[ 60];
        aux_w[ 61] = w*aux[ 61];
        aux_w[ 62] = w*aux[ 62];
        aux_w[ 63] = w*aux[ 63];
        aux_w[ 64] = w*aux[ 64];
        aux_w[ 65] = w*aux[ 65];
        aux_w[ 66] = w*aux[ 66];
        aux_w[ 67] = w*aux[ 67];
        aux_w[ 68] = w*aux[ 68];
        aux_w[ 69] = w*aux[ 69];
        aux_w[ 70] = w*aux[ 70];
        aux_w[ 71] = w*aux[ 71];
        aux_w[ 72] = w*aux[ 72];
        aux_w[ 73] = w*aux[ 73];
        aux_w[ 74] = w*aux[ 74];
        aux_w[ 75] = w*aux[ 75];
        aux_w[ 76] = w*aux[ 76];
        aux_w[ 77] = w*aux[ 77];
        aux_w[ 78] = w*aux[ 78];
        aux_w[ 79] = w*aux[ 79];
        aux_w[ 80] = w*aux[ 80];
        aux_w[ 81] = w*aux[ 81];
        aux_w[ 82] = w*aux[ 82];
        aux_w[ 83] = w*aux[ 83];
        aux_w[ 84] = w*aux[ 84];
        aux_w[ 85] = w*aux[ 85];
        aux_w[ 86] = w*aux[ 86];
        aux_w[ 87] = w*aux[ 87];
        aux_w[ 88] = w*aux[ 88];
        aux_w[ 89] = w*aux[ 89];
        aux_w[ 90] = w*aux[ 90];
        aux_w[ 91] = w*aux[ 91];
        aux_w[ 92] = w*aux[ 92];
        aux_w[ 93] = w*aux[ 93];
        aux_w[ 94] = w*aux[ 94];
        aux_w[ 95] = w*aux[ 95];
        aux_w[ 96] = w*aux[ 96];
        aux_w[ 97] = w*aux[ 97];
        aux_w[ 98] = w*aux[ 98];
        aux_w[ 99] = w*aux[ 99];
        aux_w[100] = w*aux[100];
        aux_w[101] = w*aux[101];
        aux_w[102] = w*aux[102];
        aux_w[103] = w*aux[103];
        aux_w[104] = w*aux[104];
        aux_w[105] = w*aux[105];
        aux_w[106] = w*aux[106];
        aux_w[107] = w*aux[107];
        aux_w[108] = w*aux[108];
        aux_w[109] = w*aux[109];
        aux_w[110] = w*aux[110];
        aux_w[111] = w*aux[111];
        aux_w[112] = w*aux[112];
        aux_w[113] = w*aux[113];
        aux_w[114] = w*aux[114];
        aux_w[115] = w*aux[115];
        aux_w[116] = w*aux[116];
        aux_w[117] = w*aux[117];
        aux_w[118] = w*aux[118];
        aux_w[119] = w*aux[119];

        /* Insert values into matrices */
        MatSetValues(  D, DIM_8_WLS, idx, 1, &i,  aux,  INSERT_VALUES );
        MatSetValues( D1, DIM_8_WLS, idx, 1, &i, aux_w, INSERT_VALUES );
    }
    
    /* Build Matrices from Stencil Faces */
    for (j = 0; j < stencil->size->faces; i++, j++)
    {
        /* Define 's' as current stencil face */
        s[3] = stencil->faces[j];
        getFaceIJK( mesh, s );
        getFaceCoords( mesh, s, s_xyz );

        /* Determine parent boundary of 's' */
        if (s[3] < mesh->cell_side)
            bndcond_type = solver->bndcond->e;      // east
        else if (s[3] < mesh->fn_3)
            bndcond_type = solver->bndcond->w;      // west
        else if (s[3] < 2*mesh->fn_3)
        {
            if ((s[3]-mesh->fn_3)%mesh->csXvs < mesh->cell_side)
                bndcond_type = solver->bndcond->s;  // south
            else
                bndcond_type = solver->bndcond->n;  // north;
        }
        else if ((s[3]-2*mesh->fn_3)%mesh->vert_side == 0)
            bndcond_type = solver->bndcond->b;      // bottom
        else
            bndcond_type = solver->bndcond->t;      // top

        /* Calculate auxiliary values */
        if (f[3] < mesh->fn_3)
        {
            dn[1]  = s_xyz[0] - f_xyz[0];
            dta[1] = s_xyz[1] - f_xyz[1];
            dtb[1] = s_xyz[2] - f_xyz[2];
        }
        else if (f[3] < 2*mesh->fn_3)
        {
            dtb[1] = s_xyz[0] - f_xyz[0];
            dn[1]  = s_xyz[1] - f_xyz[1];
            dta[1] = s_xyz[2] - f_xyz[2];
        }
        else
        {
            dta[1] = s_xyz[0] - f_xyz[0];
            dtb[1] = s_xyz[1] - f_xyz[1];
            dn[1]  = s_xyz[2] - f_xyz[2];
        }
        /* --- */
        dn[2] = dn[1]*dn[1];        // dn^2
        dn[3] = dn[1]*dn[2];        // dn^3
        dn[4] = dn[2]*dn[2];        // dn^4
        dn[5] = dn[2]*dn[3];        // dn^5
        dn[6] = dn[3]*dn[3];        // dn^6
        dn[7] = dn[3]*dn[4];        // dn^7
        /* --- */
        dta[2] = dta[1]*dta[1];     // dta^2
        dta[3] = dta[1]*dta[2];     // dta^3
        dta[4] = dta[2]*dta[2];     // dta^4
        dta[5] = dta[2]*dta[3];     // dta^5
        dta[6] = dta[3]*dta[3];     // dta^6
        dta[7] = dta[3]*dta[4];     // dta^7
        /* --- */
        dtb[2] = dtb[1]*dtb[1];     // dtb^2
        dtb[3] = dtb[1]*dtb[2];     // dtb^3
        dtb[4] = dtb[2]*dtb[2];     // dtb^4
        dtb[5] = dtb[2]*dtb[3];     // dtb^5
        dtb[6] = dtb[3]*dtb[3];     // dtb^6
        dtb[7] = dtb[3]*dtb[4];     // dtb^7

        /* Calculate weight function */
        if (s[3] != f[3])
            _w = 1.0 / (dn[2] + dta[2] + dtb[2]);
        else
            _w = mesh->_2_Lref2;
        if (solver->weight % 2 == 0)
            w = 1.0;
        else
            w = SQRT(_w);
        for (k = 2 + (solver->weight%2); k <= solver->weight; k+=2)
            w *= _w;


        /* Calculate row 'i' of Matrix 'D' and of Matrix 'D1' */
        if (bndcond_type == DIRICHLET)
        {
            /* --- 0 order --- */
            aux[  0] = 1.;
            /* --- 1 order --- */
            aux[  1] = dn[1];
            aux[  2] =       dta[1];
            aux[  3] =              dtb[1];
            /* --- 2 order --- */
            aux[  4] = dn[2];
            aux[  5] = dn[1]*dta[1];
            aux[  6] = dn[1]*       dtb[1];
            aux[  7] =       dta[2];
            aux[  8] =       dta[1]*dtb[1];
            aux[  9] =              dtb[2];
            /* --- 3 order --- */
            aux[ 10] = dn[3];
            aux[ 11] = dn[2]*dta[1];
            aux[ 12] = dn[2]*       dtb[1];
            aux[ 13] = dn[1]*dta[2];
            aux[ 14] = dn[1]*dta[1]*dtb[1];
            aux[ 15] = dn[1]*       dtb[2];
            aux[ 16] =       dta[3];
            aux[ 17] =       dta[2]*dtb[1];
            aux[ 18] =       dta[1]*dtb[2];
            aux[ 19] =              dtb[3];
            /* --- 4 order --- */
            aux[ 20] = dn[4];
            aux[ 21] = dn[3]*dta[1];
            aux[ 22] = dn[3]*       dtb[1];
            aux[ 23] = dn[2]*dta[2];
            aux[ 24] = dn[2]*dta[1]*dtb[1];
            aux[ 25] = dn[2]*       dtb[2];
            aux[ 26] = dn[1]*dta[3];
            aux[ 27] = dn[1]*dta[2]*dtb[1];
            aux[ 28] = dn[1]*dta[1]*dtb[2];
            aux[ 29] = dn[1]*       dtb[3];
            aux[ 30] =       dta[4];
            aux[ 31] =       dta[3]*dtb[1];
            aux[ 32] =       dta[2]*dtb[2];
            aux[ 33] =       dta[1]*dtb[3];
            aux[ 34] =              dtb[4];
            /* --- 5 order --- */
            aux[ 35] = dn[5];
            aux[ 36] = dn[4]*dta[1];
            aux[ 37] = dn[4]*       dtb[1];
            aux[ 38] = dn[3]*dta[2];
            aux[ 39] = dn[3]*dta[1]*dtb[1];
            aux[ 40] = dn[3]*       dtb[2];
            aux[ 41] = dn[2]*dta[3];
            aux[ 42] = dn[2]*dta[2]*dtb[1];
            aux[ 43] = dn[2]*dta[1]*dtb[2];
            aux[ 44] = dn[2]*       dtb[3];
            aux[ 45] = dn[1]*dta[4];
            aux[ 46] = dn[1]*dta[3]*dtb[1];
            aux[ 47] = dn[1]*dta[2]*dtb[2];
            aux[ 48] = dn[1]*dta[1]*dtb[3];
            aux[ 49] = dn[1]*       dtb[4];
            aux[ 50] =       dta[5];
            aux[ 51] =       dta[4]*dtb[1];
            aux[ 52] =       dta[3]*dtb[2];
            aux[ 53] =       dta[2]*dtb[3];
            aux[ 54] =       dta[1]*dtb[4];
            aux[ 55] =              dtb[5];
            /* --- 6 order --- */
            aux[ 56] = dn[6];
            aux[ 57] = dn[5]*dta[1];
            aux[ 58] = dn[5]*       dtb[1];
            aux[ 59] = dn[4]*dta[2];
            aux[ 60] = dn[4]*dta[1]*dtb[1];
            aux[ 61] = dn[4]*       dtb[2];
            aux[ 62] = dn[3]*dta[3];
            aux[ 63] = dn[3]*dta[2]*dtb[1];
            aux[ 64] = dn[3]*dta[1]*dtb[2];
            aux[ 65] = dn[3]*       dtb[3];
            aux[ 66] = dn[2]*dta[4];
            aux[ 67] = dn[2]*dta[3]*dtb[1];
            aux[ 68] = dn[2]*dta[2]*dtb[2];
            aux[ 69] = dn[2]*dta[1]*dtb[3];
            aux[ 70] = dn[2]*       dtb[4];
            aux[ 71] = dn[1]*dta[5];
            aux[ 72] = dn[1]*dta[4]*dtb[1];
            aux[ 73] = dn[1]*dta[3]*dtb[2];
            aux[ 74] = dn[1]*dta[2]*dtb[3];
            aux[ 75] = dn[1]*dta[1]*dtb[4];
            aux[ 76] = dn[1]*       dtb[5];
            aux[ 77] =       dta[6];
            aux[ 78] =       dta[5]*dtb[1];
            aux[ 79] =       dta[4]*dtb[2];
            aux[ 80] =       dta[3]*dtb[3];
            aux[ 81] =       dta[2]*dtb[4];
            aux[ 82] =       dta[1]*dtb[5];
            aux[ 83] =              dtb[6];
            /* --- 7 order --- */
            aux[ 84] = dn[7];
            aux[ 85] = dn[6]*dta[1];
            aux[ 86] = dn[6]*       dtb[1];
            aux[ 87] = dn[5]*dta[2];
            aux[ 88] = dn[5]*dta[1]*dtb[1];
            aux[ 89] = dn[5]*       dtb[2];
            aux[ 90] = dn[4]*dta[3];
            aux[ 91] = dn[4]*dta[2]*dtb[1];
            aux[ 92] = dn[4]*dta[1]*dtb[2];
            aux[ 93] = dn[4]*       dtb[3];
            aux[ 94] = dn[3]*dta[4];
            aux[ 95] = dn[3]*dta[3]*dtb[1];
            aux[ 96] = dn[3]*dta[2]*dtb[2];
            aux[ 97] = dn[3]*dta[1]*dtb[3];
            aux[ 98] = dn[3]*       dtb[4];
            aux[ 99] = dn[2]*dta[5];
            aux[100] = dn[2]*dta[4]*dtb[1];
            aux[101] = dn[2]*dta[3]*dtb[2];
            aux[102] = dn[2]*dta[2]*dtb[3];
            aux[103] = dn[2]*dta[1]*dtb[4];
            aux[104] = dn[2]*       dtb[5];
            aux[105] = dn[1]*dta[6];
            aux[106] = dn[1]*dta[5]*dtb[1];
            aux[107] = dn[1]*dta[4]*dtb[2];
            aux[108] = dn[1]*dta[3]*dtb[3];
            aux[109] = dn[1]*dta[2]*dtb[4];
            aux[110] = dn[1]*dta[1]*dtb[5];
            aux[111] = dn[1]*       dtb[6];
            aux[112] =       dta[7];
            aux[113] =       dta[6]*dtb[1];
            aux[114] =       dta[5]*dtb[2];
            aux[115] =       dta[4]*dtb[3];
            aux[116] =       dta[3]*dtb[4];
            aux[117] =       dta[2]*dtb[5];
            aux[118] =       dta[1]*dtb[6];
            aux[119] =              dtb[7];

            aux_w[  0] = w;
            aux_w[  1] = w*aux[  1];
            aux_w[  2] = w*aux[  2];
            aux_w[  3] = w*aux[  3];
            aux_w[  4] = w*aux[  4];
            aux_w[  5] = w*aux[  5];
            aux_w[  6] = w*aux[  6];
            aux_w[  7] = w*aux[  7];
            aux_w[  8] = w*aux[  8];
            aux_w[  9] = w*aux[  9];
            aux_w[ 10] = w*aux[ 10];
            aux_w[ 11] = w*aux[ 11];
            aux_w[ 12] = w*aux[ 12];
            aux_w[ 13] = w*aux[ 13];
            aux_w[ 14] = w*aux[ 14];
            aux_w[ 15] = w*aux[ 15];
            aux_w[ 16] = w*aux[ 16];
            aux_w[ 17] = w*aux[ 17];
            aux_w[ 18] = w*aux[ 18];
            aux_w[ 19] = w*aux[ 19];
            aux_w[ 20] = w*aux[ 20];
            aux_w[ 21] = w*aux[ 21];
            aux_w[ 22] = w*aux[ 22];
            aux_w[ 23] = w*aux[ 23];
            aux_w[ 24] = w*aux[ 24];
            aux_w[ 25] = w*aux[ 25];
            aux_w[ 26] = w*aux[ 26];
            aux_w[ 27] = w*aux[ 27];
            aux_w[ 28] = w*aux[ 28];
            aux_w[ 29] = w*aux[ 29];
            aux_w[ 30] = w*aux[ 30];
            aux_w[ 31] = w*aux[ 31];
            aux_w[ 32] = w*aux[ 32];
            aux_w[ 33] = w*aux[ 33];
            aux_w[ 34] = w*aux[ 34];
            aux_w[ 35] = w*aux[ 35];
            aux_w[ 36] = w*aux[ 36];
            aux_w[ 37] = w*aux[ 37];
            aux_w[ 38] = w*aux[ 38];
            aux_w[ 39] = w*aux[ 39];
            aux_w[ 40] = w*aux[ 40];
            aux_w[ 41] = w*aux[ 41];
            aux_w[ 42] = w*aux[ 42];
            aux_w[ 43] = w*aux[ 43];
            aux_w[ 44] = w*aux[ 44];
            aux_w[ 45] = w*aux[ 45];
            aux_w[ 46] = w*aux[ 46];
            aux_w[ 47] = w*aux[ 47];
            aux_w[ 48] = w*aux[ 48];
            aux_w[ 49] = w*aux[ 49];
            aux_w[ 50] = w*aux[ 50];
            aux_w[ 51] = w*aux[ 51];
            aux_w[ 52] = w*aux[ 52];
            aux_w[ 53] = w*aux[ 53];
            aux_w[ 54] = w*aux[ 54];
            aux_w[ 55] = w*aux[ 55];
            aux_w[ 55] = w*aux[ 55];
            aux_w[ 56] = w*aux[ 56];
            aux_w[ 57] = w*aux[ 57];
            aux_w[ 58] = w*aux[ 58];
            aux_w[ 59] = w*aux[ 59];
            aux_w[ 60] = w*aux[ 60];
            aux_w[ 61] = w*aux[ 61];
            aux_w[ 62] = w*aux[ 62];
            aux_w[ 63] = w*aux[ 63];
            aux_w[ 64] = w*aux[ 64];
            aux_w[ 65] = w*aux[ 65];
            aux_w[ 66] = w*aux[ 66];
            aux_w[ 67] = w*aux[ 67];
            aux_w[ 68] = w*aux[ 68];
            aux_w[ 69] = w*aux[ 69];
            aux_w[ 70] = w*aux[ 70];
            aux_w[ 71] = w*aux[ 71];
            aux_w[ 72] = w*aux[ 72];
            aux_w[ 73] = w*aux[ 73];
            aux_w[ 74] = w*aux[ 74];
            aux_w[ 75] = w*aux[ 75];
            aux_w[ 76] = w*aux[ 76];
            aux_w[ 77] = w*aux[ 77];
            aux_w[ 78] = w*aux[ 78];
            aux_w[ 79] = w*aux[ 79];
            aux_w[ 80] = w*aux[ 80];
            aux_w[ 81] = w*aux[ 81];
            aux_w[ 82] = w*aux[ 82];
            aux_w[ 83] = w*aux[ 83];
            aux_w[ 84] = w*aux[ 84];
            aux_w[ 85] = w*aux[ 85];
            aux_w[ 86] = w*aux[ 86];
            aux_w[ 87] = w*aux[ 87];
            aux_w[ 88] = w*aux[ 88];
            aux_w[ 89] = w*aux[ 89];
            aux_w[ 90] = w*aux[ 90];
            aux_w[ 91] = w*aux[ 91];
            aux_w[ 92] = w*aux[ 92];
            aux_w[ 93] = w*aux[ 93];
            aux_w[ 94] = w*aux[ 94];
            aux_w[ 95] = w*aux[ 95];
            aux_w[ 96] = w*aux[ 96];
            aux_w[ 97] = w*aux[ 97];
            aux_w[ 98] = w*aux[ 98];
            aux_w[ 99] = w*aux[ 99];
            aux_w[100] = w*aux[100];
            aux_w[101] = w*aux[101];
            aux_w[102] = w*aux[102];
            aux_w[103] = w*aux[103];
            aux_w[104] = w*aux[104];
            aux_w[105] = w*aux[105];
            aux_w[106] = w*aux[106];
            aux_w[107] = w*aux[107];
            aux_w[108] = w*aux[108];
            aux_w[109] = w*aux[109];
            aux_w[110] = w*aux[110];
            aux_w[111] = w*aux[111];
            aux_w[112] = w*aux[112];
            aux_w[113] = w*aux[113];
            aux_w[114] = w*aux[114];
            aux_w[115] = w*aux[115];
            aux_w[116] = w*aux[116];
            aux_w[117] = w*aux[117];
            aux_w[118] = w*aux[118];
            aux_w[119] = w*aux[119];
        }
        else if (bndcond_type == NEUMANN)
        {
            /* --- 0 order --- */
            aux[ 0] = 0;
            /* --- 1 order --- */
            aux[ 1] = mesh->area;
            aux[ 2] = 0;
            aux[ 3] = 0;
            /* --- 2 order --- */
            aux[ 4] = mesh->area * 2*dn[1];
            aux[ 5] = mesh->area *         dta[1];
            aux[ 6] = mesh->area *                dtb[1];
            aux[ 7] = 0;
            aux[ 8] = 0;
            aux[ 9] = 0;
            /* --- 3 order --- */
            aux[10] = mesh->area * 3*dn[2];
            aux[11] = mesh->area * 2*dn[1]*dta[1];
            aux[12] = mesh->area * 2*dn[1]*       dtb[1];
            aux[13] = mesh->area *         dta[2];
            aux[14] = mesh->area *         dta[1]*dtb[1];
            aux[15] = mesh->area *                dtb[2];
            aux[16] = 0;
            aux[17] = 0;
            aux[18] = 0;
            aux[19] = 0;
            /* --- 4 order --- */
            aux[20] = mesh->area * 4*dn[3];
            aux[21] = mesh->area * 3*dn[2]*dta[1];
            aux[22] = mesh->area * 3*dn[2]*       dtb[1];
            aux[23] = mesh->area * 2*dn[1]*dta[2];
            aux[24] = mesh->area * 2*dn[1]*dta[1]*dtb[1];
            aux[25] = mesh->area * 2*dn[1]*       dtb[2];
            aux[26] = mesh->area *         dta[3];
            aux[27] = mesh->area *         dta[2]*dtb[1];
            aux[28] = mesh->area *         dta[1]*dtb[2];
            aux[29] = mesh->area *                dtb[3];
            aux[30] = 0;
            aux[31] = 0;
            aux[32] = 0;
            aux[33] = 0;
            aux[34] = 0;
            /* --- 5 order --- */
            aux[35] = mesh->area * 5*dn[4];
            aux[36] = mesh->area * 4*dn[3]*dta[1];
            aux[37] = mesh->area * 4*dn[3]*       dtb[1];
            aux[38] = mesh->area * 3*dn[2]*dta[2];
            aux[39] = mesh->area * 3*dn[2]*dta[1]*dtb[1];
            aux[40] = mesh->area * 3*dn[2]*       dtb[2];
            aux[41] = mesh->area * 2*dn[1]*dta[3];
            aux[42] = mesh->area * 2*dn[1]*dta[2]*dtb[1];
            aux[43] = mesh->area * 2*dn[1]*dta[1]*dtb[2];
            aux[44] = mesh->area * 2*dn[1]*       dtb[3];
            aux[45] = mesh->area *         dta[4];
            aux[46] = mesh->area *         dta[3]*dtb[1];
            aux[47] = mesh->area *         dta[2]*dtb[2];
            aux[48] = mesh->area *         dta[1]*dtb[3];
            aux[49] = mesh->area *                dtb[4];
            aux[50] = 0;
            aux[51] = 0;
            aux[52] = 0;
            aux[53] = 0;
            aux[54] = 0;
            aux[55] = 0;
            /* --- 6 order --- */
            aux[ 56] = mesh->area * 6*dn[5];
            aux[ 57] = mesh->area * 5*dn[4]*dta[1];
            aux[ 58] = mesh->area * 5*dn[4]*       dtb[1];
            aux[ 59] = mesh->area * 4*dn[3]*dta[2];
            aux[ 60] = mesh->area * 4*dn[3]*dta[1]*dtb[1];
            aux[ 61] = mesh->area * 4*dn[3]*       dtb[2];
            aux[ 62] = mesh->area * 3*dn[2]*dta[3];
            aux[ 63] = mesh->area * 3*dn[2]*dta[2]*dtb[1];
            aux[ 64] = mesh->area * 3*dn[2]*dta[1]*dtb[2];
            aux[ 65] = mesh->area * 3*dn[2]*       dtb[3];
            aux[ 66] = mesh->area * 2*dn[1]*dta[4];
            aux[ 67] = mesh->area * 2*dn[1]*dta[3]*dtb[1];
            aux[ 68] = mesh->area * 2*dn[1]*dta[2]*dtb[2];
            aux[ 69] = mesh->area * 2*dn[1]*dta[1]*dtb[3];
            aux[ 70] = mesh->area * 2*dn[1]*       dtb[4];
            aux[ 71] = mesh->area *         dta[5];
            aux[ 72] = mesh->area *         dta[4]*dtb[1];
            aux[ 73] = mesh->area *         dta[3]*dtb[2];
            aux[ 74] = mesh->area *         dta[2]*dtb[3];
            aux[ 75] = mesh->area *         dta[1]*dtb[4];
            aux[ 76] = mesh->area *                dtb[5];
            aux[ 77] = 0;
            aux[ 78] = 0;
            aux[ 79] = 0;
            aux[ 80] = 0;
            aux[ 81] = 0;
            aux[ 82] = 0;
            aux[ 83] = 0;
            /* --- 7 order --- */
            aux[ 84] = mesh->area * 7*dn[6];
            aux[ 85] = mesh->area * 6*dn[5]*dta[1];
            aux[ 86] = mesh->area * 6*dn[5]*       dtb[1];
            aux[ 87] = mesh->area * 5*dn[4]*dta[2];
            aux[ 88] = mesh->area * 5*dn[4]*dta[1]*dtb[1];
            aux[ 89] = mesh->area * 5*dn[4]*       dtb[2];
            aux[ 90] = mesh->area * 4*dn[3]*dta[3];
            aux[ 91] = mesh->area * 4*dn[3]*dta[2]*dtb[1];
            aux[ 92] = mesh->area * 4*dn[3]*dta[1]*dtb[2];
            aux[ 93] = mesh->area * 4*dn[3]*       dtb[3];
            aux[ 94] = mesh->area * 3*dn[2]*dta[4];
            aux[ 95] = mesh->area * 3*dn[2]*dta[3]*dtb[1];
            aux[ 96] = mesh->area * 3*dn[2]*dta[2]*dtb[2];
            aux[ 97] = mesh->area * 3*dn[2]*dta[1]*dtb[3];
            aux[ 98] = mesh->area * 3*dn[2]*       dtb[4];
            aux[ 99] = mesh->area * 2*dn[1]*dta[5];
            aux[100] = mesh->area * 2*dn[1]*dta[4]*dtb[1];
            aux[101] = mesh->area * 2*dn[1]*dta[3]*dtb[2];
            aux[102] = mesh->area * 2*dn[1]*dta[2]*dtb[3];
            aux[103] = mesh->area * 2*dn[1]*dta[1]*dtb[4];
            aux[104] = mesh->area * 2*dn[1]*       dtb[5];
            aux[105] = mesh->area *         dta[6];
            aux[106] = mesh->area *         dta[5]*dtb[1];
            aux[107] = mesh->area *         dta[4]*dtb[2];
            aux[108] = mesh->area *         dta[3]*dtb[3];
            aux[109] = mesh->area *         dta[2]*dtb[4];
            aux[110] = mesh->area *         dta[1]*dtb[5];
            aux[111] = mesh->area *                dtb[6];
            aux[112] = 0;
            aux[113] = 0;
            aux[114] = 0;
            aux[115] = 0;
            aux[116] = 0;
            aux[117] = 0;
            aux[118] = 0;
            aux[119] = 0;

            aux_w[ 0] = 0;
            aux_w[ 1] = w*aux[ 1];
            aux_w[ 2] = 0;
            aux_w[ 3] = 0;
            aux_w[ 4] = w*aux[ 4];
            aux_w[ 5] = w*aux[ 5];
            aux_w[ 6] = w*aux[ 6];
            aux_w[ 7] = 0;
            aux_w[ 8] = 0;
            aux_w[ 9] = 0;
            aux_w[10] = w*aux[10];
            aux_w[11] = w*aux[11];
            aux_w[12] = w*aux[12];
            aux_w[13] = w*aux[13];
            aux_w[14] = w*aux[14];
            aux_w[15] = w*aux[15];
            aux_w[16] = 0;
            aux_w[17] = 0;
            aux_w[18] = 0;
            aux_w[19] = 0;
            aux_w[20] = w*aux[20];
            aux_w[21] = w*aux[21];
            aux_w[22] = w*aux[22];
            aux_w[23] = w*aux[23];
            aux_w[24] = w*aux[24];
            aux_w[25] = w*aux[25];
            aux_w[26] = w*aux[26];
            aux_w[27] = w*aux[27];
            aux_w[28] = w*aux[28];
            aux_w[29] = w*aux[29];
            aux_w[30] = 0;
            aux_w[31] = 0;
            aux_w[32] = 0;
            aux_w[33] = 0;
            aux_w[34] = 0;
            aux_w[35] = w*aux[35];
            aux_w[36] = w*aux[36];
            aux_w[37] = w*aux[37];
            aux_w[38] = w*aux[38];
            aux_w[39] = w*aux[39];
            aux_w[40] = w*aux[40];
            aux_w[41] = w*aux[41];
            aux_w[42] = w*aux[42];
            aux_w[43] = w*aux[43];
            aux_w[44] = w*aux[44];
            aux_w[45] = w*aux[45];
            aux_w[46] = w*aux[46];
            aux_w[47] = w*aux[47];
            aux_w[48] = w*aux[48];
            aux_w[49] = w*aux[49];
            aux_w[50] = 0;
            aux_w[51] = 0;
            aux_w[52] = 0;
            aux_w[53] = 0;
            aux_w[54] = 0;
            aux_w[55] = 0;
            aux_w[ 55] = w*aux[ 55];
            aux_w[ 56] = w*aux[ 56];
            aux_w[ 57] = w*aux[ 57];
            aux_w[ 58] = w*aux[ 58];
            aux_w[ 59] = w*aux[ 59];
            aux_w[ 60] = w*aux[ 60];
            aux_w[ 61] = w*aux[ 61];
            aux_w[ 62] = w*aux[ 62];
            aux_w[ 63] = w*aux[ 63];
            aux_w[ 64] = w*aux[ 64];
            aux_w[ 65] = w*aux[ 65];
            aux_w[ 66] = w*aux[ 66];
            aux_w[ 67] = w*aux[ 67];
            aux_w[ 68] = w*aux[ 68];
            aux_w[ 69] = w*aux[ 69];
            aux_w[ 70] = w*aux[ 70];
            aux_w[ 71] = w*aux[ 71];
            aux_w[ 72] = w*aux[ 72];
            aux_w[ 73] = w*aux[ 73];
            aux_w[ 74] = w*aux[ 74];
            aux_w[ 75] = w*aux[ 75];
            aux_w[ 76] = w*aux[ 76];
            aux_w[ 77] = 0;
            aux_w[ 78] = 0;
            aux_w[ 79] = 0;
            aux_w[ 80] = 0;
            aux_w[ 81] = 0;
            aux_w[ 82] = 0;
            aux_w[ 83] = 0;
            aux_w[ 84] = w*aux[ 84];
            aux_w[ 85] = w*aux[ 85];
            aux_w[ 86] = w*aux[ 86];
            aux_w[ 87] = w*aux[ 87];
            aux_w[ 88] = w*aux[ 88];
            aux_w[ 89] = w*aux[ 89];
            aux_w[ 90] = w*aux[ 90];
            aux_w[ 91] = w*aux[ 91];
            aux_w[ 92] = w*aux[ 92];
            aux_w[ 93] = w*aux[ 93];
            aux_w[ 94] = w*aux[ 94];
            aux_w[ 95] = w*aux[ 95];
            aux_w[ 96] = w*aux[ 96];
            aux_w[ 97] = w*aux[ 97];
            aux_w[ 98] = w*aux[ 98];
            aux_w[ 99] = w*aux[ 99];
            aux_w[100] = w*aux[100];
            aux_w[101] = w*aux[101];
            aux_w[102] = w*aux[102];
            aux_w[103] = w*aux[103];
            aux_w[104] = w*aux[104];
            aux_w[105] = w*aux[105];
            aux_w[106] = w*aux[106];
            aux_w[107] = w*aux[107];
            aux_w[108] = w*aux[108];
            aux_w[109] = w*aux[109];
            aux_w[110] = w*aux[110];
            aux_w[111] = w*aux[111];
            aux_w[112] = 0;
            aux_w[113] = 0;
            aux_w[114] = 0;
            aux_w[115] = 0;
            aux_w[116] = 0;
            aux_w[117] = 0;
            aux_w[118] = 0;
            aux_w[119] = 0;
        }
        else    // bndcond_type == ROBIN
        {
            /* TODO */
        }

        /* Insert values into matrices */
        MatSetValues(  D, DIM_8_WLS, idx, 1, &i,  aux,  INSERT_VALUES );
        MatSetValues( D1, DIM_8_WLS, idx, 1, &i, aux_w, INSERT_VALUES );
    }

    /* Assemble Matrices */
    MatAssemblyBegin(  D, MAT_FINAL_ASSEMBLY );
    MatAssemblyBegin( D1, MAT_FINAL_ASSEMBLY );
    MatAssemblyEnd( Imat, MAT_FINAL_ASSEMBLY );
    MatAssemblyEnd(    D, MAT_FINAL_ASSEMBLY );
    MatAssemblyEnd(   D1, MAT_FINAL_ASSEMBLY );


    /* Calculate Polynomial Coeffs 'P' */
    MatMatTransposeMult( D, D1, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &DD1t );
    // MatChop( DD1t, solver->reltol );
    MatCholeskyFactor( DD1t, NULL, NULL );
    MatMatSolve( DD1t, Imat, Imat );
    MatMatMult( Imat, D1, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &P );


    /* Reconstruct Polynomial */
    Poly8_WLS( stencil, mesh, solver->gamma_diff, aux_conv, poly_cells, poly_faces, P );

    /* Clean up */
    MatDestroy( &Imat );
    MatDestroy( &DD1t);
    MatDestroy( &D1);
    MatDestroy( &D );
    MatDestroy( &P );

    return 0;
}
