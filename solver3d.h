#ifndef SOLVER2D_H
#define SOLVER2D_H


#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <sys/resource.h>

#include <omp.h>
#include <petscvec.h>

#ifdef PETSC_USE_REAL___FLOAT128
#include <quadmath.h>
#endif

#include "StructTypes.h"


/* ----------------------------------------------------------------------------- */


static PetscErrorCode SolverInit( SolverSettings** solver, MeshGrid** mesh, 
                                    Solution** sol, Error** error, FILE** fout );

static PetscErrorCode SolverEnd( SolverSettings* solver, MeshGrid* mesh, 
                                    Solution* sol, Error* error, FILE* fout );


#endif // !SOLVER2D_H