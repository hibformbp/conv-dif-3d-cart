
#include "Poly.h"
#include "Reconstruction.h"


/* ------------- Weighted Least-Squares -------------- */

PetscErrorCode Poly2_WLS( Stencil* stencil, MeshGrid* mesh, PetscReal gamma_diff, PetscReal u_conv,
                            PetscReal* poly_cells, PetscReal* poly_faces, Mat P )
{

    const PetscReal *P_arr;
    MatDenseGetArrayRead( P, &P_arr );
    
    PetscReal *_C, **C;
    PetscMalloc2( 2*stencil->size->total, &_C,
                    stencil->size->total, &C );
    

    /* Collect necessary coefficients */
    for (PetscInt i = 0; i < stencil->size->total; i++)
    {
        C[i] = &(_C[i * 2]);

        /* Geometric considerations:
            * using centroid
            * dn=0
            * dta=0
            * dtb=0
        */
        
        /* Poly: contribution = term */
        C[i][0] = P_arr[i*DIM_2_WLS];           // 1
        
        /* Flux: contribution = d(term)/dn */
        C[i][1] = P_arr[i*DIM_2_WLS + 1];       // dn
    }

    MatDenseRestoreArrayRead( P, &P_arr );
    

    /* Calculate polynomial (cells)  */
    for (PetscInt sc = 0; sc < stencil->size->cells; sc++)
        poly_cells[sc] = gamma_diff * C[sc][1] 
                       +     u_conv * C[sc][0] ;


    /* Calculate polynomial (faces)  */
    for (PetscInt sf = 0, si = stencil->size->cells; sf < stencil->size->faces; sf++, si++)
        poly_faces[sf] = gamma_diff * C[si][1]
                       +     u_conv * C[si][0] ;


    /* Clean up */
    PetscFree2( _C, C );

    return 0;
}

PetscErrorCode Poly4_WLS( Stencil* stencil, MeshGrid* mesh, PetscReal gamma_diff, PetscReal u_conv,
                            PetscReal* poly_cells, PetscReal* poly_faces, Mat P )
{

    const PetscReal *P_arr;
    MatDenseGetArrayRead( P, &P_arr );
    
    PetscReal *_C, **C;
    PetscMalloc2( 4*stencil->size->total, &_C,
                    stencil->size->total, &C );

    /* Collect necessary coefficients */
    for (PetscInt i = 0; i < stencil->size->total; i++)
    {
        C[i] = &(_C[i * 4]);
        
        /* Geometric considerations:
            * dn=0
            * sum (dta^odd) = 0
            * sum (dtb^odd) = 0
            * sum (dta^odd*dtb^odd) = 0
        */
        
        /* Poly: contribution = term */
        C[i][0] = P_arr[i*DIM_4_WLS];           // 1
        C[i][1] = P_arr[i*DIM_4_WLS +  7]       // dta2
                + P_arr[i*DIM_4_WLS +  9];      // dtb2
        
        /* Flux: contribution = d(term)/dn */
        C[i][2] = P_arr[i*DIM_4_WLS +  1];      // dn
        C[i][3] = P_arr[i*DIM_4_WLS + 13]       // dn.dta2
                + P_arr[i*DIM_4_WLS + 15];      // dn.dtb2
    }

    MatDenseRestoreArrayRead( P, &P_arr );
    

    /* Gauss points */
    /* d := .5 * Lref * sqrt(1/3) */
    #ifdef PETSC_USE_REAL___FLOAT128
    PetscReal d2 = 0.3333333333333333333333333333333333333333333333333333333333333333Q / mesh->_2_Lref2;
    #else
    PetscReal d2 = 3.33333333333333333333333333E-1 / mesh->_2_Lref2;
    #endif


    /* Calculate polynomial (cells)  */
    for (PetscInt sc = 0; sc < stencil->size->cells; sc++)
        poly_cells[sc] = gamma_diff * ( C[sc][2] + d2*C[sc][3] )
                       +     u_conv * ( C[sc][0] + d2*C[sc][1] ) ;


    /* Calculate polynomial (faces)  */
    for (PetscInt sf = 0, si = stencil->size->cells; sf < stencil->size->faces; sf++, si++)
        poly_faces[sf] = gamma_diff * ( C[si][2] + d2*C[si][3] )
                       +     u_conv * ( C[si][0] + d2*C[si][1] ) ;


    /* Clean up */
    PetscFree2( _C, C );

    return 0;
}

PetscErrorCode Poly6_WLS( Stencil* stencil, MeshGrid* mesh, PetscReal gamma_diff, PetscReal u_conv,
                            PetscReal* poly_cells, PetscReal* poly_faces, Mat P )
{

    const PetscReal* P_arr;
    MatDenseGetArrayRead( P, &P_arr );
    
    PetscReal *_C, **C;
    PetscMalloc2(12*stencil->size->total, &_C,
                    stencil->size->total, &C );

    /* Collect necessary coefficients */   
    for (PetscInt i = 0; i < stencil->size->total; i++)
    {
        C[i] = &(_C[i * 12]);

        /* Geometric considerations:
            * dn=0
            * dta=dtb=0 for type A (pt 0)
            * dta=0 for type B (pts 1,2)
            * sum (dta^odd) = 0
            * sum (dtb^odd) = 0
            * sum (dta^odd*dtb^odd) = 0
        */

        /* Poly: contribution = term */
        C[i][ 0] = P_arr[i*DIM_6_WLS];           // 1
        C[i][ 1] = P_arr[i*DIM_6_WLS +  7];      // dta2
        C[i][ 2] = P_arr[i*DIM_6_WLS +  9];      // dtb2
        C[i][ 3] = P_arr[i*DIM_6_WLS + 30];      // dta4
        C[i][ 4] = P_arr[i*DIM_6_WLS + 32];      // dta2.dtb2
        C[i][ 5] = P_arr[i*DIM_6_WLS + 34];      // dtb4

        /* Flux: contribution = d(term)/dn */
        C[i][ 6] = P_arr[i*DIM_6_WLS +  1];      // dn
        C[i][ 7] = P_arr[i*DIM_6_WLS + 13];      // dn.dta2
        C[i][ 8] = P_arr[i*DIM_6_WLS + 15];      // dn.dtb2
        C[i][ 9] = P_arr[i*DIM_6_WLS + 45];      // dn.dta4
        C[i][10] = P_arr[i*DIM_6_WLS + 47];      // dn.dta2.dtb2
        C[i][11] = P_arr[i*DIM_6_WLS + 49];      // dn.dtb4
    }

    MatDenseRestoreArrayRead( P, &P_arr );


    /* Gauss weights */
    PetscReal gw_b, gw_c;
    #ifdef PETSC_USE_REAL___FLOAT128
    gw_b = 0.1587301587301587301587301587301587301587301587301587301587301587Q;
    gw_c = 0.5555555555555555555555555555555555555555555555555555555555555556Q;
    #else
    gw_b = 1.58730158730158730158730159E-1;
    gw_c = 5.55555555555555555555555556E-1;
    #endif


    /* Gauss points */
    PetscReal dbc2, dbc4;
    /* from Gauss.c: i->r, j->s, k->t */
    PetscReal di2, di4, dj2, dj4, dk2, dk4, dij4;
    #ifdef PETSC_USE_REAL___FLOAT128
    di2 = 0.6Q;
    dj2 = 0.3333333333333333333333333333333333333333333333333333333333333333Q;
    dk2 = 0.9333333333333333333333333333333333333333333333333333333333333333Q;
    #else
    di2 = 0.6;
    dj2 = 3.33333333333333333333333333E-1;
    dk2 = 9.33333333333333333333333333E-1;
    #endif
    di2 /= mesh->_2_Lref2;
    dj2 /= mesh->_2_Lref2;
    dk2 /= mesh->_2_Lref2;
    di4  = di2 * di2;
    dj4  = dj2 * dj2;
    dk4  = dk2 * dk2;
    dij4 = di2 * dj2;
    dbc2 = gw_c*dj2 + gw_b*dk2;
    dbc4 = gw_c*dj4 + gw_b*dk4;


    /* Calculate polynomial (cells)  */
    for (PetscInt sc = 0; sc < stencil->size->cells; sc++)
        poly_cells[sc]  = gamma_diff * C[sc][6] 
                        +     u_conv * C[sc][0]
                        + gamma_diff * (dbc2*C[sc][8] + dbc4*C[sc][11])
                        +     u_conv * (dbc2*C[sc][2] + dbc4*C[sc][5])
            + gw_c *  (
                        gamma_diff * (di2*C[sc][7] + di4*C[sc][9] + dij4*C[sc][10])
                        +   u_conv * (di2*C[sc][1] + di4*C[sc][3] + dij4*C[sc][4]) );


    /* Calculate polynomial (faces)  */
    for (PetscInt sf = 0, si = stencil->size->cells; sf < stencil->size->faces; sf++, si++)
        poly_faces[sf]  = gamma_diff * C[si][6] 
                        +     u_conv * C[si][0]
                        + gamma_diff * (dbc2*C[si][8] + dbc4*C[si][11])
                        +     u_conv * (dbc2*C[si][2] + dbc4*C[si][5])
            + gw_c *  (
                        gamma_diff * (di2*C[si][7] + di4*C[si][9] + dij4*C[si][10])
                        +   u_conv * (di2*C[si][1] + di4*C[si][3] + dij4*C[si][4]) );


    /* Clean up */
    PetscFree2( _C, C );

    return 0;
}

PetscErrorCode Poly8_WLS( Stencil* stencil, MeshGrid* mesh, PetscReal gamma_diff, PetscReal u_conv,
                            PetscReal* poly_cells, PetscReal* poly_faces, Mat P )
{

    const PetscReal *P_arr;
    MatDenseGetArrayRead( P, &P_arr );
    
    PetscReal *_C, **C;
    PetscMalloc2(12*stencil->size->total, &_C,
                    stencil->size->total, &C );

    /* Collect necessary coefficients */
    for (PetscInt i = 0; i < stencil->size->total; i++)
    {
        C[i] = &(_C[i * 12]);

        /* Geometric considerations:
            * gauss->num = 12
            * dn=0
            * dta=0 for type A1 (pts 0,1)
            * dtb=0 for type A2 (pts 2,3)
            * sum (dta^odd) = 0
            * sum (dtb^odd) = 0
            * sum (dta^odd*dtb^odd) = 0
        */

        /* Poly: contribution = term */
        C[i][ 0] = P_arr[i*DIM_8_WLS];          // 1
        C[i][ 1] = P_arr[i*DIM_8_WLS +   7]     // dta2
                 + P_arr[i*DIM_8_WLS +   9];    // dtb2
        C[i][ 2] = P_arr[i*DIM_8_WLS +  30]     // dta4
                 + P_arr[i*DIM_8_WLS +  34];    // dtb4
        C[i][ 3] = P_arr[i*DIM_8_WLS +  30]     // dta4
                 + P_arr[i*DIM_8_WLS +  32]     // dta2.dtb2
                 + P_arr[i*DIM_8_WLS +  34];    // dtb4
        C[i][ 4] = P_arr[i*DIM_8_WLS +  77]     // dta6
                 + P_arr[i*DIM_8_WLS +  83];    // dtb6
        C[i][ 5] = P_arr[i*DIM_8_WLS +  77]     // dta6
                 + P_arr[i*DIM_8_WLS +  79]     // dta4.dtb2
                 + P_arr[i*DIM_8_WLS +  81]     // dta2.dtb4
                 + P_arr[i*DIM_8_WLS +  83];    // dtb6

        /* Flux: contribution = d(term)/dn */
        C[i][ 6] = P_arr[i*DIM_8_WLS +   1];    // dn
        C[i][ 7] = P_arr[i*DIM_8_WLS +  13]     // dn.dta2
                 + P_arr[i*DIM_8_WLS +  15];    // dn.dtb2
        C[i][ 8] = P_arr[i*DIM_8_WLS +  45]     // dn.dta4
                 + P_arr[i*DIM_8_WLS +  49];    // dn.dtb4
        C[i][ 9] = P_arr[i*DIM_8_WLS +  45]     // dn.dta4
                 + P_arr[i*DIM_8_WLS +  47]     // dn.dta2.dtb2
                 + P_arr[i*DIM_8_WLS +  49];    // dn.dtb4
        C[i][10] = P_arr[i*DIM_8_WLS + 105]     // dn.dta6
                 + P_arr[i*DIM_8_WLS + 111];    // dn.dtb6
        C[i][11] = P_arr[i*DIM_8_WLS + 105]     // dn.dta6
                 + P_arr[i*DIM_8_WLS + 107]     // dn.dta4.dtb2
                 + P_arr[i*DIM_8_WLS + 109]     // dn.dta2.dtb4
                 + P_arr[i*DIM_8_WLS + 111];    // dn.dtb6
    }

    MatDenseRestoreArrayRead( P, &P_arr );
    

    /* Gauss weights */
    /* from Gauss.c: gw_a=2*B1, gw_b=4*B2, gw_c=4*B3 */
    PetscReal gw_a, gw_b, gw_c;
    #ifdef PETSC_USE_REAL___FLOAT128
    gw_a = 0.1209876543209876543209876543209876543209876543209876543209876543Q;
    gw_b = 0.5205929166673944571399194320467311660262864130905308672328406807Q;
    gw_c = 0.2374317746906302342181052593112935253317382782674938241251840106Q;
    #else
    gw_a = 1.20987654320987654320987654E-1;
    gw_b = 5.20592916667394457139919432E-1;
    gw_c = 2.37431774690630234218105259E-1;
    #endif


    /* Gauss points */
    /* from Gauss.c: i->r, j->s, k->t */
    PetscReal di2, di4, di6, dj2, dj4, dj6, dk2, dk4, dk6;
    #ifdef PETSC_USE_REAL___FLOAT128
    di2 = 0.8571428571428571428571428571428571428571428571428571428571428571Q;
    dj2 = 0.1448216766345023820401427197502921524169662143452029572052791244Q;
    dk2 = 0.6496034104735115552420872454065022726701417995920792727598776699Q;
    #else
    di2 = 8.57142857142857142857142857E-1;
    dj2 = 1.44821676634502382040142720E-1;
    dk2 = 6.49603410473511555242087245E-1;
    #endif
    di2 /= mesh->_2_Lref2;
    dj2 /= mesh->_2_Lref2;
    dk2 /= mesh->_2_Lref2;
    di4 = di2 * di2;
    dj4 = dj2 * dj2;
    dk4 = dk2 * dk2;
    di6 = di4 * di2;
    dj6 = dj4 * dj2;
    dk6 = dk4 * dk2;


    /* Calculate polynomial (cells)  */
    for (PetscInt sc = 0; sc < stencil->size->cells; sc++)
        poly_cells[sc]  = gamma_diff * C[sc][6] 
                        +     u_conv * C[sc][0]
                        + gw_a * (
                                    gamma_diff * ( di2*C[sc][7] + di4*C[sc][8] + di6*C[sc][10] )
                                    +   u_conv * ( di2*C[sc][1] + di4*C[sc][2] + di6*C[sc][4]  ) )
                        + gw_b *  (
                                    gamma_diff * ( dj2*C[sc][7] + dj4*C[sc][9] + dj6*C[sc][11] )
                                    +   u_conv * ( dj2*C[sc][1] + dj4*C[sc][3] + dj6*C[sc][5]  ) )
                        + gw_c *  (
                                    gamma_diff * ( dk2*C[sc][7] + dk4*C[sc][9] + dk6*C[sc][11] )
                                    +   u_conv * ( dk2*C[sc][1] + dk4*C[sc][3] + dk6*C[sc][5]  ) );


    /* Calculate polynomial (faces)  */
    for (PetscInt sf = 0, si = stencil->size->cells; sf < stencil->size->faces; sf++, si++)
        poly_faces[sf]  = gamma_diff * C[si][6] 
                        +     u_conv * C[si][0]
                        + gw_a * (
                                    gamma_diff * ( di2*C[si][7] + di4*C[si][8] + di6*C[si][10] )
                                    +   u_conv * ( di2*C[si][1] + di4*C[si][2] + di6*C[si][4]  ) )
                        + gw_b *  (
                                    gamma_diff * ( dj2*C[si][7] + dj4*C[si][9] + dj6*C[si][11] )
                                    +   u_conv * ( dj2*C[si][1] + dj4*C[si][3] + dj6*C[si][5]  ) )
                        + gw_c *  (
                                    gamma_diff * ( dk2*C[si][7] + dk4*C[si][9] + dk6*C[si][11] )
                                    +   u_conv * ( dk2*C[si][1] + dk4*C[si][3] + dk6*C[si][5]  ) );


    /* Clean up */
    PetscFree2( _C, C );


    return 0;
}
