#include "CartMesh.h"
#include "Stencil.h"


void StencilSetTypeSizes( SolverSettings* solver, StencilSize* size )
{

    if (solver->method == WLS_2)
    {
        size->sc_inn = 27;
        size->sc_max = 27;

        if (solver->cell_side <= 5)
            size->sf_max = 54;
        else
            size->sf_max = 27;
    }
    else if (solver->method == WLS_4)
    {
        size->sc_inn = 81;
        size->sc_max = 125; // prob overestimated

        if (solver->cell_side <= 7)
            size->sf_max = 150;
        else
            size->sf_max = 75;
    }
    else if (solver->method == WLS_6)
    {
        size->sc_inn = 171;
        size->sc_max = 343;  // prob overestimated
        size->sf_max = 147;
    }
    else  // solver->method == WLS_8
    {
        size->sc_inn = 317;
        size->sc_max = 729; // prob overestimated
        size->sf_max = 243;
    }

    return;
}

/* ------------------------------------ STATIC FUNCTION DECLARATIONS ------------------------------------ */
/* ------------------------------------------------------------------------------------------------------ */

/* Checks if 'cell' is in 'stencil' */
static PetscBool inStencil( PetscInt* stencil, PetscInt len, PetscInt cell );

/* Adds boundary faces of cell 'c' to stencil, and updates bounds */
static void addFaces( MeshGrid* mesh, Stencil* stencil, PetscInt* c,
                        PetscReal *xyz, PetscReal *min, PetscReal *max );

/* ------------------------------------------------------------------------------------------------------ */


PetscInt StencilFaceNBs( SolverSettings* solver, MeshGrid* mesh, Stencil* stencil, PetscInt* f )
{
    
    PetscInt    i, j, v, nx, ny, nz, sc,
                orderx, ordery, orderz, order = 2*solver->method,
                nfront, aux_nfront, *front = NULL, *aux_front = NULL,
                c[4], aux_c[4], aux_f[4],
                nb_cells[8], face_verts[4], face_cells[2], cell_faces[6];
    PetscReal   min[3], max[3], aux_min[3], aux_max[3], aux_xyz[3];

    if (f[3] < mesh->fn_3)
    {
        orderx = order;
        ordery = order + 1;
        orderz = order + 1;
    }
    else if (f[3] < 2*mesh->fn_3)
    {
        orderx = order + 1;
        ordery = order;
        orderz = order + 1;
    }
    else
    {
        orderx = order + 1;
        ordery = order + 1;
        orderz = order;
    }

    getFaceCoords( mesh, f, min );
    max[0] = min[0];
    max[1] = min[1];
    max[2] = min[2];

    stencil->size->cells = 0;
    stencil->size->faces = 0;



    /* Stencil cells: vertex 'v' of face 'f' */
    getFaceVerts( mesh, f, face_verts );
    for (v = 0; v < N_VERTS_PER_FACE; v++)
    {

        getVertCells( mesh, face_verts[v], nb_cells );

        for (i = 0; i < N_CELLS_PER_VERT; i++)
            if (!inStencil(stencil->cells, stencil->size->cells, nb_cells[i]))
            {
                /* Get cell info */
                aux_c[3] = nb_cells[i];
                getCellIJK( mesh, aux_c );
                getCellCoords( mesh, aux_c, aux_xyz );

                /* Add cell to stencil */
                stencil->cells[stencil->size->cells++] = aux_c[3];

                /* Add boundary faces (vertical) to stencil */
                addFaces( mesh, stencil, aux_c, aux_xyz, min, max);
            }
    }



    /* Check if stencil is big enough */
    nx = round( (max[0]-min[0])/(mesh->Lref) + 1.1 );
    ny = round( (max[1]-min[1])/(mesh->Lref) + 1.1 );
    nz = round( (max[2]-min[2])/(mesh->Lref) + 1.1 );
    if ( nx < orderx  ||  ny < ordery  ||  nz < orderz )
    {
        /* Allocate 'front' arrays */
        PetscMalloc2( 6*(order+1)*(order+1), &front, 
                        6*(order+1)*(order+1), &aux_front );
        
        /* Add cells to stencil front */
        for (sc = 0, nfront = 0; sc < stencil->size->cells; sc++)
            front[nfront++] = stencil->cells[sc];
    }


    /* Expand stencil if necessary */
    aux_min[0] = min[0];
    aux_min[1] = min[1];
    aux_min[2] = min[2];
    aux_max[0] = max[0];
    aux_max[1] = max[1];
    aux_max[2] = max[2];
    while ( nx < orderx  ||  ny < ordery  ||  nz < orderz )
    {
        aux_nfront = 0;

        for (i = 0; i < nfront; i++)
        {
            c[3] = front[i];
            getCellIJK( mesh, c );
            getCellFaces( mesh, c, cell_faces );

            for (j = 0; j < N_FACES_PER_CELL; j++)
            {
                /* Choose face 'j' */
                aux_f[3] = cell_faces[j];
                getFaceIJK( mesh, aux_f );

                /* Check if face-neighboring cell exists */
                getFaceCells( mesh, aux_f, face_cells );
                if (face_cells[0] == c[3])
                {
                    if (face_cells[1] == -1)
                        continue;
                    else
                        aux_c[3] = face_cells[1];
                }
                else if (face_cells[0] == -1)
                    continue;
                else
                    aux_c[3] = face_cells[0];

                /* Get cell coordinates  */
                getCellIJK( mesh, aux_c );
                getCellCoords( mesh, aux_c, aux_xyz );

                /* If enough cells on x... */
                if (nx >= orderx)
                    /* check if cell exceeds stencil x-bounds */
                    if ( aux_xyz[0] > max[0]  ||  aux_xyz[0] < min[0] )
                        continue;

                /* If enough cells on y... */
                if (ny >= ordery)
                    /* check if cell exceeds stencil y-bounds */
                    if ( aux_xyz[1] > max[1]  ||  aux_xyz[1] < min[1] )
                        continue;

                /* If enough cells on z... */
                if (nz >= orderz)
                    /* check if cell exceeds stencil z-bounds */
                    if ( aux_xyz[2] > max[2]  ||  aux_xyz[2] < min[2] )
                        continue;

                /* Check if cell is already in stencil */
                if (inStencil( stencil->cells, stencil->size->cells, aux_c[3] ))
                    continue;
                
                /* Add cell to stencil */
                stencil->cells[stencil->size->cells++] = aux_c[3];

                /* Add boundary faces to stencil */
                addFaces( mesh, stencil, aux_c, aux_xyz, aux_min, aux_max );

                /* Add cell to aux front */
                aux_front[aux_nfront++] = aux_c[3];
            }
        }

        /* Update bounds */
        if (aux_min[0] < min[0])  min[0] = aux_min[0];
        if (aux_min[1] < min[1])  min[1] = aux_min[1];
        if (aux_min[2] < min[2])  min[2] = aux_min[2];
        if (aux_max[0] > max[0])  max[0] = aux_max[0];
        if (aux_max[1] > max[1])  max[1] = aux_max[1];
        if (aux_max[2] > max[2])  max[2] = aux_max[2];

        /* Update stencil size */
        nx = round( (max[0]-min[0])/(mesh->Lref) + 1.1 );
        ny = round( (max[1]-min[1])/(mesh->Lref) + 1.1 );
        nz = round( (max[2]-min[2])/(mesh->Lref) + 1.1 );

        /* If stencil needs more cells */
        if ( nx < orderx  ||  ny < ordery  ||  nz < orderz )
        {
            /* Update front */
            nfront = 0;
            for (i = 0; i < aux_nfront; i++)
                front[nfront++]  = aux_front[i];
        }
    }


    /* Calculate stencil size */
    stencil->size->total = stencil->size->cells + stencil->size->faces;


    /* Clean up */
    PetscFree2( front, aux_front );

    
    return 0;
}


/* ---------------------------------------------------------------------------- */


static PetscBool inStencil( PetscInt* stencil, PetscInt len, PetscInt cell )
{
    if (cell < 0)
        return PETSC_TRUE;      // not really TRUE, but dont add since 'cell' doesnt exist

    for (PetscInt i = 0; i < len; i++)
        if (stencil[i] == cell)
            return PETSC_TRUE;

    return PETSC_FALSE;
}

static void addFaces( MeshGrid* mesh, Stencil* stencil, PetscInt* c, 
                        PetscReal *xyz, PetscReal *min, PetscReal *max )
{

    if (!isBndCell(mesh,c))
    {
        if (min[0] > xyz[0])  min[0] = xyz[0];
        if (min[1] > xyz[1])  min[1] = xyz[1];
        if (min[2] > xyz[2])  min[2] = xyz[2];
        if (max[0] < xyz[0])  max[0] = xyz[0];
        if (max[1] < xyz[1])  max[1] = xyz[1];
        if (max[2] < xyz[2])  max[2] = xyz[2];
    }
    else
    {
        PetscInt  bnd, aux_f[4], cell_faces[6];
        bnd = isBndCell(mesh,c);
        getCellFaces( mesh, c, cell_faces );

        /* Add east/west boundary faces to stencil */
        if (bnd%2 == 1)
        {
            /* west */
            aux_f[3] = cell_faces[0];
            getFaceIJK( mesh, aux_f );
            if (isBndFace(mesh,aux_f))
            {
                stencil->faces[(stencil->size->faces)++] = cell_faces[0];
                if (min[0] != 0)        min[0] = 0;
                if (max[0] < xyz[0])    max[0] = xyz[0];
            }
            /* east */
            else
            {
                stencil->faces[(stencil->size->faces)++] = cell_faces[1];
                if (max[0] != 1)        max[0] = 1;
                if (min[0] > xyz[0])    min[0] = xyz[0];
            }
        }
        else
        {
            if (min[0] > xyz[0])  min[0] = xyz[0];
            if (max[0] < xyz[0])  max[0] = xyz[0];
        }
        
        /* Add north/south boundary faces to stencil */
        if (bnd%4 > 1)
        {
            /* south */
            aux_f[3] = cell_faces[2];
            getFaceIJK( mesh, aux_f );
            if (isBndFace(mesh,aux_f))
            {
                stencil->faces[(stencil->size->faces)++] = cell_faces[2];
                if (min[1] != 0)        min[1] = 0;
                if (max[1] < xyz[1])    max[1] = xyz[1];
            }
            /* north */
            else
            {
                stencil->faces[(stencil->size->faces)++] = cell_faces[3];
                if (max[1] != 1)        max[1] = 1;
                if (min[1] > xyz[1])    min[1] = xyz[1];
            }
        }
        else
        {
            if (max[1] < xyz[1])  max[1] = xyz[1];
            if (min[1] > xyz[1])  min[1] = xyz[1];
        }

        /* Add bottom/top boundary faces to stencil */
        if (bnd > 3)
        {
            /* bottom */
            aux_f[3] = cell_faces[4];
            getFaceIJK( mesh, aux_f );
            if (isBndFace(mesh,aux_f))
            {
                stencil->faces[(stencil->size->faces)++] = cell_faces[4];
                if (min[2] != 0)        min[2] = 0;
                if (max[2] < xyz[2])    max[2] = xyz[2];
            }
            /* top */
            else
            {
                stencil->faces[(stencil->size->faces)++] = cell_faces[5];
                if (max[2] != 1)        max[2] = 1;
                if (min[2] > xyz[2])    min[2] = xyz[2];
            }
        }
        else
        {
            if (max[2] < xyz[2])  max[2] = xyz[2];
            if (min[2] > xyz[2])  min[2] = xyz[2];
        }
    }

    return;
}
