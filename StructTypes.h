#ifndef STRUCTTYPES_H
#define STRUCTTYPES_H

#include <petscksp.h>
#include <petscdm.h>

/* ----------------------------------------------------------------------------- */


typedef enum _BNDCOND_TYPE      { DIRICHLET, NEUMANN, ROBIN }           BNDCOND_TYPE;
typedef enum _METHOD_TYPE       { FDM_2, WLS_2, WLS_4, WLS_6, WLS_8 }   METHOD_TYPE;
typedef enum _SOLUTION_TYPE     { SIN }                                 SOLUTION_TYPE;

typedef struct _SolverOps       SolverOps;
typedef struct _SolverSettings  SolverSettings;
typedef struct _MeshGrid        MeshGrid;
typedef struct _Solution        Solution;
typedef struct _Error           Error;
typedef struct _Gauss           Gauss;
typedef struct _StencilSize     StencilSize;
typedef struct _Stencil         Stencil;



/* ----------------------------------------------------------------------------- */


struct Velocity
{
    PetscReal x, y, z;
};

struct Boundary
{
    BNDCOND_TYPE e, w, s, n, b, t;
};

struct _SolverOps
{
    PetscErrorCode  (*reconstruction)(SolverSettings*, MeshGrid*, Stencil*, PetscInt*, PetscReal*, PetscReal*);
};

struct _SolverSettings
{
    /* MPI and OpenMP  info */
    PetscMPIInt         rank,
                        nprocs,
                        nthreads;

    /* Output/Input */
    PetscBool           print_vars,
                        print_matinfo,
                        export_linsys,
                        import_linsys;
    PetscChar           linsys_fname[100];

    /* Geometry and Properties */
    PetscInt            cell_side;
    PetscReal           gamma_diff;
    struct Velocity*    u_conv;
    struct Boundary*    bndcond;

    /* Solver Methods */
    METHOD_TYPE         method;
    SOLUTION_TYPE       solution;
    KSPType             ksptype;
    PCType              pctype;
    PetscInt            asm_overlap;    // for PCASM only
    PetscReal           gamg_threshold; // for PCGAMG only
    PetscReal           abstol,
                        reltol,
                        divtol;
    PetscInt            maxits,
                        weight;
    PetscBool           custom_KSP;

    /* Solver Functions */
    struct _SolverOps*  ops;

    /* Solver Results */
    PetscInt            totits;
    KSPConvergedReason  reason;

    /* Extra Mat Options */
    MatInfo*            matinfo;
    PetscBool           alloc_opt;
    
};


/* ----------------------------------------------------------------------------- */


struct _MeshGrid
{
    /* General variables */
    PetscInt    cell_side,
                vert_side,
                cell_num,
                vert_num,
                face_num;
    PetscInt    cs2;                // cell_side^2
    PetscInt    vs2;                // vert_side^2
    PetscInt    csXvs;              // cell_side * vert_side
    PetscInt    fn_3;               // face_num/3
    PetscReal   Lref;               // cell edge length
    PetscReal   _2_Lref2;           // (2/Lref)^2
    PetscReal   _2_Lref3;           // (2/Lref)^3
    PetscReal   area;               // face area (Lref^2)
    PetscReal   vol;                // cell volume (Lref^3)

    /* DM-related info */
    PetscInt    ci_start, ci_end,   // corner indices for DMDA dm_cells
                cj_start, cj_end,
                ck_start, ck_end;
    PetscInt    cm, cn, cp;         // dimension widths for DMDA dm_cells
    PetscInt    cell_num_local;     // local size of 'cells' vec
    // PetscInt    start[3];       // starting element index in each direction
    // PetscInt    n[3];           // element width in each direction
    // PetscInt    nExtra[3];      // extra partial elements in each direction
    // PetscInt    slotW[3];       // location slots for face W
    // PetscInt    slotS[3];       // location slots for face S
    // PetscInt    slotB[3];       // location slots for face B

};


/* ----------------------------------------------------------------------------- */


struct SolutionAux
{
    Vec         phi,
                source;
    PetscReal   *phi_faces,
                *flux;
};

struct _Solution
{
    DM                      dm;
    struct SolutionAux*     anal;
    struct SolutionAux*     num;
};


/* ----------------------------------------------------------------------------- */


struct ErrorAux
{
    PetscReal   norm1,
                norm2,
                norminf,
                valmax;
    PetscInt    cellmax;
};

struct _Error
{
    struct ErrorAux*    abs;
    struct ErrorAux*    rel;
};


/* ----------------------------------------------------------------------------- */


struct _Gauss
{
    PetscInt    num;
    PetscReal   *weight;
    Vec         *points;
};


/* ----------------------------------------------------------------------------- */


struct _StencilSize
{
    PetscInt    total,
                cells,
                faces,
                sc_inn, 
                sc_max, 
                sf_max;
};

struct _Stencil
{
    PetscInt        *cells,
                    *faces;
    StencilSize*    size;
};


/* ----------------------------------------------------------------------------- */

#endif // !STRUCTTYPES_H
