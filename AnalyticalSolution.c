#include "AnalyticalSolution.h"
#include "CartMesh.h"
#include "Gauss.h"

#define QUIET

/* ----------------------------- Static Function Declarations ----------------------------- */
/* ---------------------------------------------------------------------------------------- */
static PetscErrorCode calcFACE_sin( MeshGrid* mesh, PetscReal* phi_faces );
static PetscErrorCode calcFLUX_sin( MeshGrid* mesh, PetscReal* flux );
static PetscErrorCode calcPHI_sin( void *ctx, PetscInt n, const PetscScalar *xyz, PetscScalar *phi);
static PetscErrorCode calcGRADx_sin( void *ctx, PetscInt n, const PetscScalar *xyz, PetscScalar *grad);
static PetscErrorCode calcGRADy_sin( void *ctx, PetscInt n, const PetscScalar *xyz, PetscScalar *grad);
static PetscErrorCode calcGRADz_sin( void *ctx, PetscInt n, const PetscScalar *xyz, PetscScalar *grad);
/* ---------------------------------------------------------------------------------------- */


typedef struct _CalcFuncs CalcFuncs;

struct _CalcFuncs
{
    PetscErrorCode  (*face)(MeshGrid*, PetscReal*);
    PetscErrorCode  (*flux)(MeshGrid*, PetscReal*);
    PetscErrorCode  (*phi)(void*, PetscInt, const PetscScalar*, PetscScalar*);
    PetscErrorCode  (*gradX)(void*, PetscInt, const PetscScalar*, PetscScalar*);
    PetscErrorCode  (*gradY)(void*, PetscInt, const PetscScalar*, PetscScalar*);
    PetscErrorCode  (*gradZ)(void*, PetscInt, const PetscScalar*, PetscScalar*);
    PetscErrorCode  (*lap)(void*, PetscInt, const PetscScalar*, PetscScalar*);
};



static void SetCalcFuncs( SOLUTION_TYPE type, CalcFuncs *calc )
{
    if (type == SIN)
    {
        calc->face  = calcFACE_sin;
        calc->flux  = calcFLUX_sin;
        calc->phi   = calcPHI_sin;
        calc->gradX = calcGRADx_sin;
        calc->gradY = calcGRADy_sin;
        calc->gradZ = calcGRADz_sin;
        calc->lap   = calcPHI_sin;
    }

    return;
}

static void SetCoeffs( SolverSettings* solver, PetscReal *coeffs )
{
    if (solver->solution == SIN)
    {
        coeffs[0] =  3. * PI;           // xyz coord coeff
        coeffs[1] =  3. * PI;           // x-grad coeff
        coeffs[2] =  3. * PI;           // y-grad coeff
        coeffs[3] =  3. * PI;           // z-grad coeff
        coeffs[4] = 27. * PI * PI;      // lap coeff
    }

    coeffs[1] *= solver->u_conv->x;
    coeffs[2] *= solver->u_conv->y;
    coeffs[3] *= solver->u_conv->z;
    coeffs[4] *= -solver->gamma_diff;

    return;
}



PetscErrorCode AnalyticalSolution( SolverSettings* solver, MeshGrid* mesh, Solution* sol )
{

    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\nCALCULATING ANALYTICAL SOLUTION\n\tstarted ...\n");
    #endif // !QUIET

    CalcFuncs   *calc;
    PF          pf;
    Vec         kxyz, lap, *grad;
    PetscReal   aux0, aux1, coeffs[5];

    PetscMalloc1(1, &calc);
    SetCoeffs( solver, coeffs );
    SetCalcFuncs( solver->solution, calc );



    /* Create Data Management object for Solution */
    aux0 = coeffs[0] * .5 * mesh->Lref;
    aux1 = coeffs[0] - aux0;
    DMDACreate3d( PETSC_COMM_WORLD, DM_BOUNDARY_NONE, DM_BOUNDARY_NONE, DM_BOUNDARY_NONE,
                    DMDA_STENCIL_BOX, mesh->cell_side, mesh->cell_side, mesh->cell_side,
                    PETSC_DECIDE, PETSC_DECIDE, PETSC_DECIDE, 1, solver->method,
                    NULL, NULL, NULL, &(sol->dm) );
    DMSetFromOptions( sol->dm );
    DMSetUp( sol->dm );
    DMDASetUniformCoordinates( sol->dm, aux0, aux1, aux0, aux1, aux0, aux1 );
    DMDACreatePF( sol->dm, &pf );
    setMeshDMSize( mesh, sol->dm );



    /* Create vecs for cell-centered values */
    DMGetCoordinates( sol->dm, &kxyz );
    DMCreateGlobalVector( sol->dm, &(sol->anal->phi) );
    VecDuplicate( sol->anal->phi, &(sol->anal->source) );
    VecDuplicate( sol->anal->phi, &lap );
    VecDuplicateVecs( sol->anal->phi, 3, &grad );



    /* Allocate vecs for boundary-face values */
    sol->anal->phi_faces = malloc( 6 * mesh->cs2 * sizeof(PetscReal) );
    sol->anal->flux      = malloc( 6 * mesh->cs2 * sizeof(PetscReal) );




    /*--- Analytical Function on FACE Centroid ---*/
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\tcalculating:\t phi on faces ... ");
    #endif // !QUIET

    (*calc->face)( mesh, sol->anal->phi_faces );

    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "done\n");
    #endif // !QUIET


    /*--- Face Fluxes ---*/
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\tcalculating:\t face fluxes ... ");
    #endif // !QUIET

    (*calc->flux)( mesh, sol->anal->flux );

    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "done\n");
    #endif // !QUIET



    /*--- Analytical Function on CELL Centroid---*/
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\tcalculating:\t phi on cells ... ");
    #endif // !QUIET
    
    PFSet( pf, calc->phi, NULL, NULL, NULL, NULL );
    PFApplyVec( pf, kxyz, sol->anal->phi );
    
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "done\n");
    #endif // !QUIET



    /*--- Source ---*/
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\tcalculating:\t source term on cell ");
    #endif // !QUIET

    if ( solver->method == FDM_2  ||  solver->method == WLS_2 )
    {
        PetscPrintf( PETSC_COMM_WORLD, "... ");

        /* x-flux */
        PFSet( pf, calc->gradX, NULL, NULL, NULL, NULL );
        PFApplyVec( pf, kxyz, grad[0] );

        /* y-flux */
        PFSet( pf, calc->gradY, NULL, NULL, NULL, NULL );
        PFApplyVec( pf, kxyz, grad[1] );

        /* z-flux */
        PFSet( pf, calc->gradZ, NULL, NULL, NULL, NULL );
        PFApplyVec( pf, kxyz, grad[2] );

        /* laplacian */
        PFSet( pf, calc->lap, NULL, NULL, NULL, NULL );
        PFApplyVec( pf, kxyz, lap );

        /* source (overwrite 'lap') */
        VecAXPBY( sol->anal->source, coeffs[4], 0., lap );
        VecMAXPY( sol->anal->source, 3, &(coeffs[1]), grad );
    }
    else
    {
        #ifndef QUIET
        PetscPrintf( PETSC_COMM_WORLD, "(gauss ");
        #endif // !QUIET

        Gauss* gauss;
        PetscMalloc1(1, &gauss);

        GaussPoints3D( solver, mesh, gauss, kxyz, coeffs[0] );
        
        #ifndef QUIET
        PetscPrintf( PETSC_COMM_WORLD, "n=%d) ... ", gauss->num);
        #endif // !QUIET

        /* source contribution for each gauss point */
        Vec aux;
        VecDuplicate( sol->anal->source, &aux );
        for (PetscInt n = 0; n < gauss->num; n++)
        {
            /* x-flux */
            PFSet( pf, calc->gradX, NULL, NULL, NULL, NULL );
            PFApplyVec( pf, gauss->points[n], grad[0] );

            /* y-flux */
            PFSet( pf, calc->gradY, NULL, NULL, NULL, NULL );
            PFApplyVec( pf, gauss->points[n], grad[1] );

            /* z-flux */
            PFSet( pf, calc->gradZ, NULL, NULL, NULL, NULL );
            PFApplyVec( pf, gauss->points[n], grad[2] );

            /* laplacian */
            PFSet( pf, calc->lap, NULL, NULL, NULL, NULL );
            PFApplyVec( pf, gauss->points[n], lap );

            /* source */
            VecAXPBY( aux, coeffs[4], 0., lap );
            VecMAXPY( aux, 3, &(coeffs[1]), grad );

            /* sum up contributions */
            VecAXPY( sol->anal->source, gauss->weight[n], aux );
        }

        /* Clean up */
        VecDestroy( &aux );
        VecDestroyVecs( gauss->num, &(gauss->points) );
        PetscFree(gauss->weight);
        PetscFree(gauss);
    }
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "done\n");
    #endif // !QUIET



    /* Chop vecs */
    VecChop( sol->anal->phi, solver->abstol );
    VecChop( sol->anal->source, solver->abstol );



    /* Clean up */
    PetscFree( calc );
    PFDestroy( &pf );
    VecDestroy( &lap );
    VecDestroyVecs( 3, &grad );



    /* Print vars */
    if (solver->print_vars)
    {
        PetscViewer viewer;
        FILE        *fout;
        
        PetscPrintf( PETSC_COMM_WORLD, "\texporting: \'phi\' -> \'_sol-anal-phi.out\'\n");
        PetscViewerASCIIOpen( PETSC_COMM_WORLD, "_sol-anal-phi.out", &viewer );
        VecView( sol->anal->phi, viewer );
        PetscViewerDestroy( &viewer );

        PetscPrintf( PETSC_COMM_WORLD, "\texporting: \'source\' -> \'_sol-anal-source.out\'\n");
        PetscViewerASCIIOpen( PETSC_COMM_WORLD, "_sol-anal-source.out", &viewer );
        VecView( sol->anal->source, viewer );
        PetscViewerDestroy( &viewer );
    
        PetscPrintf( PETSC_COMM_WORLD, "\texporting: \'phi_faces\' -> \'_sol-anal-phi_faces.out\'\n");
        PetscFOpen( PETSC_COMM_WORLD, "_sol-anal-phi_faces.out", "w", &fout);
        for (PetscInt i = 0; i < 6*mesh->cs2; i++)
            PetscFPrintf( PETSC_COMM_WORLD, fout, "%f\n", (double) sol->anal->phi_faces[i]);
        PetscFClose(PETSC_COMM_WORLD, fout);
        
        PetscPrintf( PETSC_COMM_WORLD, "\texporting: \'flux\' -> \'_sol-anal-flux.out\'\n");
        PetscFOpen( PETSC_COMM_WORLD, "_sol-anal-flux.out", "w", &fout);
        for (PetscInt i = 0; i < 6*mesh->cs2; i++)
            PetscFPrintf( PETSC_COMM_WORLD, fout, "%f\n", (double) sol->anal->flux[i]);
        PetscFClose(PETSC_COMM_WORLD, fout);
    }

    return 0;
}


/* ------------------------------------------------------------------------------------- */


static PetscErrorCode calcFACE_sin( MeshGrid* mesh, PetscReal *phi_faces )
{

    PetscInt    l;                  // boundary index
    PetscReal   i, j, k;
    PetscReal   _A, _ALref;

    // _A   =      PI;
    _A     =  3. * PI;
    _ALref = _A  * mesh->Lref;



    /* west (x=0 ==> sin(x)=0 ==> phi=0) */
    for (l = 0; l < mesh->cs2; l++)
        phi_faces[l] = 0;

    /* east (x=1) */
    for (j = 0.5; j < mesh->cell_side; j++)
        for (k = 0.5; k < mesh->cell_side; k++, l++)
            phi_faces[l] = SINF(_A) * SINF(_ALref*j) * SINF(_ALref*k);
    


    /* south, north */
    for (i = 0.5; i < mesh->cell_side; i++)
    {
        /* south (y=0 ==> sin(y)=0 ==> phi=0) */
        for (k = 0.5; k < mesh->cell_side; k++, l++)
            phi_faces[l] = 0;

        /* north (y=1) */
        for (k = 0.5; k < mesh->cell_side; k++, l++)
            phi_faces[l] = SINF(_ALref*i) * SINF(_A) * SINF(_ALref*k);
    }



    /* bottom, top */
    for (i = 0.5; i < mesh->cell_side; i++)
        for (j = 0.5; j < mesh->cell_side; j++)
        {
            /* bottom  (z=0 ==> sin(z)=0 ==> phi=0) */
            phi_faces[l++] = 0;

            /* top (z=1) */
            phi_faces[l++] = SINF(_ALref*i) * SINF(_ALref*j) * SINF(_A);
        }



    return 0;
}


static PetscErrorCode calcFLUX_sin( MeshGrid* mesh, PetscReal *flux )
{

    PetscInt    l = 0;              // boundary index
    PetscReal   i, j, k;
    PetscReal   _A, _ALref;

    // _A   =      PI;
    // _3A  = 3. * PI;
    _A     =  3. * PI;
    _ALref = _A  * mesh->Lref;



    /* west (x=0 ==> cos(x)=1) */
    for (j = 0.5; j < mesh->cell_side; j++)
        for (k = 0.5; k < mesh->cell_side; k++, l++)
            flux[l] = _A * SINF(_ALref*j) * SINF(_ALref*k);

    /* east (x=1) */
    for (j = 0.5; j < mesh->cell_side; j++)
        for (k = 0.5; k < mesh->cell_side; k++, l++)
            flux[l] = _A * COSF(_A) * SINF(_ALref*j) * SINF(_ALref*k);
    


    /* south, north */
    for (i = 0.5; i < mesh->cell_side; i++)
    {
        /* south (y=0 ==> cos(y)=1) */
        for (k = 0.5; k < mesh->cell_side; k++, l++)
            flux[l] = _A * SINF(_ALref*i) * SINF(_ALref*k);

        /* north (y=1) */
        for (k = 0.5; k < mesh->cell_side; k++, l++)
            flux[l] = _A * SINF(_ALref*i) * COSF(_A) * SINF(_ALref*k);
    }



    /* bottom, top */
    for (i = 0.5; i < mesh->cell_side; i++)
        for (j = 0.5; j < mesh->cell_side; j++)
        {
            /* bottom  (z=0 ==> cos(z)=1) */
            flux[l++] = _A * SINF(_ALref*i) * SINF(_ALref*j);

            /* top (z=1) */
            flux[l++] = _A * SINF(_ALref*i) * SINF(_ALref*j) * COSF(_A);
        }



    return 0;
}


static PetscErrorCode calcPHI_sin( void *ctx, PetscInt n, const PetscScalar *xyz, PetscScalar *phi)
{
    for (PetscInt i = 0; i < n; i++)
        phi[i]  = SINF( xyz[3*i]   )
                * SINF( xyz[3*i+1] ) 
                * SINF( xyz[3*i+2] );
    
    return 0;
}


static PetscErrorCode calcGRADx_sin( void *ctx, PetscInt n, const PetscScalar *xyz, PetscScalar *grad)
{
    for (PetscInt i = 0; i < n; i++)
        grad[i] = COSF( xyz[3*i]   )
                * SINF( xyz[3*i+1] )
                * SINF( xyz[3*i+2] );

    return 0;
}


static PetscErrorCode calcGRADy_sin( void *ctx, PetscInt n, const PetscScalar *xyz, PetscScalar *grad)
{
    for (PetscInt i = 0; i < n; i++)
        grad[i] = SINF( xyz[3*i]   )
                * COSF( xyz[3*i+1] )
                * SINF( xyz[3*i+2] );

    return 0;
}


static PetscErrorCode calcGRADz_sin( void *ctx, PetscInt n, const PetscScalar *xyz, PetscScalar *grad)
{
    for (PetscInt i = 0; i < n; i++)
        grad[i] = SINF( xyz[3*i]   )
                * SINF( xyz[3*i+1] )
                * COSF( xyz[3*i+2] );

    return 0;
}

