#ifndef CARTMESH_H
#define CARTMESH_H


#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <petscdmda.h>
#include <petscdmstag.h>

#ifdef PETSC_USE_REAL___FLOAT128
#include <quadmath.h>
#endif

#include "StructTypes.h"


/* 3D Geometric Constants */
/* ---------------------- */
#define N_DIMS 3
#define N_VERTS_PER_CELL 8
#define N_FACES_PER_CELL 6
#define N_CELLS_PER_VERT 8
#define N_VERTS_PER_FACE 4
/* ---------------------- */


void CartMesh( SolverSettings* solver, MeshGrid* mesh );

void setMeshDMSize( MeshGrid* mesh, DM dm );

void MeshDestroy( MeshGrid* mesh );

void getCellIJK( MeshGrid* mesh, PetscInt* c );

void getFaceIJK( MeshGrid* mesh, PetscInt* f );

void getCellCoords( MeshGrid* mesh, PetscInt* c, PetscReal* xyz );

void getFaceCoords( MeshGrid* mesh, PetscInt* f, PetscReal* xyz );

void getFaceVerts( MeshGrid* mesh, PetscInt* f, PetscInt* face_verts );

void getFaceCells( MeshGrid* mesh, PetscInt* f, PetscInt* face_cells );

void getVertCells( MeshGrid* mesh, PetscInt v, PetscInt* vert_cells );

void getCellFaces( MeshGrid* mesh, PetscInt* c, PetscInt* cell_faces );

PetscBool isBndFace( MeshGrid* mesh, PetscInt* f );

PetscInt isBndCell( MeshGrid* mesh, PetscInt* c );

PetscInt totalUniqueStencil( MeshGrid* mesh, PetscInt bw );

PetscInt faceGlobal2Bnd( MeshGrid* mesh, PetscInt f );


#endif // !CARTMESH_H