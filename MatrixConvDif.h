#ifndef MATRIXCONVDIF_H
#define MATRIXCONVDIF_H


#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <petscmat.h>

#ifdef PETSC_USE_REAL___FLOAT128
#include <quadmath.h>
#endif

#include "StructTypes.h"

#define NUM_FACES_PER_CELL   6
#define STENCIL_TOL          1


PetscErrorCode MatrixConvDif( SolverSettings* solver, MeshGrid* mesh, Solution* sol, Mat A, Vec b );


#endif // !MATRIXCONVDIF_H