#ifndef GAUSS_H
#define GAUSS_H


#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <petscvec.h>

#ifdef PETSC_USE_REAL___FLOAT128
#include <quadmath.h>
#endif

#ifndef NDIMS
#define NDIMS 3
#endif

#include "StructTypes.h"


PetscErrorCode GaussPoints3D( SolverSettings* solver, MeshGrid* mesh,
                                Gauss* gauss, Vec kxyz, PetscReal k );


#endif // !GAUSS_H