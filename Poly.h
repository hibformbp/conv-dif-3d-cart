#ifndef POLY_H
#define POLY_H


#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <petscmat.h>

#ifdef PETSC_USE_REAL___FLOAT128
#include <quadmath.h>
#endif

#include "StructTypes.h"


/* ------------- Weighted Least-Squares -------------- */

PetscErrorCode Poly2_WLS( Stencil* stencil, MeshGrid* mesh, PetscReal gamma_diff, PetscReal u_conv,
                            PetscReal* poly_cells, PetscReal* poly_faces, Mat P );

PetscErrorCode Poly4_WLS( Stencil* stencil, MeshGrid* mesh, PetscReal gamma_diff, PetscReal u_conv,
                            PetscReal* poly_cells, PetscReal* poly_faces, Mat P );

PetscErrorCode Poly6_WLS( Stencil* stencil, MeshGrid* mesh, PetscReal gamma_diff, PetscReal u_conv,
                            PetscReal* poly_cells, PetscReal* poly_faces, Mat P );

PetscErrorCode Poly8_WLS( Stencil* stencil, MeshGrid* mesh, PetscReal gamma_diff, PetscReal u_conv,
                            PetscReal* poly_cells, PetscReal* poly_faces, Mat P );


#endif // !POLY_H