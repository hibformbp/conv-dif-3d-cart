#ifndef ANALYTICALSOLUTION_H
#define ANALYTICALSOLUTION_H

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <petscdmda.h>
#include <petscpf.h>

#ifdef PETSC_USE_REAL___FLOAT128
    #include <quadmath.h>
    #define  SINF(x)  sinq((x))
    #define  COSF(x)  cosq((x))
    #ifndef M_PIq
        #define M_PIq 3.141592653589793238462643383279502884Q  /* pi (quad) */
    #endif
    #define PI M_PIq
#else
    #define  SINF(x)  sin((x))
    #define  COSF(x)  cos((x))
    #ifndef M_PI
        #define M_PI  3.14159265358979323846	/* pi (double) */
    #endif
    #define PI M_PI
#endif

#include "StructTypes.h"



void SetOps_Solution( SolverOps* ops, SOLUTION_TYPE type );

PetscErrorCode AnalyticalSolution( SolverSettings* solver, MeshGrid* mesh, Solution* sol );


#endif // !ANALYTICALSOLUTION_H