#include "CartMesh.h"
#include "MatrixConvDif.h"
#include "Reconstruction.h"
#include "Stencil.h"

#define QUIET
// #define DEBUG
// #define PRINT_STENCIL


// TODO: 
//  >> neumann, robin BC's


PetscErrorCode MatrixConvDif( SolverSettings* solver, MeshGrid* mesh, Solution* sol, Mat A, Vec b )
{
    
    
    PetscInt        i, sc, sf, c[4], f[4], cell_faces[6],           // c/f[4] = {i, j, k, c/f}
                    count = 0, progress = 0;
    PetscReal       aux_conv, *aux_A, aux_b, border_cells,
                    *poly_cells, *poly_faces,
                    *aux_A_inner_w = NULL, *aux_A_inner_e = NULL,
                    *aux_A_inner_s = NULL, *aux_A_inner_n = NULL,
                    *aux_A_inner_b = NULL, *aux_A_inner_t = NULL;
    AO              ao;
    Stencil         *stencil;
    BNDCOND_TYPE    bndcond_type;


    DMDAGetAO( sol->dm, &ao );

    border_cells = totalUniqueStencil(mesh,solver->method+1);
    // if (solver->nprocs == 1)
    // {
    //     PetscInt inner_side = mesh->cell_side - 2*(solver->method+1);
    //     border_cells = mesh->cell_num - inner_side*inner_side*inner_side;           // total - inner
    // }
    // else
    // {
    //     /* only for rank=1 */
    //     PetscInt inner_base = mesh->cell_side - 2*(solver->method+1);
    //     PetscInt inner_height = mesh->cell_side - (solver->method+1);
    //     border_cells = mesh->cell_num_local - inner_height*inner_base*inner_base;   // total - inner
    // }

    PetscMalloc1( 1, &stencil );
    PetscMalloc1( 1, &(stencil->size) );
    StencilSetTypeSizes( solver, stencil->size );
    PetscMalloc2( stencil->size->sc_max, &(stencil->cells),
                    stencil->size->sf_max, &(stencil->faces) );

    PetscMalloc2( stencil->size->sc_max, &poly_cells,
                    stencil->size->sf_max, &poly_faces );


    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\n\t\t.0  .1  .2  .3  .4  .5  .6  .7  .8  .9  1\n\t\t|");
    #endif // !QUIET

    for (c[0] = mesh->ci_start, progress = 0; c[0] < mesh->ci_end; c[0]++)
    for (c[1] = mesh->cj_start; c[1] < mesh->cj_end; c[1]++)
    for (c[2] = mesh->ck_start; c[2] < mesh->ck_end; c[2]++)
    {

        #ifndef QUIET
        if ((count*40)/border_cells > progress)
            for ( ; progress < (count*40)/border_cells; progress++)
                PetscPrintf( PETSC_COMM_WORLD, ">");
        #endif // !QUIET

        aux_b = 0.;

        /* Identify Cell and Neighbors */
        c[3] = c[0]*mesh->cs2 + c[1]*mesh->cell_side + c[2];
        getCellFaces( mesh, c, cell_faces );


        /* Iterate on Face 'i' of Cell 'c' */
        for (i = 0; i < NUM_FACES_PER_CELL; i++)
        {

            /* Define Face 'f' as Face 'i' of Cell 'c' */
            f[3]  = cell_faces[i];
            getFaceIJK( mesh, f );
            

            /* Calculate Stencil for Face 'f' */
            StencilFaceNBs( solver, mesh, stencil, f );
            #ifdef PRINT_STENCIL
            PetscPrintf(PETSC_COMM_SELF,"\n[%d] %lld: ",solver->rank,c[3]);
            for (PetscInt i = 0; i < stencil->size->cells; i++)
                PetscPrintf(PETSC_COMM_SELF," %4lld",stencil->cells[i]);
            #endif // PRINT_STENCIL


            /* Reconstructs the Polynomial 'T' on Face 'f' */
            if (stencil->size->faces == 0)          // is: inner stencil
            {
                if (f[3] < mesh->fn_3)                     // is: x-normal face
                {
                    if (aux_A_inner_e == NULL)              // first: inner x-normal face
                    {
                        PetscMalloc2( stencil->size->sc_inn, &aux_A_inner_e,
                                        stencil->size->sc_inn, &aux_A_inner_w );
                        (solver->ops->reconstruction)( solver, mesh, stencil, f, aux_A_inner_e, NULL );
                        for (sc = 0; sc < stencil->size->sc_inn; sc++)
                            aux_A_inner_w[sc] = -aux_A_inner_e[sc];
                    }
                    if (i == 0)
                        aux_A = aux_A_inner_w;
                    else
                        aux_A = aux_A_inner_e;
                }
                else if (f[3] < 2*mesh->fn_3)
                {
                    if (aux_A_inner_n == NULL)              // first: inner y-normal face 
                    {
                        PetscMalloc2( stencil->size->sc_inn, &aux_A_inner_n,
                                        stencil->size->sc_inn, &aux_A_inner_s );
                        (solver->ops->reconstruction)( solver, mesh, stencil, f, aux_A_inner_n, NULL );
                        for (sc = 0; sc < stencil->size->sc_inn; sc++)
                            aux_A_inner_s[sc] = -aux_A_inner_n[sc];
                    }
                    if (i == 2)
                        aux_A = aux_A_inner_s;
                    else
                        aux_A = aux_A_inner_n;
                }
                else
                {
                    if (aux_A_inner_t == NULL)              // first: inner z-normal face 
                    {
                        PetscMalloc2( stencil->size->sc_inn, &aux_A_inner_t,
                                        stencil->size->sc_inn, &aux_A_inner_b );
                        (solver->ops->reconstruction)( solver, mesh, stencil, f, aux_A_inner_t, NULL );
                        for (sc = 0; sc < stencil->size->sc_inn; sc++)
                            aux_A_inner_b[sc] = -aux_A_inner_t[sc];
                    }
                    if (i == 4)
                        aux_A = aux_A_inner_b;
                    else
                        aux_A = aux_A_inner_t;
                }
            }

            else
            {
                (*solver->ops->reconstruction)( solver, mesh, stencil, f, poly_cells, poly_faces );
                if (i%2 == 0)
                {
                    for (sc = 0; sc < stencil->size->cells; sc++)
                        poly_cells[sc] = -poly_cells[sc];
                    for (sf = 0; sf < stencil->size->faces; sf++)
                        poly_faces[sf] = -poly_faces[sf];
                }
                aux_conv = ( f[3] < mesh->fn_3 
                                ? solver->u_conv->x 
                                : ( f[3] < 2*mesh->fn_3 
                                    ? solver->u_conv->y 
                                    : solver->u_conv->z ) );
                aux_A = poly_cells;

                /* Calculate Contribution from each Stencil Face 'sf' */
                for (sf = 0; sf < stencil->size->faces; sf++)
                {
                    if (stencil->faces[sf] < mesh->cs2)
                        bndcond_type = solver->bndcond->w;
                    else if (stencil->faces[sf] < mesh->fn_3)
                        bndcond_type = solver->bndcond->e;
                    else if (stencil->faces[sf] < 2*mesh->fn_3)
                    {
                        if ((stencil->faces[sf]-mesh->fn_3)%mesh->csXvs < mesh->cell_side)
                            bndcond_type = solver->bndcond->s;
                        else
                            bndcond_type = solver->bndcond->n;
                    }
                    else if ((stencil->faces[sf]-2*mesh->fn_3)%mesh->vert_side == 0)
                        bndcond_type = solver->bndcond->b;
                    else
                        bndcond_type = solver->bndcond->t;

                    if (bndcond_type == DIRICHLET)
                        aux_b -= poly_faces[sf] * sol->anal->phi_faces[faceGlobal2Bnd(mesh,stencil->faces[sf])];
                    else if (bndcond_type == NEUMANN)
                        aux_b -= poly_faces[sf] * sol->anal->flux[faceGlobal2Bnd(mesh,stencil->faces[sf])] * mesh->area;
                    else  // bndcond_type == ROBIN
                        aux_b -= poly_faces[sf] * 
                                (  sol->anal->flux[faceGlobal2Bnd(mesh,stencil->faces[sf])] * mesh->area * solver->gamma_diff/aux_conv
                                    +   sol->anal->phi_faces[faceGlobal2Bnd(mesh,stencil->faces[sf])]      );
                }
            }

            /* Add Entries to Matrix 'A' */
            sc = c[3];
            AOApplicationToPetsc( ao, 1, &sc );
            AOApplicationToPetsc( ao, stencil->size->cells, stencil->cells );
            #ifdef PRINT_STENCIL
            PetscPrintf(PETSC_COMM_SELF,"\n[%d] %lld: ",solver->rank,sc);
            for (PetscInt i = 0; i < stencil->size->cells; i++)
                PetscPrintf(PETSC_COMM_SELF," %4lld",stencil->cells[i]);
            #endif // PRINT_STENCIL
            MatSetValues( A, 1, &sc, stencil->size->cells, stencil->cells, aux_A, ADD_VALUES );
        }
        if (aux_b != 0.)
        {
            /* Insert Entries into Vector 'b'  */
            VecSetValue( b, c[3], aux_b*mesh->area, INSERT_VALUES );
            count++;
        }
    }
    


    /* Assemble Matrix 'A' and Vector 'b' */
    MatAssemblyBegin( A, MAT_FINAL_ASSEMBLY );
    VecAssemblyBegin( b );
    MatAssemblyEnd( A, MAT_FINAL_ASSEMBLY );
    VecAssemblyEnd( b );



    /* Final calculations */
    // MatChop( A, solver->abstol );
    // VecChop( b, solver->abstol );
    MatScale( A, mesh->area );
    VecAXPY ( b, mesh->vol, sol->anal->source );



    /* Clean up */
    PetscFree2( poly_cells, poly_faces );
    PetscFree2( stencil->cells, stencil->faces );
    PetscFree( stencil->size );
    PetscFree( stencil );
    if(aux_A_inner_w)  PetscFree2( aux_A_inner_e, aux_A_inner_w );
    if(aux_A_inner_s)  PetscFree2( aux_A_inner_n, aux_A_inner_s );
    if(aux_A_inner_b)  PetscFree2( aux_A_inner_t, aux_A_inner_b );


    return 0;
}
