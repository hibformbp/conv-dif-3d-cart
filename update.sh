#!/bin/bash

echo ""
echo "__________ Fetching git ___________"
git fetch || exit

echo "" 
echo "___________ Pulling git ___________"
git pull || exit

echo "" 
echo "______ Removing object files ______"
rm -v *.o

echo "" 
echo "________ Making executable ________"
make solver3d
