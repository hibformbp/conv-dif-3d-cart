
#include "MatrixConvDif.h"
#include "Stencil.h"
#include "WeightedLeastSquares.h"

#define QUIET
#define NONZERO_GUESS   PETSC_TRUE
#define PRELOAD         PETSC_FALSE

/* ----------------------- Static Function Declarations ----------------------- */
/* ---------------------------------------------------------------------------- */
static void getMatNZs( SolverSettings* solver, PetscInt* d_nz, PetscInt* o_nz );
/* ---------------------------------------------------------------------------- */


PetscErrorCode WeightedLeastSquares( SolverSettings* solver, MeshGrid* mesh, Solution* sol )
{

    // PetscPreLoadBegin(PRELOAD,"Prep");

    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\nCALCULATING NUMERICAL SOLUTION\n\tstarted ...\n");
    #endif // !QUIET
    
    PetscInt            d_nz, o_nz;     // num. nonzeros per row
    Mat                 A;      
    KSP                 ksp;            // linear solver context
    PC                  pc;             // preconditioner context



    /* ---------------------------------------------------------------------------------- */



    /* Create linear equation Ax=b objects */
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\tcreating lin eq objects ");
    #endif // !QUIET

    VecDuplicate( sol->anal->phi, &(sol->num->phi) );
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, ".");
    #endif // !QUIET

    VecDuplicate( sol->anal->source, &(sol->num->source) );
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, ".");
    #endif // !QUIET

    DMCreateMatrix( sol->dm, &A );
    MatSetType( A, MATMPIAIJ );
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, ". done\n");
    #endif // !QUIET



    /* ---------------------------------------------------------------------------------- */

    if (solver->import_linsys)
    {
        PetscViewer viewer;

        // #if defined(PETSC_HAVE_HDF5)
        // PetscViewerHDF5Open( PETSC_COMM_WORLD, "matA.h5", FILE_MODE_READ, &viewer );
        // PetscViewerPushFormat( viewer, PETSC_VIEWER_HDF5_PETSC );
        // PetscObjectSetName((PetscObject) A, "A");
        // #else
        // PetscViewerBinaryOpen( PETSC_COMM_WORLD, "matA.dat.gz", FILE_MODE_READ, &viewer );
        // #endif
        // MatLoad( A, viewer );
        // PetscViewerDestroy( &viewer );

        PetscViewerBinaryOpen( PETSC_COMM_WORLD, solver->linsys_fname, FILE_MODE_READ, &viewer );
        MatLoad( A, viewer );
        // PetscViewerDestroy( &viewer );

        // #if defined(PETSC_HAVE_HDF5)
        // PetscViewerHDF5Open( PETSC_COMM_WORLD, "vecb.h5", FILE_MODE_READ, &viewer );
        // PetscViewerPushFormat( viewer, PETSC_VIEWER_HDF5_PETSC );
        // #else
        // PetscViewerBinaryOpen( PETSC_COMM_WORLD, "vecb.dat.gz", FILE_MODE_READ, &viewer );
        // #endif
        // VecLoad( sol->num->source, viewer );
        // PetscViewerDestroy( &viewer );

        // PetscViewerBinaryOpen( PETSC_COMM_WORLD, solver->linsys_fname, FILE_MODE_READ, &viewer );
        VecLoad( sol->num->source, viewer );
        PetscViewerDestroy( &viewer );
    }

    else
    {
        // PetscPreLoadStage("Build A");
        PetscLogStage stagenum;
        PetscLogStageRegister("Build A",&stagenum);
        PetscLogStagePush(stagenum);

        /* Alloc nonzeros */
        getMatNZs( solver, &d_nz, &o_nz );
        MatMPIAIJSetPreallocation( A, d_nz, NULL, o_nz, NULL );

        /* Set Otions */
        VecSetOption( sol->num->source, VEC_IGNORE_NEGATIVE_INDICES, PETSC_TRUE );
        // MatSetOption( A, MAT_NO_OFF_PROC_ENTRIES, PETSC_TRUE );  // could set this iff -da_processors_x NPROCS
        MatSetOption( A, MAT_IGNORE_ZERO_ENTRIES, PETSC_TRUE );

        /* Build Coeff Matrix */
        #ifndef QUIET
        PetscPrintf( PETSC_COMM_WORLD, "\tbuilding coeff matrix A ");
        #endif // !QUIET
        MatrixConvDif( solver, mesh, sol, A, sol->num->source );


        if (solver->print_matinfo)
            MatGetInfo( A, MAT_GLOBAL_SUM, solver->matinfo );

        /* Export matrix */
        if (solver->export_linsys)
        {
            PetscViewer viewer;

            // // #if defined(PETSC_HAVE_HDF5)
            // PetscObjectSetName((PetscObject) A, "mat A");
            // PetscViewerHDF5Open( PETSC_COMM_WORLD, "matA.h5", FILE_MODE_WRITE, &viewer );
            // PetscViewerPushFormat( viewer, PETSC_VIEWER_HDF5_PETSC );
            // // #else
            // // PetscViewerBinaryOpen( PETSC_COMM_WORLD, "matA.dat.gz", FILE_MODE_WRITE, &viewer );
            // // #endif
            // MatView( A, viewer );
            // PetscViewerPopFormat( viewer );
            // PetscViewerDestroy( &viewer );
            
            PetscViewerBinaryOpen( PETSC_COMM_WORLD, solver->linsys_fname, FILE_MODE_WRITE, &viewer );
            MatView( A, viewer );
            // PetscViewerDestroy( &viewer );
            
            // // #if defined(PETSC_HAVE_HDF5)
            // PetscViewerHDF5Open( PETSC_COMM_WORLD, "vecb.h5", FILE_MODE_WRITE, &viewer );
            // PetscViewerPushFormat( viewer, PETSC_VIEWER_HDF5_PETSC );
            // // #else
            // // PetscViewerBinaryOpen( PETSC_COMM_WORLD, "vecb.dat.gz", FILE_MODE_WRITE, &viewer );
            // // #endif
            // PetscObjectSetName((PetscObject) sol->num->source, "vec b");
            // VecView( sol->num->source, viewer );
            // PetscViewerPopFormat( viewer );
            // PetscViewerDestroy( &viewer );
            
            // PetscViewerBinaryOpen( PETSC_COMM_WORLD, solver->linsys_fname, FILE_MODE_WRITE, &viewer );
            VecView( sol->num->source, viewer );
            PetscViewerDestroy( &viewer );
        }
    }


    /* ---------------------------------------------------------------------------------- */


    // PetscPreLoadStage("KSPSetUp");
    PetscPreLoadBegin(PRELOAD,"KSPSetUp");

    /* Start Solver Context */
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\n\tcreating solver context ... ");
    #endif // !QUIET

    KSPCreate( PETSC_COMM_WORLD, &ksp );
    KSPSetOperators( ksp, A, A );
    KSPGetPC( ksp, &pc );
    
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "done\n");
    #endif // !QUIET


    /* Set Solver and Preconditioner Types */
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\tconfiguring solver and preconditioner ...\n");
    #endif // !QUIET

    if (!(solver->custom_KSP))
    {
        KSPSetType( ksp, solver->ksptype );
        if (!strcmp( solver->ksptype, KSPGMRES ))
            KSPGMRESSetRestart( ksp, 5 );
        PCSetType( pc, solver->pctype );
        if (!strcmp( solver->pctype, PCASM ))
            PCASMSetOverlap( pc, solver->asm_overlap );
        else if (!strcmp( solver->pctype, PCGAMG ))
        {
            PCGAMGSetThreshold( pc, &solver->gamg_threshold, 1 );   // assuming fixed nlevel=1
#if PETSC_VERSION_LT(3,18,0)
            PCGAMGSetSymGraph( pc, PETSC_TRUE );
#endif
        }
        #ifndef QUIET
        PetscPrintf( PETSC_COMM_WORLD, "\t\tPC:  %s\n\t\tKSP: %s\n", solver->pctype, solver->ksptype);
        #endif // !QUIET
        KSPSetTolerances( ksp, solver->reltol, solver->abstol, solver->divtol, solver->maxits );
    }
    else
        KSPSetFromOptions( ksp );



    /* Set Initial Value */
    KSPSetInitialGuessNonzero( ksp, NONZERO_GUESS );
    VecCopy( sol->anal->phi, sol->num->phi );

    KSPSetUp(ksp);
    KSPSetUpOnBlocks(ksp);

    /* Compute initial residual */
    #ifndef QUIET
    Vec         r0;
    PetscReal   r0_norm1;
    VecDuplicate( sol->num->source, &r0);
    VecCopy( sol->num->source, r0 );
    VecScale( r0, -1 );
    MatMultAdd( A, sol->num->phi, r0, r0 );
    VecNorm( r0, NORM_2, &r0_norm1 );
    VecDestroy( &r0 );
    PetscPrintf( PETSC_COMM_WORLD, "\tinitial residual = %E\n", r0_norm1);
    #endif // !QUIET




    /* Solve Linear System */
    PetscPreLoadStage("KSPSolve");
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\tsolving ... ");
    #endif // !QUIET

    KSPSolve( ksp, sol->num->source, sol->num->phi );
    KSPConvergedReasonView( ksp, PETSC_VIEWER_STDOUT_WORLD );
    KSPGetConvergedReason( ksp, &(solver->reason) );
    KSPGetTotalIterations( ksp, &(solver->totits) );
    


    /* ---------------------------------------------------------------------------------- */


    /* Clean up */
    KSPDestroy( &ksp );


    PetscPreLoadEnd();
    // PetscPreLoadStage("Cleanup");


    /* Print vars */
    if (solver->print_vars)
    {
        PetscViewer viewer;

        PetscPrintf( PETSC_COMM_WORLD, "\texporting: \'sol->num->source\' -> \'_sol-num-source.out\'\n");
        PetscViewerASCIIOpen( PETSC_COMM_WORLD, "_sol-num-source.out", &viewer );
        VecView( sol->num->source, viewer );
        PetscViewerDestroy( &viewer );

        PetscPrintf( PETSC_COMM_WORLD, "\texporting: \'sol->num->phi\' -> \'_sol-num-phi.out\'\n");
        PetscViewerASCIIOpen( PETSC_COMM_WORLD, "_sol-num-phi.out", &viewer );
        VecView( sol->num->phi, viewer );
        PetscViewerDestroy( &viewer );

        PetscViewerASCIIOpen( PETSC_COMM_WORLD, "_sol-num-matA.out", &viewer );
        PetscPrintf( PETSC_COMM_WORLD, "\texporting: \'A\' -> \'_sol-num-matA.out\'\n");
        MatView( A, viewer );
        PetscViewerDestroy( &viewer );
    }



    /* Clean up */
    MatDestroy( &A );



    // PetscPreLoadEnd();

    return 0;
}


static void getMatNZs( SolverSettings* solver, PetscInt* d_nz, PetscInt* o_nz )
{
    if (solver->method == WLS_2)
    {
        *d_nz = 27;

        if (solver->alloc_opt)
            *o_nz = 12;
        else
            *o_nz = 19;
    }
    else if (solver->method == WLS_4)
    {
        *d_nz = 90;
        
        if (solver->alloc_opt)
            *o_nz = 38; // 9+21+8
        else
            *o_nz = 57;
    }
    else if (solver->method == WLS_6)
    {
        *d_nz = 210;
        
        if (solver->alloc_opt)
            *o_nz = 100;
        else
            *o_nz = 147;
    }
    else
    {
        *d_nz = 432;
        
        if (solver->alloc_opt)
            *o_nz = 280;
        else
            *o_nz = 302;
    }

    return;
}
