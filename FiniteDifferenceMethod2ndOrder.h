#ifndef FINITEDIFFERENCESMETHOD2NDORDER_H
#define FINITEDIFFERENCESMETHOD2NDORDER_H


#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <petscksp.h>

#ifdef PETSC_USE_REAL___FLOAT128
#include <quadmath.h>
#endif

#include "StructTypes.h"


PetscErrorCode FiniteDifferenceMethod2ndOrder( SolverSettings* solver, MeshGrid* mesh, Solution* sol );


#endif // !FINITEDIFFERENCESMETHOD2NDORDER_H