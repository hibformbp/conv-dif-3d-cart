#include "Gauss.h"
#include "StructTypes.h"


PetscErrorCode GaussPoints3D( SolverSettings* solver, MeshGrid* mesh,
                                Gauss* gauss, Vec kxyz, PetscReal k )
{

    /* d[p][d]: 'd'-dimension distance of point 'p' to centroid */
    PetscReal   *_d, **d;
    PetscInt    c, n;
    

    if (solver->method == WLS_4)
    {
        /* REFERENCE: Stroud 1971 (C3 3-1, pg261) */

        gauss->num = 6;
        
        
        /* allocate arrays */
        PetscMalloc1( gauss->num, &gauss->weight );
        PetscMalloc2( NDIMS*gauss->num, &_d,
                        gauss->num, &d );
        for (n = 0; n < gauss->num; n++)
            d[n] = &(_d[n*NDIMS]);
        

        /* aux values */
        PetscReal aux_1 = k * .5 * mesh->Lref;
        PetscReal aux_w = 1. / 6.;
        

        /* assign 'd' values */
        /* (1,0,0) FS combinations */
        /* point 0 */
        d[0][0] = aux_1;
        d[0][1] = 0;
        d[0][2] = 0;
        /* point 1 */
        d[1][0] = -aux_1;
        d[1][1] = 0;
        d[1][2] = 0;
        /* point 2 */
        d[2][0] = 0;
        d[2][1] = aux_1;
        d[2][2] = 0;
        /* point 3 */
        d[3][0] = 0;
        d[3][1] = -aux_1;
        d[3][2] = 0;
        /* point 4 */
        d[4][0] = 0;
        d[4][1] = 0;
        d[4][2] = aux_1;
        /* point 5 */
        d[5][0] = 0;
        d[5][1] = 0;
        d[5][2] = -aux_1;


        /* assign weights */
        gauss->weight[0] = aux_w;
        gauss->weight[1] = aux_w;
        gauss->weight[2] = aux_w;
        gauss->weight[3] = aux_w;
        gauss->weight[4] = aux_w;
        gauss->weight[5] = aux_w;

    }
    else if (solver->method == WLS_6)
    {
        /* REFERENCES: Peterson 2021, Stroud 1971 (C3 5-1, pg262) */
        /* ORIGINAL: Stroud 1967 */
        /* note: Peterson implicitly uses V=8 in weights */


        gauss->num = 13;
        

        /* allocate arrays */
        PetscMalloc1( gauss->num, &gauss->weight );
        PetscMalloc2( NDIMS*gauss->num, &_d,
                        gauss->num, &d );
        for (n = 0; n < gauss->num; n++)
            d[n] = &(_d[n*NDIMS]);
        

        /* aux values */
        #ifdef PETSC_USE_REAL___FLOAT128
        /* r (Stroud), lambda (Peterson) */
        PetscReal aux_1 = k * 0.5Q * mesh->Lref *  0.8803044066993097804773781820986034819492584487911456654305710391Q;
        /* s (Stroud), xi (Peterson) */
        PetscReal aux_2 = k * 0.5Q * mesh->Lref * -0.4958481714257111528142124236428787873515767668876735170247598902Q;
        /* u (Stroud), mu (Peterson) */
        PetscReal aux_3 = k * 0.5Q * mesh->Lref *  0.7956214221640954154298248256757873621888635407062551745880117335Q;
        /* v (Stroud), gamma (Peterson) */
        PetscReal aux_4 = k * 0.5Q * mesh->Lref *  0.0252937117448425813473892559293235835726170226482598523161688573Q;
        /* B0 (Stroud), A (Peterson) */
        PetscReal aux_a = 0.2105263157894736842105263157894736842105263157894736842105263157Q;
        /* B1 (Stroud), B (Peterson) */
        PetscReal aux_b = 0.0681234189096970896058634777261180523149654235871226066803096041Q;
        /* B2 (Stroud), C (Peterson) */
        PetscReal aux_c = 0.0634555284587239630257154696423030003166135237812984459512693432Q;
        #else
        /* r (Stroud), lambda (Peterson) */
        PetscReal aux_1 = k * 0.5 * mesh->Lref *  8.80304406699309780477378182E-1;
        /* s (Stroud), xi (Peterson) */
        PetscReal aux_2 = k * 0.5 * mesh->Lref * -4.95848171425711152814212424E-1;
        /* u (Stroud), mu (Peterson) */
        PetscReal aux_3 = k * 0.5 * mesh->Lref *  7.95621422164095415429824826E-1;
        /* v (Stroud), gamma (Peterson) */
        PetscReal aux_4 = k * 0.5 * mesh->Lref *  2.52937117448425813473892559E-2;
        /* B0 (Stroud), A (Peterson) */
        PetscReal aux_a = 2.10526315789473684210526316E-1;
        /* B1 (Stroud), B (Peterson) */
        PetscReal aux_b = 6.81234189096970896058634777E-2;
        /* B2 (Stroud), C (Peterson) */
        PetscReal aux_c = 6.34555284587239630257154696E-2;
        #endif
        

        /* assign 'd' values */
        /* point 0 */
        d[0][0] = 0;
        d[0][1] = 0;
        d[0][2] = 0;
        /* r-s combinations */
        /* point 1 */
        d[1][0] = aux_1;
        d[1][1] = aux_2;
        d[1][2] = aux_2;
        /* point 2 */
        d[2][0] = -aux_1;
        d[2][1] = -aux_2;
        d[2][2] = -aux_2;
        /* point 3 */
        d[3][0] = aux_2;
        d[3][1] = aux_1;
        d[3][2] = aux_2;
        /* point 4 */
        d[4][0] = -aux_2;
        d[4][1] = -aux_1;
        d[4][2] = -aux_2;
        /* point 5 */
        d[5][0] = aux_2;
        d[5][1] = aux_2;
        d[5][2] = aux_1;
        /* point 6 */
        d[6][0] = -aux_2;
        d[6][1] = -aux_2;
        d[6][2] = -aux_1;
        /* u-v combinations */
        /* point 7 */
        d[7][0] = aux_3;
        d[7][1] = aux_3;
        d[7][2] = aux_4;
        /* point 8 */
        d[8][0] = -aux_3;
        d[8][1] = -aux_3;
        d[8][2] = -aux_4;
        /* point 9 */
        d[9][0] = aux_3;
        d[9][1] = aux_4;
        d[9][2] = aux_3;
        /* point 10 */
        d[10][0] = -aux_3;
        d[10][1] = -aux_4;
        d[10][2] = -aux_3;
        /* point 11 */
        d[11][0] = aux_4;
        d[11][1] = aux_3;
        d[11][2] = aux_3;
        /* point 12 */
        d[12][0] = -aux_4;
        d[12][1] = -aux_3;
        d[12][2] = -aux_3;

        /* assign weights*/
        gauss->weight[ 0] = aux_a;
        gauss->weight[ 1] = aux_b;
        gauss->weight[ 2] = aux_b;
        gauss->weight[ 3] = aux_b;
        gauss->weight[ 4] = aux_b;
        gauss->weight[ 5] = aux_b;
        gauss->weight[ 6] = aux_b;
        gauss->weight[ 7] = aux_c;
        gauss->weight[ 8] = aux_c;
        gauss->weight[ 9] = aux_c;
        gauss->weight[10] = aux_c;
        gauss->weight[11] = aux_c;
        gauss->weight[12] = aux_c;

    }
    else if (solver->method == WLS_8)
    {
        /* REFERENCE: Stroud 1971 (C3 7-2, pg265) */
        /* note: usin lower signs in Stroud eqs */

        gauss->num = 34;
        

        /* allocate arrays */
        PetscMalloc1( gauss->num, &gauss->weight );
        PetscMalloc2( NDIMS*gauss->num, &_d,
                        gauss->num, &d );
        for (n = 0; n < gauss->num; n++)
            d[n] = &(_d[n*NDIMS]);


        /* aux values */
        #ifdef PETSC_USE_REAL___FLOAT128
        /* r */
        PetscReal aux_1 = k * 0.5Q * mesh->Lref * 0.925820099772551461566566776583999523Q;
        /* s */
        PetscReal aux_2 = k * 0.5Q * mesh->Lref * 0.406703186426716110513205391725062729Q;
        /* t */
        PetscReal aux_3 = k * 0.5Q * mesh->Lref * 0.734112528752115327191059792285107325Q;
        /* B0 */
        PetscReal aux_a = 0.036968449931412894375857338820301783Q;
        /* B1 */
        PetscReal aux_b = 0.011762688614540466392318244170096022Q;
        /* B2 */
        PetscReal aux_c = 0.051541732783929448674697509834406707Q;
        /* B3 */
        PetscReal aux_d = 0.028087896845700180954932119795222922Q;
        #else
        /* r */
        PetscReal aux_1 = k * 0.5 * mesh->Lref * 9.25820099772551461567E-1;
        /* s */
        PetscReal aux_2 = k * 0.5 * mesh->Lref * 4.06703186426716110513E-1;
        /* t */
        PetscReal aux_3 = k * 0.5 * mesh->Lref * 7.34112528752115327191E-1;
        /* B1 */
        PetscReal aux_a = 3.69684499314128943759E-2;
        /* B2 */
        PetscReal aux_b = 1.17626886145404663923E-2;
        /* B3 */
        PetscReal aux_c = 5.15417327839294486747E-2;
        /* B4 */
        PetscReal aux_d = 2.80878968457001809549E-2;
        #endif
        

        /* assign 'd' values */
        /* (r,0,0) FS combinations (6pts) */
        /* point 0 */
        d[0][0] = aux_1;
        d[0][1] = 0;
        d[0][2] = 0;
        /* point 1 */
        d[1][0] = -aux_1;
        d[1][1] = 0;
        d[1][2] = 0;
        /* point 2 */
        d[2][0] = 0;
        d[2][1] = aux_1;
        d[2][2] = 0;
        /* point 3 */
        d[3][0] = 0;
        d[3][1] = -aux_1;
        d[3][2] = 0;
        /* point 4 */
        d[4][0] = 0;
        d[4][1] = 0;
        d[4][2] = aux_1;
        /* point 5 */
        d[5][0] = 0;
        d[5][1] = 0;
        d[5][2] = -aux_1;
        /* (r,r,0) FS combinations (12pts) */
        /* point 6 */
        d[6][0] = aux_1;
        d[6][1] = aux_1;
        d[6][2] = 0;
        /* point 7 */
        d[7][0] = -aux_1;
        d[7][1] = aux_1;
        d[7][2] = 0;
        /* point 8 */
        d[8][0] = aux_1;
        d[8][1] = -aux_1;
        d[8][2] = 0;
        /* point 9 */
        d[9][0] = -aux_1;
        d[9][1] = -aux_1;
        d[9][2] = 0;
        /* point 10 */
        d[10][0] = aux_1;
        d[10][1] = 0;
        d[10][2] = aux_1;
        /* point 11 */
        d[11][0] = aux_1;
        d[11][1] = 0;
        d[11][2] = -aux_1;
        /* point 12 */
        d[12][0] = -aux_1;
        d[12][1] = 0;
        d[12][2] = aux_1;
        /* point 13 */
        d[13][0] = -aux_1;
        d[13][1] = 0;
        d[13][2] = -aux_1;
        /* point 14 */
        d[14][0] = 0;
        d[14][1] = aux_1;
        d[14][2] = aux_1;
        /* point 15 */
        d[15][0] = 0;
        d[15][1] = aux_1;
        d[15][2] = -aux_1;
        /* point 16 */
        d[16][0] = 0;
        d[16][1] = -aux_1;
        d[16][2] = aux_1;
        /* point 17 */
        d[17][0] = 0;
        d[17][1] = -aux_1;
        d[17][2] = -aux_1;
        /* (s,s,s) +- combinations (8pts) */
        /* point 18 */
        d[18][0] = aux_2;
        d[18][1] = aux_2;
        d[18][2] = aux_2;
        /* point 19 */
        d[19][0] = aux_2;
        d[19][1] = aux_2;
        d[19][2] = -aux_2;
        /* point 20 */
        d[20][0] = aux_2;
        d[20][1] = -aux_2;
        d[20][2] = aux_2;
        /* point 21 */
        d[21][0] = aux_2;
        d[21][1] = -aux_2;
        d[21][2] = -aux_2;
        /* point 22 */
        d[22][0] = -aux_2;
        d[22][1] = aux_2;
        d[22][2] = aux_2;
        /* point 23 */
        d[23][0] = -aux_2;
        d[23][1] = aux_2;
        d[23][2] = -aux_2;
        /* point 24 */
        d[24][0] = -aux_2;
        d[24][1] = -aux_2;
        d[24][2] = aux_2;
        /* point 25 */
        d[25][0] = -aux_2;
        d[25][1] = -aux_2;
        d[25][2] = -aux_2;
        /* (t,t,t) +- combinations (8pts) */
        /* point 26 */
        d[26][0] = aux_3;
        d[26][1] = aux_3;
        d[26][2] = aux_3;
        /* point 27 */
        d[27][0] = aux_3;
        d[27][1] = aux_3;
        d[27][2] = -aux_3;
        /* point 28 */
        d[28][0] = aux_3;
        d[28][1] = -aux_3;
        d[28][2] = aux_3;
        /* point 29 */
        d[29][0] = aux_3;
        d[29][1] = -aux_3;
        d[29][2] = -aux_3;
        /* point 30 */
        d[30][0] = -aux_3;
        d[30][1] = aux_3;
        d[30][2] = aux_3;
        /* point 31 */
        d[31][0] = -aux_3;
        d[31][1] = aux_3;
        d[31][2] = -aux_3;
        /* point 32 */
        d[32][0] = -aux_3;
        d[32][1] = -aux_3;
        d[32][2] = aux_3;
        /* point 33 */
        d[33][0] = -aux_3;
        d[33][1] = -aux_3;
        d[33][2] = -aux_3;


        /* assign weights*/
        gauss->weight[ 0] = aux_a;
        gauss->weight[ 1] = aux_a;
        gauss->weight[ 2] = aux_a;
        gauss->weight[ 3] = aux_a;
        gauss->weight[ 4] = aux_a;
        gauss->weight[ 5] = aux_a;
        gauss->weight[ 6] = aux_b;
        gauss->weight[ 7] = aux_b;
        gauss->weight[ 8] = aux_b;
        gauss->weight[ 9] = aux_b;
        gauss->weight[10] = aux_b;
        gauss->weight[11] = aux_b;
        gauss->weight[12] = aux_b;
        gauss->weight[13] = aux_b;
        gauss->weight[14] = aux_b;
        gauss->weight[15] = aux_b;
        gauss->weight[16] = aux_b;
        gauss->weight[17] = aux_b;
        gauss->weight[18] = aux_c;
        gauss->weight[19] = aux_c;
        gauss->weight[20] = aux_c;
        gauss->weight[21] = aux_c;
        gauss->weight[22] = aux_c;
        gauss->weight[23] = aux_c;
        gauss->weight[24] = aux_c;
        gauss->weight[25] = aux_c;
        gauss->weight[26] = aux_d;
        gauss->weight[27] = aux_d;
        gauss->weight[28] = aux_d;
        gauss->weight[29] = aux_d;
        gauss->weight[30] = aux_d;
        gauss->weight[31] = aux_d;
        gauss->weight[32] = aux_d;
        gauss->weight[33] = aux_d;

    }


    /* Calculate Gauss Points */
    PetscReal *arr_points;
    const PetscReal *arr_kxyz;
    VecDuplicateVecs( kxyz, gauss->num, &(gauss->points) );
    VecGetArrayRead( kxyz, &arr_kxyz );
    for (n = 0; n < gauss->num; n++)
    {
        VecGetArray( gauss->points[n], &arr_points );
        for (c = 0; c < mesh->cell_num_local; c++)
        {
            arr_points[3*c]   = arr_kxyz[3*c]   + d[n][0];
            arr_points[3*c+1] = arr_kxyz[3*c+1] + d[n][1];
            arr_points[3*c+2] = arr_kxyz[3*c+2] + d[n][2];
        }
        VecRestoreArray( gauss->points[n], &arr_points );
    }
    VecRestoreArrayRead( kxyz, &arr_kxyz );
    

    /* Clean up */
    PetscFree2( _d, d );


    return 0;
}


/* GaussPoints2D:
    * The following snippets are for reference only,
    * and will never be compiled.
    * Instead, the 2D Gauss points are directly
    * implemented in the corresponding functions
    * given in "Poly.c".
*/
#if 0
    if (solver->method == WLS_4)
    {
        /* REFERENCE: Stroud 1971 (C2 3-1, pg243), with correction from Cools 2003 */

        gauss->num = 4;

        /* aux values */
        PetscReal aux_1 = 0.5 * mesh->Lref * 5.77350269189625764509148781E-1;
        PetscReal aux_w = mesh->vol / 4.;

        /* assign 'd' values */
        /* (r,r) +- combinations */
        /* point 0 */
        d[0][0] = aux_1;
        d[0][1] = aux_1;
        d[0][2] = 0;
        /* point 1 */
        d[1][0] = aux_1;
        d[1][1] = -aux_1;
        d[1][2] = 0;
        /* point 2 */
        d[2][0] = -aux_1;
        d[2][1] = aux_1;
        d[2][2] = 0;
        /* point 3 */
        d[3][0] = -aux_1;
        d[3][1] = -aux_1;
        d[3][2] = 0;

        /* assign weights */
        gauss->weight[0] = aux_w;
        gauss->weight[1] = aux_w;
        gauss->weight[2] = aux_w;
        gauss->weight[3] = aux_w;
    }
    else if (solver->method == WLS_6)
    {
        /* REFERENCES: Stroud 1971 (C2 5-1, pg246) */

        gauss->num = 7;

        /* aux values */
        /* t */
        PetscReal aux_1 = 0.5 * mesh->Lref * 9.66091783079295904914577610E-1;
        /* r */
        PetscReal aux_2 = 0.5 * mesh->Lref * 7.74596669241483377035853080E-1;
        /* s */
        PetscReal aux_3 = 0.5 * mesh->Lref * 5.77350269189625764509148781E-1;
        /* B0 */
        PetscReal aux_c = mesh->vol * 1.38888888888888888888888889E-1;
        /* B1 */
        PetscReal aux_b = mesh->vol * 7.93650793650793650793650794E-2;
        /* B2 */
        PetscReal aux_a = mesh->vol * 2.85714285714285714285714286E-1;

        /* assign 'd' values */
        /* point 0 */
        d[0][0] = 0;
        d[0][1] = 0;
        d[0][2] = 0;
        /* (0,t) +- combinations */
        /* point 1 */
        d[1][0] = 0;
        d[1][1] = aux_1;
        d[1][2] = 0;
        /* point 2 */
        d[2][0] = 0;
        d[2][1] = -aux_1;
        d[2][2] = 0;
        /* (r,s) +- combinations */
        /* point 3 */
        d[3][0] = aux_2;
        d[3][1] = aux_3;
        d[3][2] = 0;
        /* point 4 */
        d[4][0] = aux_2;
        d[4][1] = -aux_3;
        d[4][2] = 0;
        /* point 5 */
        d[5][0] = -aux_2;
        d[5][1] = aux_3;
        d[5][2] = 0;
        /* point 6 */
        d[6][0] = -aux_2;
        d[6][1] = -aux_3;
        d[6][2] = 0;

        /* assign weights*/
        gauss->weight[0] = aux_a;
        gauss->weight[1] = aux_b;
        gauss->weight[2] = aux_b;
        gauss->weight[3] = aux_c;
        gauss->weight[4] = aux_c;
        gauss->weight[5] = aux_c;
        gauss->weight[6] = aux_c;
    }
    else if (solver->method == WLS_8)
    {
        /* REFERENCE: Stroud 1971 (C2 7-1, pg253) */

        gauss->num = 12;

        /* aux values */
        /* r */
        PetscReal aux_1 = 0.5 * mesh->Lref * 9.258200997725514615665667766E-1;
        /* s */
        PetscReal aux_2 = 0.5 * mesh->Lref * 3.805544332083156563791063591E-1;
        /* t */
        PetscReal aux_3 = 0.5 * mesh->Lref * 8.059797829185987437078561814E-1;
        /* B1 */
        PetscReal aux_a = mesh->vol * 6.049382716049382716049382716E-2;
        /* B2 */
        PetscReal aux_b = mesh->vol * 1.301482291668486142849798580E-1;
        /* B3 */
        PetscReal aux_c = mesh->vol * 5.935794367265755855452631483E-2;
        

        /* assign 'd' values */
        /* (r,0) FS combinations (4pts) */
        /* point 0 */
        d[0][0] = aux_1;
        d[0][1] = 0;
        d[0][2] = 0;
        /* point 1 */
        d[1][0] = -aux_1;
        d[1][1] = 0;
        d[1][2] = 0;
        /* point 2 */
        d[2][0] = 0;
        d[2][1] = aux_1;
        d[2][2] = 0;
        /* point 3 */
        d[3][0] = 0;
        d[3][1] = -aux_1;
        d[3][2] = 0;
        /* (s,s) +- combinations (4pts) */
        /* point 4 */
        d[4][0] = aux_2;
        d[4][1] = aux_2;
        d[4][2] = 0;
        /* point 5 */
        d[5][0] = aux_2;
        d[5][1] = -aux_2;
        d[5][2] = 0;
        /* point 6 */
        d[6][0] = -aux_2;
        d[6][1] = aux_2;
        d[6][2] = 0;
        /* point 7 */
        d[7][0] = -aux_2;
        d[7][1] = -aux_2;
        d[7][2] = 0;
        /* (t,t) +- combinations (4pts) */
        /* point 8 */
        d[8][0] = aux_3;
        d[8][1] = aux_3;
        d[8][2] = 0;
        /* point 9 */
        d[9][0] = aux_3;
        d[9][1] = -aux_3;
        d[9][2] = 0;
        /* point 10 */
        d[10][0] = -aux_3;
        d[10][1] = aux_3;
        d[10][2] = 0;
        /* point 11 */
        d[11][0] = -aux_3;
        d[11][1] = -aux_3;
        d[11][2] = 0;


        /* assign weights*/
        gauss->weight[ 0] = aux_a;
        gauss->weight[ 1] = aux_a;
        gauss->weight[ 2] = aux_a;
        gauss->weight[ 3] = aux_a;
        gauss->weight[ 4] = aux_b;
        gauss->weight[ 5] = aux_b;
        gauss->weight[ 6] = aux_b;
        gauss->weight[ 7] = aux_b;
        gauss->weight[ 8] = aux_c;
        gauss->weight[ 9] = aux_c;
        gauss->weight[10] = aux_c;
        gauss->weight[11] = aux_c;
    }

#endif