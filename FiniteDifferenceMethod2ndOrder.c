#include "FiniteDifferenceMethod2ndOrder.h"
#include "CartMesh.h"

#define QUIET

/* ----------------------- Static Function Declarations ----------------------- */
/* ---------------------------------------------------------------------------- */

/* Builds coefficient matrix and source vector for FDM of conv-dif eqn */
static PetscErrorCode MatrixFDM( SolverSettings* solver, MeshGrid* mesh, Solution* sol, Mat A, Vec b );

/* ---------------------------------------------------------------------------- */


PetscErrorCode FiniteDifferenceMethod2ndOrder( SolverSettings* solver, MeshGrid* mesh, Solution* sol )
{

    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\nCALCULATING NUMERICAL SOLUTION\n\tstarted ...\n");
    #endif // !QUIET

    PetscInt        d_nz = 7, o_nz = 3;     // num. nonzeros per row (diag, off-diag)
    Mat             A;
    KSP             ksp;                    // linear solver context
    PC              pc;                     // preconditioner context



    /* ---------------------------------------------------------------------------------- */



    /* Create linear equation Ax=b objects */
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\tcreating lin eq objects ");
    #endif // !QUIET

    VecDuplicate( sol->anal->phi, &(sol->num->phi) );
    
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, ".");
    #endif // !QUIET

    VecDuplicate( sol->anal->source, &(sol->num->source) );
    VecSetOption( sol->num->source, VEC_IGNORE_NEGATIVE_INDICES, PETSC_TRUE );
    
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, ".");
    #endif // !QUIET

    DMCreateMatrix( sol->dm, &A );
    MatSetType( A, MATMPIAIJ );
    MatMPIAIJSetPreallocation( A, d_nz, NULL, o_nz, NULL );
    MatSetOption( A, MAT_IGNORE_ZERO_ENTRIES, PETSC_TRUE );
    
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, ". done\n");
    #endif // !QUIET



    /* ---------------------------------------------------------------------------------- */



    /* Calculate entries for coefficient matrix and source vector */
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\tbuilding matrix \n\t\t");
    #endif // !QUIET

    MatrixFDM( solver, mesh, sol, A, sol->num->source );
    VecAXPY( sol->num->source, mesh->area, sol->anal->source );

    if (solver->print_matinfo)
        MatGetInfo( A, MAT_GLOBAL_SUM, solver->matinfo );

    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\n\t\tdone: %.1f sec.\n", time->num->matrix);
    #endif // !QUIET



    /* ---------------------------------------------------------------------------------- */



    /* Create KSP context */
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\tcreating solver context ... ");
    #endif // !QUIET
    
    KSPCreate( PETSC_COMM_WORLD, &ksp );
    KSPSetOperators( ksp, A, A );
    KSPGetPC( ksp, &pc );
    
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "done\n");
    #endif // !QUIET



    /* Set Solver and Preconditioner Types */
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\tconfiguring solver and preconditioner ...\n");
    #endif // !QUIET

    if (!(solver->custom_KSP))
    {
        KSPSetType( ksp, solver->ksptype );
        if (!strcmp( solver->ksptype, KSPGMRES ))
            KSPGMRESSetRestart( ksp, 5 );

        PCSetType( pc, solver->pctype );
        if (!strcmp( solver->pctype, PCASM ))
            PCASMSetOverlap( pc, solver->asm_overlap );
        else if (!strcmp( solver->pctype, PCGAMG ))
            PCGAMGSetThreshold( pc, &solver->gamg_threshold, 1 );   // assuming fixed nlevel=1
        #ifndef QUIET
        PetscPrintf( PETSC_COMM_WORLD, "\t\tPC:  %s\n\t\tKSP: %s\n", solver->pctype, solver->ksptype);
        #endif // !QUIET
        
        KSPSetTolerances( ksp, solver->reltol, solver->abstol, solver->divtol, solver->maxits );
    }
    else
        KSPSetFromOptions( ksp );



    /* Set Initial Value */
    KSPSetInitialGuessNonzero( ksp, PETSC_TRUE );
    VecCopy( sol->anal->phi, sol->num->phi );



    /* Compute initial residual */
    Vec         r0;
    PetscReal   r0_norm1;
    VecDuplicate( sol->num->source, &r0);
    VecCopy( sol->num->source, r0 );
    VecScale( r0, -1 );
    MatMultAdd( A, sol->num->phi, r0, r0 );
    VecNorm( r0, NORM_2, &r0_norm1 );
    VecDestroy( &r0 );
    
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\tinitial residual = %E\n", r0_norm1);
    #endif // !QUIET



    /* Solve Linear System */
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "\tsolving ... ");
    #endif // !QUIET
    
    KSPSolve( ksp, sol->num->source, sol->num->phi );
    
    #ifndef QUIET
    PetscPrintf( PETSC_COMM_WORLD, "done: %.1f sec.\n\t", time->num->solve);
    #endif // !QUIET
    
    KSPConvergedReasonView( ksp, PETSC_VIEWER_STDOUT_WORLD );
    KSPGetConvergedReason( ksp, &(solver->reason) );
    KSPGetTotalIterations( ksp, &(solver->totits) );



    /* ---------------------------------------------------------------------------------- */



    /* Print vars */
    if (solver->print_vars)
    {
        PetscViewer viewer;

        PetscPrintf( PETSC_COMM_WORLD, "\texporting: \'source\' -> \'_sol-num-source.out\'\n");
        PetscViewerASCIIOpen( PETSC_COMM_WORLD, "_sol-num-source.out", &viewer );
        VecView( sol->num->source, viewer );
        PetscViewerDestroy( &viewer );

        PetscPrintf( PETSC_COMM_WORLD, "\texporting: \'phi_num\' -> \'_sol-num-phi.out\'\n");
        PetscViewerASCIIOpen( PETSC_COMM_WORLD, "_sol-num-phi.out", &viewer );
        VecView( sol->num->phi, viewer );
        PetscViewerDestroy( &viewer );

        PetscViewerASCIIOpen( PETSC_COMM_WORLD, "_sol-num-matA.out", &viewer );
        PetscPrintf( PETSC_COMM_WORLD, "\texporting: \'A\' -> \'_sol-num-matA.out\'\n");
        MatView( A, viewer );
        PetscViewerDestroy( &viewer );
    }



    /* Clean up */
    KSPDestroy( &ksp );
    MatDestroy( &A );


    return 0;
}



static PetscErrorCode MatrixFDM( SolverSettings* solver, MeshGrid* mesh, Solution* sol, Mat A, Vec b )
{
    
    PetscInt        c[4], f[4], aux_c;                  // aux indexing variables
    PetscInt        total, progress = 0;                // aux vars for progress bar
    PetscInt        cell_faces[6], cols[7];             // ordering: {(P,) W, E, S, N, B, T}
    PetscReal       aux_b, inner_stencil[7], aux_A[7];  // ordering: {P, W, E, S, N, B, T}
    AO              ao;


    DMDAGetAO( sol->dm, &ao );

    total = totalUniqueStencil(mesh, solver->method+1);

    /* pre-calculate values for inner stencil */
    inner_stencil[0] = -6 * solver->gamma_diff;
    inner_stencil[1] = solver->gamma_diff  -  .5 * solver->u_conv->x * mesh->Lref;
    inner_stencil[2] = solver->gamma_diff  +  .5 * solver->u_conv->x * mesh->Lref;
    inner_stencil[3] = solver->gamma_diff  -  .5 * solver->u_conv->y * mesh->Lref;
    inner_stencil[4] = solver->gamma_diff  +  .5 * solver->u_conv->y * mesh->Lref;
    inner_stencil[5] = solver->gamma_diff  -  .5 * solver->u_conv->z * mesh->Lref;
    inner_stencil[6] = solver->gamma_diff  +  .5 * solver->u_conv->z * mesh->Lref;
    

    for (c[0] = mesh->ci_start, progress = 0; c[0] < mesh->ci_end; c[0]++)
    for (c[1] = mesh->cj_start; c[1] < mesh->cj_end; c[1]++)
    for (c[2] = mesh->ck_start; c[2] < mesh->ck_end; c[2]++)
    {
        #ifndef QUIET
        if ((progress*10)%total == 0)
            PetscPrintf( PETSC_COMM_WORLD, ".");
        #endif // !QUIET

        /* Identify Cell and Neighbors */
        c[3] = c[0]*mesh->cs2 + c[1]*mesh->cell_side + c[2];
        cols[0] = c[3];
        cols[1] = c[3] - mesh->cs2;
        cols[2] = c[3] + mesh->cs2;
        cols[3] = c[3] - mesh->cell_side;
        cols[4] = c[3] + mesh->cell_side;
        cols[5] = c[3] - 1;
        cols[6] = c[3] + 1;


        /* Calc Aux Source Terms and Aux A_* Vectors */
        if (isBndCell(mesh,c) > 0)
        {
            /* initialize aux variables */
            aux_A[0] = 0;
            aux_b    = 0;
            aux_c    = isBndCell(mesh, c);
            getCellFaces( mesh, c, cell_faces );

            /* west, east */
            if (aux_c % 2 == 1)
            {
                f[3] = cell_faces[0];
                getFaceIJK( mesh, f );
                if (isBndFace(mesh, f))
                {
                    cols[1] = -1;
                    aux_A[0] += -3 * solver->gamma_diff  +  .5 * solver->u_conv->x * mesh->Lref;
                    aux_A[1] = 0;
                    aux_A[2] = solver->gamma_diff  +  .5 * solver->u_conv->x * mesh->Lref;
                    aux_b = ( -2 * solver->gamma_diff  +  solver->u_conv->x * mesh->Lref ) 
                            * sol->anal->phi_faces[faceGlobal2Bnd(mesh,cell_faces[0])];
                }
                else
                {
                    cols[2] = -1;
                    aux_A[0] += -3 * solver->gamma_diff  -  .5 * solver->u_conv->x * mesh->Lref;
                    aux_A[1] = solver->gamma_diff  -  .5 * solver->u_conv->x * mesh->Lref;
                    aux_A[2] = 0;
                    aux_b = ( -2 * solver->gamma_diff  -  solver->u_conv->x * mesh->Lref ) 
                            * sol->anal->phi_faces[faceGlobal2Bnd(mesh,cell_faces[1])];
                }
            }
            else
            {
                aux_A[0] += -2 * solver->gamma_diff;
                aux_A[1] = solver->gamma_diff  -  .5 * solver->u_conv->x * mesh->Lref;
                aux_A[2] = solver->gamma_diff  +  .5 * solver->u_conv->x * mesh->Lref;
            }

            /* south, north */
            if (aux_c % 4 > 1)
            {
                f[3] = cell_faces[2];
                getFaceIJK( mesh, f );
                if (isBndFace(mesh, f))
                {
                    cols[3] = -1;
                    aux_A[0] += -3 * solver->gamma_diff  +  .5 * solver->u_conv->y * mesh->Lref;
                    aux_A[3] = 0;
                    aux_A[4] = solver->gamma_diff  +  .5 * solver->u_conv->y * mesh->Lref;
                    aux_b = ( -2 * solver->gamma_diff  +  solver->u_conv->y * mesh->Lref ) 
                            * sol->anal->phi_faces[faceGlobal2Bnd(mesh,cell_faces[2])];
                }
                else
                {
                    cols[4] = -1;
                    aux_A[0] += -3 * solver->gamma_diff  -  .5 * solver->u_conv->y * mesh->Lref;
                    aux_A[3] = solver->gamma_diff  -  .5 * solver->u_conv->y * mesh->Lref;
                    aux_A[4] = 0;
                    aux_b = ( -2 * solver->gamma_diff  -  solver->u_conv->y * mesh->Lref ) 
                            * sol->anal->phi_faces[faceGlobal2Bnd(mesh,cell_faces[3])];
                }
            }
            else
            {
                aux_A[0] += -2 * solver->gamma_diff;
                aux_A[3] = solver->gamma_diff  -  .5 * solver->u_conv->y * mesh->Lref;
                aux_A[4] = solver->gamma_diff  +  .5 * solver->u_conv->y * mesh->Lref;
            }

            /* bottom, top */
            if (aux_c > 3)
            {
                f[3] = cell_faces[4];
                getFaceIJK( mesh, f );
                if (isBndFace(mesh, f))
                {
                    cols[5] = -1;
                    aux_A[0] += -3 * solver->gamma_diff  +  .5 * solver->u_conv->z * mesh->Lref;
                    aux_A[5] = 0;
                    aux_A[6] = solver->gamma_diff  +  .5 * solver->u_conv->z * mesh->Lref;
                    aux_b = ( -2 * solver->gamma_diff  +  solver->u_conv->z * mesh->Lref ) 
                            * sol->anal->phi_faces[faceGlobal2Bnd(mesh,cell_faces[4])];
                }
                else
                {
                    cols[6] = -1;
                    aux_A[0] += -3 * solver->gamma_diff  -  .5 * solver->u_conv->z * mesh->Lref;
                    aux_A[5] = solver->gamma_diff  -  .5 * solver->u_conv->z * mesh->Lref;
                    aux_A[6] = 0;
                    aux_b = ( -2 * solver->gamma_diff  -  solver->u_conv->z * mesh->Lref ) 
                            * sol->anal->phi_faces[faceGlobal2Bnd(mesh,cell_faces[5])];
                }
            }
            else
            {
                aux_A[0] += -2 * solver->gamma_diff;
                aux_A[5] = solver->gamma_diff  -  .5 * solver->u_conv->z * mesh->Lref;
                aux_A[6] = solver->gamma_diff  +  .5 * solver->u_conv->z * mesh->Lref;
            }

            /* Set Values for Coeff Matrix */
            aux_c = c[3];
            AOApplicationToPetsc( ao, 1, &aux_c );
            AOApplicationToPetsc( ao, 7, cols );
            MatSetValues( A, 1, &aux_c, 7, cols, aux_A, INSERT_VALUES );

            /* Set Value for Source Vector */
            VecSetValue( b, c[3], aux_b, INSERT_VALUES );

            /* update progress count */
            progress++;
        }
        else
        {
            aux_c = c[3];
            AOApplicationToPetsc( ao, 1, &aux_c );
            AOApplicationToPetsc( ao, 7, cols );
            MatSetValues( A, 1, &aux_c, 7, cols, inner_stencil, INSERT_VALUES );
        }
    }


    /* Assemble matrix and vector */
    MatAssemblyBegin( A, MAT_FINAL_ASSEMBLY );
    VecAssemblyBegin( b );
    MatAssemblyEnd( A, MAT_FINAL_ASSEMBLY );
    VecAssemblyEnd( b );
    

    return 0;
}
